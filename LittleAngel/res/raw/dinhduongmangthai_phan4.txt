﻿5. Nhu cầu glucose
  Cần 5-6g/kg/ngày hoặc 350-420g/ngày trong thời kì mang thai và có thể tăng lên 500g/ngày trong thời kì hậu sản. Không nên vượt quá mức này vì có thể dẫn đến béo phì và đái tháo đường. 
  Nên ăn nhiều rau và trái cây, cung cấp nhiều vitamin và khoáng chất, chống táo bón.
