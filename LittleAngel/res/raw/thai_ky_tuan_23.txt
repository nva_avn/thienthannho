﻿Thai kỳ tuần 23
Lúc này nếu bạn đi siêu âm, bạn đã có thể nhìn thấy rõ ràng tay, chân và mặt mũi của con.
Thai kỳ tuần 23 mẹ cảm thấy chiếc bụng của mình đang to ra nhanh chóng, đồng nghĩa với việc con bạn cũng đang lớn nhanh đến chóng mặt. Bây giờ bé đặc biệt nhạy cảm với âm thanh. Vì thế bất cứ âm thanh nào bên ngoài đều khiến bé vô cùng thích thú.
 
Thay đổi ở Thai nhi tuần 23
 
Tuần thai thứ 23 em bé của bạn đã dài hơn 30cm và nặng khoảng 600g. Bé vẫn tiếp tục phát triển không ngừng. Lúc này nếu bạn đi siêu âm, bạn đã có thể nhìn thấy rõ ràng tay, chân và mặt mũi của con. Lỗ mũi và mắt của bé bắt đầu mở ra. Tuy nhiên “chất tạo màu” cho lòng đen của mắt vẫn chưa hình thành.
Dây rốn của bé dài và khỏe hơn, kích thước của nó lúc này là khoảng 55cm.  Dây rốn được tạo nên bởi 2 động mạch và 1 tĩnh mạch trong cơ thể và đây là chìa khóa cho sự sống của bé. Các mạch máu trong phổi của bé cũng đang phát triển mạnh mẽ, tạo tiền đề cho hơi thở sau này.
Tuyến tụy - một trong những tuyến sản xuất hoóc môn cơ bản, đang dần hoàn thiện. Thính giác của bé khi này đã khá nhạy bén để tiếp nhận những âm thanh từ phía bên ngoài, chuẩn bị cho quá trình hòa nhập thế giới bên ngoài của bé. Thời gian này, thai nhi đạp mạnh lắm rồi. Bất cứ lúc nào bạn cũng đều có thể cảm nhận được những chuyển động của thai nhi.
 
Thay đổi ở mẹ khi mang thai tuần 23
 
Trong suốt thai kỳ, do có một lượng nước khá lớn cung cấp cho cơ thể, do đó một số ít chúng có thể tụ lại thành một lớp mỏng xung quanh vùng mặt. Do đó, mặt và mí mắt mẹ dường như bị sưng phồng lên, nhất là lúc sáng sớm khi thức dậy. Bầu ngực của mẹ đang làm nhiệm vụ tích lũy sữa cho bé, điều này khiến núm vú lớn hơn, quầng vú rộng ra và sẫm màu đi. Lúc này vú mẹ cũng bắt đầu rò rỉ một loại sữa gọi là sữa non colostrum.
Bạn có thể cảm thấy đau nhiều ở phía dưới lưng suốt cả ngày dài. Điều này thực sự đem lại cho mẹ bé cảm giác rất phiền toái và khó chịu. Ngoài ra mắt cá chân và bàn chân của bạn cũng bắt đầu sưng lên một chút trong tuần hoặc vài tháng tới, đặc biệt là vào lúc cuối ngày.
Máu lưu thông chậm chạp ở vùng chân cùng với những thay đổi hóa học trong máu có thể gây ra một số khả năng giữ nước dẫn đến sưng còn được gọi là phù nề.
 
Các bệnh thường gặp
 
Bệnh tiểu đường thai nghén có thể nói là chứng bệnh khá nguy hiểm trong suốt quá trình mang thai của bạn. Bệnh này làm giảm quá trình phân giải insulin vào trong các tế bào.
Điều này khiến lượng đường trong máu và nước tiểu tăng cao. Bạn có thể dễ dàng nhận thấy mình bị mắc bệnh tiểu đường thai nghén hay không bằng cách chú ý nước tiểu xem có bị kiến đen tới hay không. Nếu bạn không khống chế được bệnh này, nguy cơ bạn mắc phải chứng phù nề sẽ tăng cao. Điều này dẫn đến khả năng bạn phải mổ đẻ cao.
Các chứng bệnh như tiền sản giật cũng làm phiền bạn trong thời gian tam cá nguyệt cuối. Ngoài ra, nếu không chữa bệnh tiểu đường thai nghén, con bạn sau khi sinh ra cũng có thể mắc phải bệnh vàng da.
 
Chế độ dinh dưỡng
 
Trong tuần này, bạn nên hạn chế lượng natri hấp thu vào cơ thể. Hấp thụ nhiều natri trong suốt thai kì khiến khả năng giải phóng nước của cơ thể bạn bị hạn chế, dẫn đến hiện tượng phù nề đối với phụ nữ khi mang thai, đặc biệt vào giai đoạn cuối của thai kỳ. Bạn nên tránh các loại thức ăn như muối lạc, muối vừng, đồ ăn nguội sẵn như xúc xích, nem chua rán, khoai tây chiên…
Bạn có thể áp dụng chế độ dinh dưỡng 6 bữa/ngày, mỗi bữa cung cấp khoảng 2000 đến 2500 kalo. Ngoài ra, bạn nên uống thêm nhiều nước lọc để tránh mất nước. Bạn cũng có thể kết hợp uống các loại nước hoa quả ít đường như nước dừa, nước cam, bưởi ép… nếu như bạn chán phải uống quá nhiều nước lọc.
Ăn nhiều rau xanh và hoa quả tươi giúp bạn giảm bớt được cảm giác nặng nề, tránh được nguy cơ bị phù nề, bệnh tiểu đường thai nghén…
Có thể bổ sung thêm lượng kalo cung cấp cho cơ thể mẹ và bé bằng việc uống thêm một số các loại sữa dành cho bà bầu. Tốt nhất bạn nên chọn sữa bầu phù hợp dưới sự chỉ dẫn, tư vấn của bác sĩ chuyên khoa.
 
Bố mẹ cần làm
 
Việc đến thăm khám bác sĩ lúc này đã trở thành vấn đề khá quan trọng với những ông bố bà mẹ tương lai. Việc này có thể giúp bố mẹ chủ động hơn trong việc đối phó với những thay đổi về sức khỏe và sự phát triển của bé. Bạn nên xét nghiệm lượng đường trong nước tiểu tuần này để sớm phát hiện và điều trị, tránh gây ảnh hưởng tới thai nhi.
Bố mẹ cùng nhau lập kế hoạch đi mua sắm thêm quần áo bà bầu hay quần áo cho em bé tương lai, điều này giúp cả bố và mẹ giảm bớt được áp lực khi mang thai, cũng như gắn kết thêm tình cảm gia đình
Bên cạnh đó bạn nên có một chế độ ăn uống hợp lý, kết hợp với việc tập thể dục giúp mẹ bé khỏe mạnh và dễ sinh hơn.
Giai đoạn này mẹ rất nhạy cảm và yếu đuối, do đó bố nên tìm hiểu các phương pháp thể dục cũng như chế độ ăn phù hợp cho mẹ, giúp mẹ các công việc gia đình, quan tâm mẹ nhiều hơn nữa nhé.
