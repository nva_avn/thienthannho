﻿6. Vitamin B1

  Nhu cầu vitamin B1 ở phụ nữ mang thai cần cung cấp đủ để phòng tránh bệnh tê phù. 
Nhu cầu vitamin B1 sẽ được đáp ứng khi sử dụng gạo không xay xát trắng quá, chế độ ăn nhiều hạt họ đậu. 
  Những thực phẩm thiếu vitamin B1 là các loại đã qua chế biến ví dụ như gạo xát quá trắng, các loại ngũ cốc, dầu mỡ tinh chế và rượu. 
  Thực phẩm giàu vitamin B1 là thịt heo, các loại hạt đậu, rau, các loại sản phẩm từ nấm mốc, men, một số loài cá.  

