﻿Lời khuyên
BỘT TRỨNG CHO TRẺ 6 -7 THÁNG TUỔI
    Thành phần 
    + Bột gạo: 2 -3 thìa cà phê
    + Lòng đỏ trứng gà: 1/2 quả
    + Dầu hoặc mỡ: 1 - 2 thìa cà phê
    + Nước mắm: 1 thìa cà phê
    + Rau băm nhuyễn: 1 thìa
    + Nước: 1 bát
   Cách làm
   + Hoà bột với nước lã vừa đủ, đặtlên bếp vừa đun vừa quấy, để sôi khoảng 5 phút
   +  Trứng đánh tơi. Tắt lửa để bột nguội bớt rồi cho trứng vào quấy đều. Bật lửa   để sôi vài phút
   + Cho rau, dầu ăn , nước mắm,quấy đều đun sôi lên
  + Đổ ra bát, cho trẻ ăn
Thành phẩm: bột chín trong, đổ bột ra róc xoong. Bột để 1lúc có độ vồng của bột

BỘT CÁ CHO TRẺ 6 -7 THÁNG TUỔI
Thành phần 
   + Bột gạo: 2 - 3 thìa cà phê
   + Cá luộc chín gỡ lấy thịt nghiền nát: 2 thìa cà phê
   + Mỡ hoặc dầu: 1 - 2 thìa cà phê
   + Rau xanh giã nhỏ: 1 thìa
   + Nước : 1 bát
   + Nước mắm: 1 thìa cà phê
   + Giã xương cá lọc lấy nước 
Cách làm
  + Hòa bột vào nước luộc cá và nước lọc xương
  + Khuấy đều và đun sôi, đậy vung khoảng 5 phút
  + Cá đã nghiền nát cho vào bát bột để sôi 2 phút
  + Cho rau, dầu, nước mắm, quấy đều đun sôi lên

Thành phẩm: bột chín trong, đổ bột ra róc xoong. Bột để 1
lúc có độ vồng của bột

BỘT LẠC CHO TRẺ 6 -7 THÁNG TUỔI
Thành phần 
  + Bột gạo: 4 thìa cà phê
  + Lạc rang chín, giã nhỏ mịn: 3 thìa cà phê
  + Mỡ hoặc dầu: 1 thìa cà phê
  + Rau xanh giã nhỏ: 1 -2 thìa cà phê
  + Nước mắm: 1 thìa cà phê
  + Nước : 1 bát
Cách làm
  + Hoà bột với nước lã vừa đủ, đặt lên bếp vừa đun vừa quấy, để sôi khoảng 5 phút
  + Lạc giã nhỏ, mịn cho vào bột để sôi vài phút
  + Cho rau, mỡ, nước mắm khuấy đều đun sôi lên
  + Đổ ra bát cho trẻ ăn
Thành phẩm: bột chín trong, đổ bột ra róc xoong. Bột để 1
lúc có độ vồng của bột

BỘT TÔM CHO TRẺ 6 -7 THÁNG TUỔI
Thành phần
  + Bột gạo: 4 thìa cà phê
  + Tôm bóc vỏ giã nhỏ: 3
  thìa cà phê
  + Rau xanh giã nhỏ: 1 thìa cà phê
  + Nước mắm: 1 thìa cà phê
  + Nước : 1 bát
 Cách làm
  + Giã vỏ tôm lọc lấy nước.
  + Cho tôm giã/băm nhỏ hòa vào nước lọc tôm và cho bột gạo vào hòa tan. Vừa đun vừa khuấy đều. Khi sôi đậy vung  khoảng 5 phút.
  + Cho rau xanh giã nhỏ.
  + Cho nước mắm, dầu quấy đều đun sôi lên.
  + Đổ ra bát cho trẻ ăn
Thành phẩm: bột chín trong, đổ bột ra róc xoong. Bột để 1
lúc có độ vồng của bộ.

BỘT THỊT CHO TRẺ 8 -9 THÁNG TUỔI
 Thành phần 
   + Bột gạo: 4 thìa cà phê
   + Thịt (lợn, bò, gà..) băm nhỏ nhuyễn : 3 thìa cà phê
   + Mỡ hoặc dầu: 2 thìa cà phê
   + Rau xanh giã nhỏ: 1 -2 thìa cà phê
  + Nước mắm: 1 thìa
  + Nước : 1 bát
 Cách làm
  + Hoà bột với nước lã vừa đủ,cho thịt đã băm nhuyễn hòa cùng với bột, đặt lên bếp vừa đun vừa quấy, để sôi đậy vung khoảng 5 phút
  + Cho rau, dầu/mỡ, nước mắm khuấy đều đun sôi vài phút
  + Đổ ra bát cho trẻ ăn

Thành phẩm: bột chín trong, đổ bột ra róc xoong. Bột để 1
lúc có độ vồng của bột

BỘT CUA CHO TRẺ 8 -9 THÁNG TUỔI
  Thành phần 
   + Bột gạo: 4 thìa cà phê
   + Nước lọc cua: 1 bát
   + Mỡ hoặc dầu: 1 thìa cà phê
   + Rau xanh giã nhỏ: 1 - 2 thìa cà phê
   + Nước mắm: 1 thìa cà phê
Cách làm
   + Hoà bột gạo với nước lọc cua, vừa đun vừa quấy sôi, đậy vung khoảng 5 phút
   + Cho rau xanh giã nhỏ cho vào bột để sôi vài phút
   + Cho gạch cua đã lọc vào khuấy đều, cho dầu/mỡ, nước mắm, khuấy đều đun sôi lên
   + Đổ ra bát cho trẻ ăn
Thành phẩm: bột chín trong, đổ bột ra róc xoong. Bột thơm
không tanh. Bột để 1 lúc có độ vồng của bộ

BỘT GAN CHO TRẺ 10 -12 THÁNG TUỔI
Thành phần 
   + Bột gạo: 4 thìa cà phê
   + Gan (lợn, gà) băm hoặc nghiền nát: 3 thìa cà phê
   + Dầu ăn: 2 thìa cà phê
   + Rau xanh giã nhỏ: 2 thìa cà phê
   + Nước mắm: 1 thìa cà phê
   + Nước: 1 bát con
Cách làm
   + Hoà bột với nước lã vừa đủ, đặt lên bếp vừa đun vừa quấy, để sôi khoảng 5 phút
   + Gan băm nhỏ hoặc nghiền nát cho vào bột để sôi vài phút
   + Cho rau xanh giã nhỏ
   + Cho nước mắm, dầu quấy đều đun sôi lên
   + Đổ ra bát cho trẻ ăn
Thành phẩm: bột chín trong, đổ bột ra róc xoong. Bột để 1
lúc có độ vồng của bột.

CHÁO CÁ CHO TRẺ TỪ 10 - 12 THÁNG TUỔI
 Thành phần 
  + Cháo sơ chế : 5 thìa cà phê
  + Cá luộc chín, gỡ thịt nghiền nát: 3 thìa cà phê
  + Mỡ hoặc dầu: 1 - 2 thìa cà phê
  + Nước mắm: 1 thìa cà phê
Cách làm
  + Đun sôi cháo đã sơ chế
  + Cá luộc chín gỡ thịt nghiền nát cho vào cháo để sôi vài phút
  + Cho rau xanh giã nhỏ vào
  + Cho nước mắm, dầu quấy đều đun sôi lên
  + Đổ ra bát cho trẻ ăn

CHÁO THỊT CHO TRẺ TỪ 10 - 12 THÁNG TUỔI
Thành phần 
  + Cháo sơ chế: 5 thìa cà phê
  + Thịt (lợn, bò, gà….) băm nhỏ, nhuyễn: 3 thìa cà phê
  + Mỡ hoặc dầu: 2 thìa cà phê
  + Rau xanh giã nhỏ: 2 thìa
  + Nước mắm:1 thìa cà phê
Cách làm
  + Đun sôi cháo đã sơ chế
  + Cá luộc chín gỡ thịt nghiền nát cho vào cháo để sôi vài phút
  + Cho rau xanh giã nhỏ vào
  + Cho nước mắm, dầu quấy đều đun sôi lên
  + Đổ ra bát cho trẻ ăn

CHÁO TÔM CHO TRẺ TỪ 10 - 12 THÁNG TUỔI
Thành phần 
  + Cháo sơ chế: 5 thìa cà phê
  + Tôm bóc vỏ: 3 thìa cà phê 
  + Mỡ hoặc dầu: 2 thìa cà phê
  + Rau xanh giã nhỏ: 2 thìa cà phê
  + Nước mắm: 1 thìa
Cách làm
  + Đun sôi cháo sơ chế.
  + Tôm bóc vỏ giã nhỏ cho vào cháo để sôi vài phút
  + Cho rau xanh giã nhỏ vào
  + Cho nước mắm, dầu, quấy đều đun sôi lên
  + Đổ ra bát cho trẻ ăn

CHÁO TRỨNG CHO TRẺ TỪ 10 - 12 THÁNG TUỔI
Thành phần 
  + Cháo sơ chế: 5 thìa cà phê
  + Trứng gà 1 quả (lòng đỏ) hoặc 4 quả trứng chim cút
  + Mỡ hoặc dầu: 1-2 thìa càphê
  + Rau xanh giã nhỏ: 2 thìa cà phê
  + Nước mắm: 1 thìa cà phê
Cách làm
  + Đun sôi cháo sơ chế
  + Lấy lòng đỏ bỏ vào bát đánh đều cho vào bột vừa đổ vừa quấy đều, để sôi vài phút
  + Cho rau xanh giã nhỏ vào
  + Cho nước mắm, dầu quấy đều đun sôi lên.
  + Đổ ra bát cho trẻ ăn
