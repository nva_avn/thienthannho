﻿2. Ba tháng giữa thai kỳ
 a) Sinh lí, giải phẫu học thai nhi: da và các tuyến trong da hoàn chỉnh dần và tiết ra chất gây. Buồng trứng hình thành hoàn chỉnh và tinh hoàn từ bụng xuống bìu khoảng tháng thứ 7. Người mẹ có thể cảm nhận được những cử động của thai nhi (thai máy) từ lúc thai được 19 tuần, cảm nhận này sẽ rõ hơn khi thai được khoảng 25 – 28 tuần.
 b) Những thay đổi ở người mẹ: mẹ bắt đầu tăng cân, phù chân, cảm nhận đau và dãn khớp xương, có thể hay chóng mặt, đau đầu, đau lưng và táo bón. Sau 25 tuần, bắt đầu xuất hiện cơn gò Braxton Hicks.
 c) Khám thai và các xét nghiệm cần thiết:
- Cần khám thai mỗi tháng để được kiểm tra lại cân nặng, huyết áp, dấu hiệu phù chân, theo dõi sự phát triển thai nhi qua đo bề cao tử cung, nghe tim thai.
- Xét nghiệm nước tiểu để phát hiện sớm tiền sản giật, nhiễm trùng tiểu.
- Siêu âm: khảo sát hình thái học thai nhi khi thai được 18 – 24 tuần. Siêu âm không thể thấy được hết các dị tật của thai nhi và một số dị tật xuất hiện muộn nên kết quả siêu âm bình thường không có nghĩa là thai nhi hoàn toàn bình thường. Mặc dù siêu âm không có hại, nhưng chỉ nên siêu âm khi có chỉ định của bác sĩ.
- Test dung nạp đường: thời điểm 24 – 28 tuần, phụ nữ châu Á hơn 25 tuổi, hoặc có tiền sử gia đình tiểu đường type II, tiền sử thai chết lưu, sinh con > 4kg.
 d) Những biến chứng của thai kì có thể xảy ra như: tiền sản giật (sau 20 tuần), thai chết lưu, nhau tiền đạo, nhau bong non, ối vỡ non, sinh cực non.
 e) Những dấu hiệu bất thường cần lưu ý: Nhức đầu, chóng mặt, hoa mắt, nhìn mờ, đau thượng vị, phù nhiều, tiểu gắt, tiểu buốt, bụng gò cứng kèm đau, ra nước hoặc ra huyết âm đạo… 
 f) Những việc thai phụ cần làm
- Uống nhiều nước, ăn nhiều chất xơ
- Tiêm VAT ngừa uốn ván
- Bổ sung viên sắt mỗi ngày đến sau sinh 1-3 tháng
- Bổ sung 1.000mg canxi mỗi ngày cho đến lúc cai sữa, hoặc có thể bổ sung canxi bằng sữa, phômai
- Mang giày thấp, tránh trơn trợt
- Giữ lưng thẳng, không cúi khom người
- Nằm kê chân cao
- Thay đổi tư thế từ từ, nếu chóng mặt nên nằm nghiêng
- Nên ăn nhiều cá và rau xanh đậm
- Tập thể dục nhẹ nhàng
 g) Những việc cần tránh
- Tránh tăng cân quá nhanh
- Tránh ăn nhiều tinh bột, thức ăn ngọt
- Tránh ăn quá mặn 
- Tránh làm việc nặng, thức khuya
- Tránh ăn cá có nhiều thủy ngân
