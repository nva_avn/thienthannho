﻿Sự phát triển của bé 10 tháng tuổi
Bé của bạn đã được 10 tháng, “3 tháng biết lẫy, 7 tháng biết bò, 9 tháng lò dò biết đi” bé đều đã trải qua. Đây là giai đoạn các khả năng của bé được hoàn thiện dần.
1. Sự phát triển của bé:
 
Ở tháng thứ 9 bé mới chỉ chập chững bước đi những bước đầu tiên. Thì đến giai đoạn này bé đã có thể bò với tốc độ nhanh hơn so với lúc trước. Khả năng di chuyển và sự hiếu động của bé khiến các bậc phụ huynh không khỏi lo lắng cho sự an toàn của bé. Bé còn có thể tự ngồi được rồi tự đứng mà không cần sự giúp đỡ của bất kỳ ai. Một số bé có thể đi vài bước.
Bé  biết đem đồ chơi cầm trong tay bỏ vào trong đồ đựng, nhưng động tác vẫn còn chậm do cơ và xương còn yếu. Điều này dẫn đến việc bé sẽ thường xuyên bị ngã hơn. Bạn cũng sẽ bị bất ngờ khi bé có khả năng cúi xuống và nhặt một món đồ chơi trong khi mẹ vẫn giữ một tay cho bé.
 
Trí não của bé tương đối phát triển, khả năng tìm kiếm các đồ vật đến giai đoạn này đã rất nhạy bén. Khả năng nhớ cũng tốt hơn. Bé đặc biệt thích bắt chước động tác của người lớn và những đứa bé khác.
Bé cũng thích cùng chơi với bố mẹ, xem sách, nghe người lớn kể chuyện, thích chơi trò giấu đồ vật. Một vài bé còn biết chủ động dang tay ra để phối hợp khi bố mẹ mặc quần áo cho.
Đây là giai đoạn tốt để bạn dạy cho bé một vài điều bổ ích như tên các đồ vật, con vật, hình khối hay cách gọi tên bố, mẹ. Hãy cho bé chơi vơi một vài quyển truyện tranh màu sắc để tạo cho bé hứng thú đọc sách sau này nhé.
Hệ thống xương và răng của bé đang dần hoàn thiện để phù hợp cho quá trình vận động và tiêu hóa sau này. Lúc này bé đã mọc khoảng 6 răng cửa do đó có thể ăn được các món snack mềm hoặc miếng hoa quả nhỏ.
 
Các giác quan phát triển mạnh mẽ trong những tháng này. Điều này giải thích vì sao bé cười với mọi người khi gặp, vẫy tay theo sự hướng dẫn của bố mẹ khi chào tạm biệt.
Khả năng ngôn ngữ của bé đã tiến triển tốt hơn so với tuần trước. Bé nói nhiều hơn, bi bô cả ngày rất đáng yêu. Bé có thể hiểu và nói được nhiều cụm từ đơn giản. Khi bắt chước lời nói của người lớn, bé bắt chước về ngữ điệu và sắc thái biểu cảm chính xác hơn bắt chước ngữ âm.
 
1. Chế độ dinh dưỡng cần thiết cho bé 10 tháng tuổi:
 
Do giai đoạn này bé đang rất hiếu động, vận động nên bạn có thể nhận thấy sự thèm ăn và số lượng thực phẩm của bé tăng lên khá nhiều. Chế độ dinh dưỡng trong các bữa ăn của bé cũng giống như thực đơn lúc bé được 9 tháng tuổi, chỉ có khác là bổ sung khối lượng tăng dần theo tháng tuổi hoặc tăng dần vì bé thấy ngon miệng.
Ngoài sữa mẹ và sữa bột hang ngày bé cần thêm 3 bữa chính vơi đầy đủ các chất dinh dưỡng cần thiết như thịt cá, tôm, trứng, đậu, rau củ quả… Thức ăn cho bé vẫn phải được nghiền bằng nĩa nhưng bạn có thể tiếp tục tăng dần độ đặc cũng như thức ăn mềm có thể xắt thành những miếng lớn hơn 1 chút.
Ngoài các bữa chính bạn có thể bổ sung thêm các bữa phụ trong ngày như bánh snack, bánh quy có đầy đủdinh dưỡng dành cho lứa tuổi này, hoa quả tươi, hoặc nước ép… Đến thời điểm này bé đã có thể ăn nguyên một lòng đỏ trứng gà một lần. Tuy nhiên bạn đừng nên cho bé ăn quá nhiều trứng gà trong 1 tuần.
 
2. Những bệnh thường gặp ở bé và cách phòng tránh:
 
Vì bé đang trong thời kỳ ăn dặm nên có thể sẽ gặp tình trạng đi phân sống. Nếu gặp tình trạng này, mẹ có thể cho cháu dùng men tiêu hóa khoảng 10 ngày, không nên kéo dài vì không cần thiết, quan trọng là phải chọn những thức ăn phù hợp với lứa tuổi của bé.
Các bệnh sốt do mọc răng, dị ứng thức ăn hoặc các bệnh về da như rôm sảy, mụn nhọt vẫn xảy ra thường xuyên khiến bé trong độ tuổi này thường quấy khóc khiến cha mẹ rất lo lắng. Nếu các triệu chứng bệnh ngày càng nặng hơn, bố mẹ cần đưa bé tới các cơ sở y tế để tìm ra bệnh và có phương pháp điều trị.
 
3. Bố mẹ cần làm:
 
Do càng ngày càng năng động nên bé háo hức khám phá quanh nhà nhiều hơn. Bé thậm chí có thể bò lên hoặc lao xuống cầu thang nếu bố mẹ lơ là. Vì thế, đây là thời điểm bạn cần đóng các cửa lại hoặc chặn thanh chắn ở lối cầu thang để bé an toàn. Bé rất hiếu động nên nước sôi, những đồ vật sắc nhọn cũng cần được giữ xa khỏi bé để tránh gây ra các tổn thương nguy hiểm.
Tháng thứ 10 cũng là thời điểm bé tiếp thu rất nhanh và có khả năng ghi nhớ tốt. Do đó, quan trọng hơn hết là mẹ nên tiếp tục trò chuyện cùng con. Bạn có thể giúp con học từ mới bằng cách lặp đi lặp lại khi bé nỗ lực tiếp cận với một đồ vật.

