﻿7. I-ốt
  I-ốt có vai trò rất quan trọng đối với phụ nữ mang thai. Hậu quả nghiêm trọng nhất của thiếu I-ốt là ảnh hưởng đến sự phát triển của bào thai. Phụ nữ mang thai thiếu I-ốt có nguy cơ xảy thai, thai chết lưu, sinh non, trẻ sinh ra sẽ bị chậm phát triển trí tuệ do tổn thương não, cân nặng sơ sinh thấp, ngoài ra dễ bị các khuyết tật bẩm sinh như liệt tay chân, nói ngọng, điếc, câm, lé. Thiếu I-ốt dẫn đến tăng tỷ lệ tử vong chu sinh.  
Nhu cầu I-ốt ở phụ nữ mang thai cần cao hơn bình thường,200 µg/ngày.
  Thực phẩm giàu I-ốt là cá biển, rong biển. Sử dụng muối ăn có bổ sung I-ốt là giải pháp chính để phòng chống các rối loạn do thiếu hụt I-ốt. 
