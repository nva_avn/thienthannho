﻿Thai kỳ tuần 8
Thai nhi tuần này dài khoảng 1,3 cm, nặng chỉ khoảng 1g. Tất cả các bộ phận cần thiết cho cơ thể sống đã hình thành đầy đủ ở trong phôi thai.
Phôi thai trong tuần thứ 7 đã chuyển biến khá hoàn thiện, tạo bước đà cho tuần phát triển thứ 8. Đây là tuần cuối cùng của thời kỳ phôi thai để bắt đầu thời kỳ tiếp theo, bào thai. Sự phát triển của thai nhi trong tuần thai thứ 8 như thế nào, và mẹ bé cần biết những gì, chúng ta cùng đi sâu vào tìm hiểu nhé.
 
Thay đổi ở thai nhi tuần thứ 8:
 
Tuần thứ 8, thai nhi phát triển liên tục và biến đổi không ngừng trong cơ thể bạn. Thai nhi tuần này dài khoảng 1,3 cm, nặng chỉ khoảng 1g. Tất cả các bộ phận cần thiết cho cơ thể sống đã hình thành đầy đủ ở trong phôi thai. Chiếc đuôi nhỏ trong những tuần đầu tiên vẫn chưa rụng nhưng nó đang ngày càng nhỏ đi so với của cơ thể đang phát triển của bé.
Tim và não đã khá hoàn hảo. Mí mắt của phôi thai đang hình thành, chóp mũi của bé cũng đã xuất hiện. Các chi của bé giờ đã có thể nhận biết rõ ràng. Hàm ếch và răng cũng đang hình thành. Tai của phôi thai cũng phát triển cả trong lẫn ngoài. Khung xương liên kết với các cơ để tạo hình cho cơ thể, khuôn mặt cũng được hình thành rõ nét hơn.
Bộ não của thai nhi với các tế bào thần kinh trong tuần này phát triển nhanh chóng. 
 
Thay đổi ở mẹ trong tuần thai thứ 8:
 
Cơ thể bạn khi mang thai 8 tuần vẫn chưa có biểu hiện gì khác thường vì bụng của bạn còn chưa to ra. Thậm chí người ngoài còn chưa nhận ra bạn đang mang thai. Bạn cũng không hề có cảm giác gì, nhưng thực tế tử cung của bạn đang lớn không ngừng cùng với sự phát triển của thai nhi.
Tuần này, bạn vẫn tiếp tục chịu đựng các cảm giác khó chịu của ốm nghén. Một số chị em có những kiểu ốm nghén rất lạ như thích ăn rễ cây, ăn gạch…
Một số trường hợp khác lại không có biểu hiện ốm nghén, các ông chồng tự nhiên lại thèm ăn một thức ăn nào đó trong khi bình thường anh ta không thích ăn, hoặc không ăn nhiều như vậy. Trường hợp này dân gian gọi là ốm nghén thay vợ.
Các biểu hiện của ốm nghén là rất bình thường nên nếu chị em mắc phải cũng không nên quá lo lắng mà gây ảnh hưởng cho sức khỏe bản thân và thai nhi trong bụng.
 
Chế độ dinh dưỡng:
 
Nếu bạn vẫn cảm thấy ốm nghén nhiều và không thể ăn nhiều, hãy thử uống nhiều nước quả như nước dừa nước cam để bổ sung vitamin và khoáng chất cho cơ thể của bạn và cho thai nhi, tránh các đồ uống có ga vì nó sẽ gây hại cho bé yêu.
Nếu thời tiết nóng ẩm, hãy tăng cường uống thêm nước để tránh tình trạng khử nước. Có rất nhiều lựa chọn cho bà bầu: sữa, kem, nước dừa, nước chanh tươi… đều rất bổ dưỡng.
Đảm bảo nước bạn uống là sạch hoàn toàn. Nếu phải mua nước khi đi ngoài đường thì nên chọn các thương hiệu có uy tín, kiểm tra kỹ hạn sử dụng và thành phần.
 
Các bệnh thường gặp:
 
Trong giai đoạn đầu mang thai, đặc biệt là trong tuần thứ 8 này nhiều phụ nữ mang thai cảm thấy mình vô cùng nhạy cảm, dễ khóc. Nguyên nhân là do sự tăng tiết của các hoocmon và cả những lo lắng bâng quơ.
Hãy cố gắng thư giãn và dành càng nhiều thời gian nghỉ ngơi càng tốt. Không chỉ cảm xúc lên xuống thất thường khi bầu bí mà sau sinh, đôi khi bạn sẽ thấy mình thật yếu đuối.
Ở giai đoạn này cũng dễ bị sẩy thai với dấu hiệu dễ nhận biết nhất là ra máu nơi vùng kín. Rất nhiều người bị sẩy thai như vậy mà không hề biết.
 
Bố mẹ cần làm:
 
Hãy duy trì hoạt động thăm khám tại bệnh viện hoặc các phòng khám chuyên khoa để có được những lời khuyên cũng như có những giải pháp kịp thời với một số trường hợp bất thường. Tránh sử dụng các loại đồ ăn thức uống có chứa nhiều chất kích thích vì các chất này dễ gây ảnh hưởng tới bé. Nên tìm hiểu các vấn đề liên quan tới sự phát triển của bé để có kinh nghiệm chăm sóc bé tốt hơn.
Nên tránh stress cho mẹ vì chúng có thể làm ảnh hưởng tới sự phát triển bình thường của bé. Bố bé có thể giúp mẹ bé giảm ốm nghén bằng việc giúp mẹ làm các công việc nhà hoặc quan tâm mẹ bé nhiều hơn. Nên áp dụng các biện pháp giảm ốm nghén, uống trà thảo dược, chăm sóc da tốt nhất.
Giữ sức khỏe thật tốt cho mẹ cũng là giữ cho bé yêu luôn luôn được khỏe mạnh!

