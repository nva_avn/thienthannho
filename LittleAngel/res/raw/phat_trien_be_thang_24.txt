﻿Sự phát triển của trẻ 24 tháng tuổi
Từ lúc mầm sống nhỏ nhoi xuất hiện, khi con yêu chào đời đến khi trẻ đang chuẩn bị đón chờ sinh nhật lần thứ hai của mình, thời gian trôi đi thật nhanh nhưng đây cũng là quãng thời gian vất vả nhất của bạn. Mỗi ngày con lớn là một ngày bạn có thêm những vui sướng, hạnh phúc.
1. Sự phát triển của trẻ 24 tháng tuổi:
 
Khi trẻ được 2 tuổi thì cân nặng và chiều cao tăng lên đáng kể. Trung bình trẻ nặng khoảng 12kg cao 85cm, và hàm răng tương đối hoàn thiện (20 răng ).
 
Ở tháng thứ 24, trẻ ngày càng hiếu động hơn đặc biệt là những bé trai. Khả năng vận động của trẻ tương đối tốt, trẻ có thể chạy nhảy bất cứ nơi nao, khi nào trẻ muốn.
 
Tuy nhiên hầu hết trẻ đều thích chạy hơn thích đi! Trẻ rất thích được tự làm theo ý của mình mà không cần bất cứ sự trợ giúp nào từ ai. Trẻ có thể tự mở cửa thậm chí cả cửa có then cài nếu trẻ có thể với tới.
Một thú vui đặc biệt của lứa tuổi này là thích tự lên xuống cầu thang một mình.
 
 
 
Khả năng nhận thức của trẻ cũng đạt tới tầm cao, trẻ đã có thể hiểu được các cử chỉ thông thường của mọi người xung quanh. Khả năng ghi nhớ cũng tốt hơn so với những tháng trước đây.
 
Trẻ đặt ra rất nhiều câu hỏi và muốn được người lớn giải đáp ngay lập tức! Nếu bạn không muốn trả lời trẻ bạn đừng nên bực tức, hay cáu giận nhé, hãy bớt chút thời gian để trả lời trẻ hoặc hứa sẽ trả lời trẻ trước giờ đi ngủ chẳng hạn, điều này giúp trẻ hình thành thói quen có trách nhiệm, và quan tâm tới việc của người khác.
 
Trẻ rất thích bắt chước người khác và đặc biệt thích thú khi được nói chuyện điện thoại. Một vài trẻ thường cầm một đồ vật gì đó tương tự như một chiếc điện thoại và tự sáng tạo ra một cuộc nói chuyện rất lâu.
 
Trẻ bắt chước một cách cực kỳ chính xác thái độ và cách nói chuyện của bố mẹ khi gọi điện.
 
Khả năng nói và giao tiếp xã hội của trẻ cũng tiến triển nhiều. Lúc này trẻ đã có thể kể cho bố mẹ nghe một mẩu chuyện nhỏ, hay hát một bài hát mà trẻ thích.
 
Đừng tiếc lời khen mỗi khi trẻ kể chuyện hoặc hát nhé. Điều này sẽ tạo ra phấn khích đặc biệt giúp trẻ tiếp tục phát huy những lần sau.
 
2. Chế độ dinh dưỡng cần thiết:
 
Ở tuổi này, hệ tiêu hoá của trẻ đã tương đối hoàn thiện. Hơn nữa trẻ được làm quen và ăn được nhiều món ăn giống với người lớn nhờ hàm răng phát triển.
 
Trung bình khẩu phần ăn của trẻ bằng 2/3 so với khẩu phần ăn của người lớn. Do trẻ vẫn trong quá trình sinh trưởng và phát triển nên bố mẹ vẫn nên đặc biệt chú ý tới chế độ dinh dưỡng hàng ngày của trẻ.
 
Chế độ ăn hằng ngày cần có đầy đủ các nhóm dinh dưỡng thiết yếu cho sự tăng trưởng như: Protein (đạm), lipit (béo), gluxit (đường, bột), chất khoáng, vitamin, vi chất và nước.
 
Tuy nhiên trẻ không thích ăn những thức ăn lạ mà chỉ thích ăn những món quen thuộc mà trẻ thấy thích. Vì vậy, bố mẹ cần lưu ý khi nào muốn trẻ trải nghiệm những món mới cần tập cho trẻ ăn lượng ít đến nhiều, thỉnh thoảng đến thường xuyên để tránh trẻ bị dị ứng thức ăn, hay ăn không ngon miệng.
 
Không nên ép trẻ ăn thêm khi trẻ không muốn ăn nữa để tránh cảm giác sợ ăn những lần sau.
 
3. Bệnh thường gặp và cách phòng tránh:
 
Thời tiết trong mùa đông ở nước ta rất khắc nghiệt đặc biệt là ở miền Bắc. Điều này khiến nhiều người không quen đặc biệt là trẻ em và thường mắc phải một số bệnh về đường hô hấp như viêm mũi họng.
 
Viêm mũi họng thường hay gặp ở trẻ do virus gây ra. Biểu hiện của bệnh thường là trẻ chảy nước mũi nhiều, ít ho. Một số trường hợp chảy mũi sau, nước mũi chảy xuống họng gây ho kinh khủng. Bệnh này có thể tự điều trị bằng cách cho trẻ uống thuốc và dùng dung dịch natri clorid 0.9%, làm sạch mũi…
 
Để phòng bệnh này, điều cơ bản là giữ trẻ không bị lạnh, đặc biệt là cổ họng và mũi như quàng khăn hoặc đeo khẩu trang cho trẻ. Thêm vào đó, trong mùa lạnh nên cho trẻ ăn đủ chất để nâng cao sức đề kháng, đồng thời vệ sinh môi trường sạch sẽ, thông thoáng khí. 
 
Lưu ý: Có một vài trường hợp chậm nói là biểu biện của hội chứng tự kỷ. Nếu đến thời điểm này mà trẻ còn chưa biết nói thì cha mẹ nên đưa trẻ tới gặp bác sĩ để kiểm tra.
 
4. Cha mẹ nên làm:
 
 
 
Kế hoạch cai sữa cho con của bạn đã được thực hiện chưa? Hãy tham khảo một vài lời khuyên của chúng tôi để có thể cai sữa nhanh chóng cho bé mà không quá vất vả.
 
Trong cột mốc này, vai trò của bố mẹ vô cùng quan trọng đối với sự phát triển toàn diện về cả thể chất lẫn trí tuệ của trẻ. Do đó cha mẹ vẫn không được lơ là việc duy trì một chế độ dinh dưỡng hợp lý hàng ngày, giữ vệ sinh thân thể cho trẻ để chống lại một số bệnh dễ mắc.
 
Ngoài ra có thể chơi cùng trẻ để phát huy khả năng sáng tạo của trẻ cũng như hiểu trẻ hơn. Bạn có thể tổ chức một bữa tiệc nho nhỏ để kỉ niệm ngày trẻ tròn 2 tuổi và đừng quên lưu lại những giây phút tuyệt vời này.

