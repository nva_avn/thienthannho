﻿Bé một tuần tuổi: Sự phát triển tổng quan
Em bé sơ sinh của bạn đang bắt nhịp và một thế giới mới đầy mới lạ với đầy ánh sáng và sự sôi động. Hãy xem những thay đổi đầu đời của bé ra sao nhé.
Sự di chuyển:
Bé di chuyển một cách gượng gạo, và thích ứng còn khó khăn. Thế nhưng, đến khoảng cuối tháng, bạn sẽ thấy được sự di chuyển của bé nhanh nhẹn hơn, cách điều khiển cơ bắp cũng già giặn hơn.
 
Cách thở:
Hãy chú ý xem bé của bạn có thường xuyên thở dốc hay không? Đừng lo lắng nếu bé tạo ra âm thanh ồn ào khi ngủ nhé, không phải bé bị cảm lạnh đâu. Đó chỉ là do đường hô hấp của bé đang trong thời gian thích nghi với không khí khác lạ trong nhà bạn mà thôi.
Nếu quan sát kỹ hơn, bạn sẽ thấy được cách thở của bé, có thể là nhanh và sâu vào lúc đầu rồi chậm và êm dần, hoặc chậm sâu rồi thi thoảng phát ra âm thanh khò khè khá ồn. Nếu bạn cảm thấy quá bất thường và lo lắng, hãy gọi điện ngay cho bác sĩ hay hộ sinh của bạn.
Cách chơi đùa:
Hãy chơi đùa với bé bằng những cử chỉ vui vẻ, gây háo hức và nhẹ nhàng, tình cảm. Các bé thực sự thích nhìn thấy mặt người lớn, chúng xem đó là một trò vui đặc biệt.
Đặc biệt, trong thời gian này, bé chỉ thích mẹ thôi nhé. hãy gần gũi và nựng bé bằng những cử chỉ, tình cảm hết sức thân thương. Lúc này, tầm nhìn của bé còn khá kém, tối đa khoảng 20cm. Ngoài khuôn mặt của mẹ, bé thích nhìn vào những vật sáng, nhiều màu sắc và chuyển động linh hoạt.
 
Việc bảo vệ bé:
Khi mới sinh ra, bạn đừng nên ấp ủm quá nhiều cho bé. Bé cần được một môi trường thoải mái để có thể thích nghi nhanh chóng. Nếu có thể, hãy cho bé nằm sấp để giúp tăng sức mạnh và sự dẻo dai của các cơ, đặc biệt là cơ cổ.
 
Cho bé bú:
Sữa mẹ luôn là nguồn năng lượng tuyệt vời và có tác động vô cùng quan trọng đối với bé, cả về sức khỏe thể chất và tinh thần trong suốt cuộc đời. Do vậy, đừng quá để ý đến vóc dáng mà hạn chế cho bé bú nhé.
Nên cho bé ăn khoảng 2 đến 3 tiếng một lần hoặc ít nhất cũng phải 8 lần trong 1 ngày.  Việc cho con bú bao nhiêu và cách bú thế nào cũng cần phải qua sự chỉ dẫn của những người có chuyên môn. Nếu bạn sinh đôi trở lên, thì hãy tìm cách để được nguồn giúp đỡ sữa mẹ thay thế từ cơ quan y tế hoặc người thân, hàng xóm nếu có thể.
Lời khuyên cho mẹ:
Đây là thời gian thay đổi khá lớn đối với bạn. Bạn sẽ đôi lúc cảm thấy mệt mỏi và khó thích nghi. Tuy nhiên, tình yêu thương đối với đứa con thiên thần sẽ giúp bạn dễ dàng thổi bay tất cả những căng thẳng ấy.
Em bé có thể bị cúm 1 đến 2 ngày vì sự thay đổi hooc-mon từ nguồn cung cấp qua cơ thể mẹ đến khi độc lập ở bên ngoài.
 
Đừng quá lo lắng khi thấy bé có biểu hiện gì đó hơi lệch lạc một chút so với mốc đo sự phát triển, bởi mỗi cơ thể lại có những đặc điểm riêng. Tốt nhất, mọi vấn đề lo lắng của bạn đều phải được thông qua sự tư vấn của những người có chuyên môn. 
