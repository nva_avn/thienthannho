﻿1. Kiểm tra tổng quát
  Trước khi mang thai cần kiểm tra sức khỏe tổng quát. Những bệnh lý như tim mạch, tiểu đường, cao huyết áp, bệnh lý tuyến giáp, thiếu máu … cần được điều trị ổn định trước khi mang thai.
