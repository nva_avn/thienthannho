﻿5. Vai trò của tập thể dục
  Tập thể dục đều đặn rất tốt cho quá trình mang thai và khi "lâm bồn". Tập thể dục ở cường độ vừa phải thì có lợi nhưng quá mức có thể gây ra sảy thai. 
  Nên tập thói quen này trước khi muốn mang thai nếu bạn thuộc tuýp người "ít vận động". Duy trì chế độ tập thể dục nhẹ nhàng trong suốt quá trình mang thai. Đi bộ hàng ngày là một phương pháp đơn giản và hiệu quả. 
  Trao đổi với bác sĩ về các loại hình thể dục và kế hoạch tập thể dục của bạn.
