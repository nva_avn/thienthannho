﻿Thai kỳ tuần 35
Bé của bạn đã sẵn sàng chào đời trong tuần này và nếu có bất kỳ vấn đề gì cũng rất dễ điều trị.
Tuần thai thứ 35 và những tuần tiếp theo, em bé của bạn sẽ chỉ tập trung vào việc phát triển cân nặng, chuẩn bị cho ngày ra khỏi bụng mẹ. Bé của bạn đã sẵn sàng chào đời trong tuần này và nếu có bất kỳ vấn đề gì cũng rất dễ điều trị. Còn mẹ, hãy chăm sóc bản thân không ngừng để đảm bảo cả tâm, sinh lý trước khi sinh con.
 
Thay đổi ở bé trong tuần thai thứ 35
 
Đến thời điểm này bé yêu của bạn nặng khoảng 2,5kg và cao khoảng 45cm. Bé không có khả năng để nhào lộn nữa do không còn nhiều khoảng trống trong bụng mẹ để bé cử động, nhưng bé sẽ đá vào bụng bạn nhiều hơn.
Phổi của bé đã hầu như hoàn thiện và sẵn sàng cho việc hít thở không khí bên ngoài rồi. Bé có xu hướng phát triển cân nặng là chủ yếu. Lượng nước ối trong tuần này đã giảm đi khá nhiều. Lớp lông tơ bao phủ cơ thể bé - tức chất sáp bao phủ làn da của bé đang bắt đầu rụng dần đi do bé đã ngâm trong nước ối gần 9 tháng. Việc nuốt nước ối ở trẻ vẫn tiếp tục xảy ra cho đến tuần này.
Hệ bài tiết hoạt động khá nhạy bén, bé đã có thể thải ra phân su- một hỗn hợp màu đen bạn sẽ nhìn thấy trong lần thay tã đầu của bé. Bé lúc này sẽ nằm ở tư thế đầu chúc xuống gần với cổ tử cung của mẹ.
 
Thay đổi ở mẹ trong tuần thai thứ 35
 
Tuần này, rốn của mẹ cách đỉnh tử cung khoảng 15 cm, từ đỉnh tử cung tới khớp dính là 35 cm. Đến tuần này bạn vẫn tiếp tục tăng cân, tuy nhiên có vẻ tăng chậm hơn so với những tuần trước.  Hãy giữ mức tăng cân cho phù hợp bạn nhé, tránh thừa cân gây khó sinh.
Thời gian này, chân tay bạn luôn phải chịu những cơn tê buốt hoặc râm ran như kiến bò. Đây là một biểu hiện do áp lực của thai nhi lên các dây thần kinh ở khu vực này. Cảm giác này sẽ giảm dần khi bé chào đời. Hãy thư giãn, nghỉ ngơi thật tốt để giảm bớt những khó chịu có thể gặp phải.
Bạn có thể massage những chỗ bị tê, tránh đứng lâu một chỗ, tránh ngồi vắt chân cao, hãy thường xuyên đi lại vận động nhẹ nhàng. Nếu cảm thấy ngày càng khó chịu thì bạn cần trao đổi với bác sĩ ngay.
 
Chế độ dinh dưỡng
 
Mẹ nên hạn chế ăn thức ăn nguội hoặc đông lạnh. Những loại thực phẩm này khiến bạn và bé tăng nguy cơ mắc một số bệnh lây lan. Ngoài ra các loại thực phẩm chứa nhiều mỡ, đường bạn cũng vẫn nên tránh trong giai đoạn này để tránh mắc phải các bệnh như tiểu đường hay thừa cân…
Theo kinh nghiệm dân gian, bạn có thể ăn thêm cá chép cho em bé có làn môi đỏ, uống nước dừa cho làn da bé trằng trẻo, ăn rau dền cho làn da bé hồng hào mịn màng, hay ăn trứng ngỗng cho trẻ thông minh…
Bên cạnh đó, bạn cũng không nên bỏ qua các loại rau củ quả chứa nhiều chất xơ trong các bữa ăn hàng ngày. Những loại thực phẩm này giúp cơ thể bạn được nhẹ nhõm, thoải mái. Sữa dành cho bà bầu hay bất kì loại sữa nào bạn thích có chứa nhiều canxi- các chất có lợi cho sự phát triển thể chất và trí não của bé, luôn là lựa chọn tối ưu cho mẹ bầu khi mang thai.
Uống nước nhiều cũng giúp cơ thể bạn loại bỏ được những độc tố trong thời kì mang thai, giúp bạn tránh được những cơn ợ nóng, táo bón…Do đó bạn nên chú ý, uống đủ 8 ly nước mỗi ngày để được khỏe mạnh trong suốt thai kì nhé.
 
Những bệnh thường gặp
 
Tuần này bạn vẫn phải chịu đựng chứng đau đầu chóng mặt. Nguyên nhân của các triệu chứng này là do thai nhi trong bụng bạn quá lớn, chèn ép lên các dây thần kinh cũng như mạch máu làm giảm độ lưu thông của máu lên não. Do đó bạn nên tránh thay đổi tư thế đột ngột để tránh bị ngã nhé.
Các bệnh về răng miệng trong thời kì này cũng là một điểm mà bạn cần lưu ý khi mang thai. Do các hooc mon thai nghén, một số vi khuẩn đường miệng hoạt động tích cực gây ra các vấn đề cho răng miệng của bạn, đặc biệt là viêm lợi.
Bên cạnh đó, bạn nên chú ý đến cách chăm sóc răng miệng một chút, thay đổi bàn chải đánh răng, loại kem đánh răng phù hợp là một trong những cách đơn giản nhất để giảm tránh nguy cơ mắc các bệnh về răng miệng.
GIãn tĩnh mạch khiến xuất hiện những vết đỏ tím, hoặc phù nề do cơ thể bạn tích nước cũng vẫn xuất hiện trong tuần này. Vận động nhẹ nhàng, massage thường xuyên sẽ giúp bạn cảm thấy khỏe hơn đấy.
 
Bố mẹ nên làm
 
Vẫn duy trì việc tới thăm khám bác sĩ để theo dõi tình hình sức khỏe của cả mẹ và bé để chủ động có biện pháp đối phó khi có trường hợp bất thường xảy ra.
Bố mẹ nên thường xuyên nói chuyện với con để làm quen với việc bé sắp xuất hiện trong tổ ấm của mình. Bố mẹ cũng có thể tận dụng lúc này để có thể bàn bạc kế hoạch nuôi dạy con sao cho tốt, nên làm gì sau khi sinh bé, bố có thể giúp gì cho mẹ…
Bạn vẫn nên tiếp tục thực hiện những bài tập nhẹ nhàng, đi dạo vào buổi tối hoặc sáng, đến các lớp học tiền sản để sẵn sàng sức khỏe cũng như kỹ năng nuôi con bạn. 
