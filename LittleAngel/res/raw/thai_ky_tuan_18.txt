﻿Thai kỳ tuần 18
Lúc này bé đã có thể ý thức được với tiếng ồn từ thế giới bên ngoài. Giác quan của bé đang phát triển mạnh mẽ. Bộ não của bé đã bắt đầu phân chia các khu vực chuyên biệt cho các giác quan: khứu giác, vị giác, thính giác, thị giác, và xúc giác.
Thai kỳ tuần 18 các giác quan của bé đã phát triển mạnh, thị giác của bé đang tiến triển nhanh, võng mạc nhạy cảm hơn với ánh sáng, tai cũng đã hoàn thiện những đường nét cuối cùng và sụn mềm đã bắt đầu hình thành giữa các đầu xương.
 
Thay đổi của thai nhi tuần thứ 18
 
Trong tuần này bé dài khoảng 13 cm từ đỉnh đầu tới mông, và nặng xấp xỉ khoảng 140g. Lúc này bé đã có thể ý thức được với tiếng ồn từ thế giới bên ngoài. Giác quan của bé đang phát triển mạnh mẽ. Bộ não của bé đã bắt đầu phân chia các khu vực chuyên biệt cho các giác quan: khứu giác, vị giác, thính giác, thị giác, và xúc giác. Thị giác của bé cũng đang tiến triển tốt, võng mạc nhạy cảm hơn với ánh sáng, hoặc khi tiếp xúc với ánh sáng.
Thận của bé tiếp tục sản xuất ra nước tiểu và tóc bé bắt đầu mọc ra. Ngoài ra, ở tuần thai này bé còn thể hiện nhiều biểu hiện đáng yêu khác như ngáp, nuốt, bú, nấc và một số cử chỉ trên khuôn mặt. Cánh tay và chân của bé đã cân đối so với phần còn lại của cơ thể. Bé đã có thể siết chặt lấy hai bàn tay khi tình cờ chạm vào nhau. Tuy nhiên, hiện tượng này không được tạo ra dưới sự kiểm soát của não bé, đây hoàn toàn chỉ là bộc phát.
 
Thay đổi ở mẹ khi mang thai tuần thứ 18
 
Bạn có thể cảm nhận được tử cung của mình nằm ngay dưới rốn. Tử cung lúc này đã lớn hơn nhiều so với tuần trước. Hệ thống tim mạch của bạn đang trải qua những thay đổi lớn và trong ba tháng này, huyết áp của bạn có lẽ sẽ thấp hơn bình thường.
Những thay đổi về tính linh động của khớp xương có thể là một nguyên nhân góp phần vào sự thay đổi trong dáng điệu của bạn và sự khó chịu ở vùng lưng dưới dẫn đến các cơn đau lưng cho các thai phụ.
Lòng bàn tay chuyển sang màu đỏ đó do sự gia tăng estrogen. Nám da cũng bắt đầu xuất hiện ở má, mũi và với mật độ nhiều hơn. Bạn cũng có thể nhận thấy một số điểm trên cơ thể như núm vú, tàn nhang, sẹo, nách, đùi, hay cả vùng kín bị thâm. Tuy nhiên các vết thâm này sẽ biến mất sau khi bạn sinh, chính vì thế bạn không cần thiết phải lo lắng. Bên cạnh đó bạn cũng nên chú ý, giữ gìn làn da bạn khỏi tiếp xúc lâu với nắng, tránh da xám, sạm màu sau khi bạn sinh.
 
Trong giai đoạn này, cân nặng của bạn đang tích cực tăng. Trung bình một thai phụ nếu tăng ở khoảng 5-6kgs trong tuần này là bình thường. Nếu cân nặng của bạn tăng quá nhanh, bạn nên kìm hãm lại sớm như có thể vì việc này có khả năng ảnh hưởng tới quá trình sinh đẻ sau này.
 
Các bệnh thường gặp
 
Các biểu hiện của ốm nghén đương nhiên sẽ không gây khó chịu nhiều cho bạn trong tuần này nữa, nên bạn có thể thoải mái ăn thêm các loại thực phẩm có lợi cho sức khỏe bạn và bé.
Các thai phụ sẽ gặp phải chứng đau nhói thoáng qua không thường xuyên. Đặc biệt khi mẹ bầu thay đổi vị trí hoặc hoạt động nhiều sau một ngày. Đây là biểu hiện của chứng đau dây thần kinh tọa. Nguyên nhân là do các dây chằng nâng đỡ tử cung của mẹ đang bị kéo giãn để nâng đỡ cái thai trong bụng ngày càng tăng về kích thước và cân nặng. Tuy nhiện bạn cũng không nên lo lắng nhiều về những cơn đau này. Nếu các triệu chứng đau nhói, khó chịu này kéo dài, bạn nên đến bác sĩ để được khám chữa kịp thời.
Một số thai phụ có sức khỏe yếu thường cảm thấy chóng mặt khi thay đổi tư thế. Nguyên nhân là do huyết áp của bạn thấp hơn bình thường trong giai đoạn này. Bạn không nên lo lắng, hãy bổ sung thêm dưỡng chất đặc biệt là chất sắt, bạn có thể giảm bớt được tình trạng này.
 
Chế độ dinh dưỡng
 
Trong tuần thai thứ 18, bộ não của bé đang phát triển khá mạnh, do đó bạn nên bổ sung các chất có lợi cho sự phát triển trí não như các thực phẩm chứa canxi cao, chất khoáng…Bạn nên tránh ăn các loại hải sản như cá kiếm, cá thu và cá kình vì các loại này có ảnh hưởng không tốt tới sự phát triển toàn diện bộ não của bé.
Ngoài bổ sung các chất xơ trong bữa ăn hàng ngày, bạn nên chú ý ăn nhiều rau xanh hoặc các loại hoa quả có màu đỏ tươi, nó sẽ giúp bạn giảm bớt các vết thâm cũng như các sắc tố gây nám da.
Lượng calo bổ sung cho cơ thể cũng không nên giảm bớt nếu như bạn giữ mức cân nặng tăng trong khoảng từ 5-6 kg. Đừng lo lắng nếu như cơ thể bạn đang phát phì, bạn cảm thấy thật nặng nề vì lúc này bạn không chỉ cung cấp dưỡng chất cho chỉ một cơ thể mà là hai.
Để giảm các triệu chứng chóng mặt bạn cần bổ sung lượng sắt cần thiết cho cơ thể bằng việc tìm đến các loại thức ăn có màu đỏ tươi như thịt, gan… hoặc các loại biệt dược chứa sắt cho bà bầu.
 
Bố mẹ cần làm
 
Trong tuần này, não bé đã có những phát triển vượt trội, các cơ quan xúc giác cũng đang dần hoàn thiện chức năng của chúng. Do đó bạn nên trao đổi thông tin với bé nhiều hơn. Có thể cho bé nghe các loại nhạc không lời với giai điệu nhẹ nhàng, hoặc nói chuyện với bé hằng ngày. Điều này có lợi ích trong việc kích thích khả năng tiếp nhận thông tin của bé cũng góp phần tạo mối quan hệ khăng khít giữa bố mẹ và thành viên mới sắp trào đời.
Cần phải thường xuyên thăm khám bác sĩ, đặc biệt là phải làm các xét nghiệm về mức đường huyết của mẹ.
Nên có chế độ ngủ nghỉ thích hợp cho mẹ, tốt nhất bạn nên dành ra tầm 15-30 phút ngủ trưa để cho cơ thể đỡ mệt mỏi cũng như thoát khỏi stress.
Điều quan trọng nữa là luôn quan tâm tới vệ sinh ăn uống như trước đó, bổ sung nhiều dưỡng chất cho sự phát triển của bé, tránh cho bé mắc phải các bệnh truyền nhiễm.
Bên cạnh đó, mẹ nên tham gia các lớp học tiền sản đầy đủ để có những kĩ năng thuần thục chăc sóc cho bé.
