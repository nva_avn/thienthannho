﻿Sự phát triển của bé hai tháng tuổi
Ở tháng thứ hai này, bé bắt đầu kiểm soát được cơ thể và có những sự thay đổi về mặt thể chất và tinh thần để thích nghi với thế giới bên ngoài. Mặc dù bé vẫn còn rất nhỏ và mong manh nhưng khi ôm ấp và nhìn vào mắt bé, các mẹ có thể cảm nhận được sự thay đổi đó.
Một số thay đổi đáng chú ý nhất trong giai đoạn này là:
 
Thay đổi về mặt thể chất:
 
Bé cứng cáp hơn, sự rung cơ của tháng đầu tiên từ từ biến mất, cơ cổ cũng cứng hơn, bé có thể ngóc đầu lên một góc 45 độ. Tay bé cử động uyển chuyển hơn, bé có thể với tay ra nắm những vật ở trước mặt và giữ chúng trong khoảng thời gian vài giây. Thị giác của bé so với giai đoạn một tháng tuổi nhanh nhạy hơn nhiều (ví dụ như khi nhìn lúc lắc đung đưa). Thính giác của bé cũng phát triển hơn.
 
Thời gian ngủ của bé ít hơn trước, có bé thức đến 10 tiếng một ngày. Ở tháng này, bé có thể bắt đầu ngủ một mạch đến sáng nhưng cũng có bé vẫn thức dậy 2-3 lần đòi bú nhưng dần dần thời gian ngủ và bú của bé cũng được ổn định.
 
Thay đổi về mặt tinh thần:
Bé hai tháng tuổi rất thích nhìn những vật thể, đồ chơi nhiều màu sắc, hình dạng. Vì thị giác đã phát triển hơn (bé có thể nhìn những vật trong khoảng cách 40cm) giúp bé có thể nhìn xa hơn và nhìn xuống đất (vì cổ đã cứng hơn) nên bé thích thú nhìn những vật thể xung quanh.
 
Do bé đã tương đối kiểm soát được cơ thể mình nên phản xạ nắm tự phát (tay nắm tròn) dần không còn, bé thích thú với việc sử dụng đôi bàn tay của mình.
 
Cách thức giao tiếp:
 
Bé ở giai đoạn này giao tiếp với mọi người xung quanh bằng cách ra những ám hiệu.
 
Các bố mẹ có thể dễ dàng nhận biết và hiểu bé muốn gì thông qua những ám hiệu đó. Ví dụ: bé nhìn chăm chú, huơ tay chân, cười, kêu ư ư và có những biểu hiện tươi vui nghĩa là bé đang cảm thấy thích thú với việc gì đó. Còn nếu như bé quay đầu đi hướng khác, ngáp, cau mày, quấy, khóc nghĩa là bé không thấy hứng thú với việc đó.
 
Khi bé thấy thích thú và vui vẻ, bé sẽ cười và miệng phát ra âm thanh với cường độ ngày càng tăng. Đây sẽ là quãng thời gian đáng nhớ cho cả mẹ và bé khi lần đầu tiên mẹ nhìn thấy nụ cười thực sự trên môi bé.
 
Bé cũng có nhiều tiến bộ về ngôn ngữ, bé lắng nghe mẹ trò chuyện, quan sát miệng và nhìn cách lưỡi mẹ di chuyển khi nói.  Khi bé nhìn chăm chú  một vật gì đó nghĩa là bé đang muốn nói về đồ vật đó hoặc cũng có thể bé thấy đó là những thứ mới lạ và muốn khám phá chúng.
 
Lời khuyên cho các bà mẹ:
Thường xuyên nói chuyện nhẹ nhàng, ôm ấp, âu yếm bé
 
- Mua những đồ chơi nhiều màu sắc cho bé (đồ bằng nhựa như bóng mềm, thú bông)
 
- Đưa bé đi tiêm phòng và kiểm tra sức khỏe (cân, đo, kiểm tra mắt, tiêm chủng…cho bé)
 
- Khi bé ngủ, hãy đặt bé nằm ngửa để tránh hội chứng đột tử trẻ sơ sinh.
 
-Nếu thời tiết thuận lợi hãy đưa bé ra ngoài để bé làm quen với thế giới xung quanh.
 
- Khi cho bé nằm nôi, phía trên hãy treo những đồ chơi nhiều màu sắc để bé nhìn, vừa để luyện thị lực vừa để khuyến khích chân tay bé cử động nhiều hơn. 
 
- Thường xuyên cắt móng tay, móng chân cho bé.
 
