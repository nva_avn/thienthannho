﻿Lời khuyên
Nuôi con bằng sữa mẹ hoàn toàn trong 6
tháng đầu (180 ngày tuổi)
 Nên cho trẻ bú sữa non sớm trong vòng 1 giờ sau sinh
 Cho trẻ bú hoàn toàn và theo nhu cầu trong 6 tháng
đầu mà không cần cho trẻ ăn bất cứ một loại thức ăn,
nước uống nào
 Nguy cơ có thể xẩy ra nếu trẻ không được nuôi hoàn
toàn bằng sữa mẹ trong 6 tháng đầu:
 Trẻ dễ bị suy dinh dưỡng
 Trẻ mắc bệnh vì
thiếu các yếu tố
bảo vệ có trong
sữa mẹ
 Trẻ dễ bị tiêu chảy
do thức ăn bổ
sung không sạch
hay khó tiêu
 Trẻ có thể bị dị ứng với
thức ăn không phù hợp
 Tăng nguy cơ mang thai của bà mẹ