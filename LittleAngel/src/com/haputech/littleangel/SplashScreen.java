package com.haputech.littleangel;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;

/**
 * Man hinh welcome
 * 
 * @author NgVietAn
 * 
 */
public class SplashScreen extends Activity {

	private ProgressBar proBar;
	private int progress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
		proBar = (ProgressBar) findViewById(R.id.proBar);
		progress = 0;
		Thread logoTimer = new Thread() {
			public void run() {
				try {
					int logoTimer = 0;
					while (logoTimer < 3000) {
						sleep(30);
						logoTimer = logoTimer + 30;
						progress++;
						proBar.setProgress(progress);
					}
					startActivity(new Intent(
							"com.haputech.littleangel.CLEARSCREEN"));
				}

				catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				finally {
					finish();
				}
			}
		};

		logoTimer.start();
	}
}
