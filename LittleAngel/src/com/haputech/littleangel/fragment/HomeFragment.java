package com.haputech.littleangel.fragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.db.DatabaseBaby;
import com.haputech.littleangel.fragment.book.BookViewFragment;
import com.haputech.littleangel.object.Baby;
import com.haputech.littleangel.util.ConfigApp;
import com.haputech.littleangel.util.ImageHelper;

/**
 * Home fragment
 * 
 * @author NgVietAn
 * 
 */
public class HomeFragment extends AppFragment {

	private TextView tvName, tvBirthday, tvGender;
	private ImageView ivBabyPic;
	private ListView lvInfor;

	private int babyId;
	private DatabaseBaby db;
	private Baby baby;

	private String[] time;
	private int[] phattrien, dinhduong;
	private HomeAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		((AppFragmentActivity) getActivity()).setActionBarTitle(getResources()
				.getString(R.string.home));
		View view = inflater.inflate(R.layout.fragment_main, null);
		initComponents(view);
		initData();
		setAdapter();
		addListener();
		setHasOptionsMenu(true);
		return view;
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		tvName = (TextView) v.findViewById(R.id.tvBabyName);
		tvBirthday = (TextView) v.findViewById(R.id.tvBabyBirthday);
		tvGender = (TextView) v.findViewById(R.id.tvBabyGender);

		ivBabyPic = (ImageView) v.findViewById(R.id.ivBabyPic);

		lvInfor = (ListView) v.findViewById(R.id.lvInfor);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();
		db = new DatabaseBaby(getActivity().getApplicationContext());

		Resources res = getActivity().getResources();
		SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");

		SharedPreferences pref = getActivity().getSharedPreferences(
				ConfigApp.SHARED_PREF, Context.MODE_PRIVATE);
		babyId = pref.getInt(ConfigApp.BABY_ID, 1);

		baby = db.getBaby(babyId);

		tvName.setText(baby.getName());
		if (baby.getSex() == -1) {
			tvBirthday.setText(res.getString(R.string.preg) + " : "
					+ formatDate.format(baby.getPregnancy()));
			tvGender.setText("");
		} else {
			tvBirthday.setText(res.getString(R.string.birthday) + " : "
					+ formatDate.format(baby.getBirthday()));
			if (baby.getSex() == Baby.MALE)
				tvGender.setText(res.getString(R.string.male));
			else
				tvGender.setText(res.getString(R.string.female));
		}

		if (!baby.getImage().equals("")) {
			Bitmap bitmap = BitmapFactory.decodeFile(baby.getImage());
			ivBabyPic.setImageBitmap(ImageHelper.getResizedBitmapByWidth(
					bitmap, 100));
		}

		if (baby.getSex() == -1) {
			time = new String[41];
			for (int i = 0; i < 41; i++) {
				time[i] = getResources().getString(R.string.week_old) + " "
						+ (i + 1);
			}
		} else {
			time = new String[39];
			for (int i = 0; i < 4; i++) {
				time[i] = getResources().getString(R.string.week_old) + " "
						+ (i + 1);
			}
			for (int i = 4; i < 39; i++) {
				time[i] = getResources().getString(R.string.month_old) + " "
						+ (i - 2);
			}
		}
		getBook();
	}

	private void getBook() {
		TypedArray dev, nul;
		if (baby.getSex() == -1) {
			dev = getResources().obtainTypedArray(R.array.phattrieng_mangthai);
		} else {
			dev = getResources().obtainTypedArray(R.array.phattrien_sausinh);
		}
		int len = dev.length();
		phattrien = new int[len];
		for (int i = 0; i < len; i++) {
			phattrien[i] = dev.getResourceId(i, 0);
			dev.recycle();
		}
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub
		adapter = new HomeAdapter(getActivity(),
				android.R.layout.simple_list_item_1, time);
		lvInfor.setAdapter(adapter);

		// move list
		int old = 0;
		Calendar today = Calendar.getInstance();
		if (baby.getSex() == -1) {
			old = (int) ((today.getTimeInMillis() - baby.getPregnancy()) / 604800000);
		} else {
			old = (int) ((today.getTimeInMillis() - baby.getBirthday()) / 604800000);
			if (old >= 4) {
				old = old / 4 + 1;
			}
		}
		Log.i("Tuoi", old + "");
		lvInfor.setSelection(old);
	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == android.R.id.home) {
			((AppFragmentActivity) getActivity()).finish();
		}
		return super.onOptionsItemSelected(item);
	}

	class HomeAdapter extends ArrayAdapter<String> {

		Context context;
		String[] time;

		public HomeAdapter(Context context, int textViewResourceId,
				String[] time) {
			super(context, textViewResourceId, time);
			this.context = context;
			this.time = time;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View listView;
			listView = null;
			convertView = null;
			if (convertView == null) {
				listView = new View(context);
				listView = inflater.inflate(R.layout.row_main_quick, null);
				TextView tvTime = (TextView) listView.findViewById(R.id.tvWeek);
				ImageView btBook = (ImageView) listView
						.findViewById(R.id.btBook);
				ImageView btNutrition = (ImageView) listView
						.findViewById(R.id.btNutrition);

				tvTime.setText(time[position]);

				btBook.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						SharedPreferences pref = getActivity()
								.getSharedPreferences(ConfigApp.SHARED_PREF,
										Context.MODE_PRIVATE);
						Editor editor = pref.edit();
						editor.putInt(ConfigApp.BOOK_NAME, phattrien[position]);
						editor.commit();
						Fragment book = new BookViewFragment();
						AppFragmentActivity act = (AppFragmentActivity) getActivity();
						act.switchContent(book);

					}
				});

				btNutrition.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Toast.makeText(context, "Cap nhat sau !",
								Toast.LENGTH_SHORT).show();
					}
				});
			}
			return listView;
		}
	}
}
