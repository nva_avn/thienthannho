package com.haputech.littleangel.fragment;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;

/**
 * Date picker dialog fragment. Use when show date picker in fragment
 * 
 * @author NgVietAn
 * 
 */
public class DatePickerDialogFragment extends DialogFragment {

	private Calendar mCalendar = Calendar.getInstance();

	private Fragment mFragment;

	public DatePickerDialogFragment(Fragment mFragment) {
		super();
		this.mFragment = mFragment;
	}

	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return new DatePickerDialog(getActivity(),
				(OnDateSetListener) mFragment, mCalendar.get(Calendar.YEAR),
				mCalendar.get(Calendar.MONTH),
				mCalendar.get(Calendar.DAY_OF_MONTH));
	}
}
