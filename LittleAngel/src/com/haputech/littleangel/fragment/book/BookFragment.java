package com.haputech.littleangel.fragment.book;

import java.util.ArrayList;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.adapter.BookAdapter;
import com.haputech.littleangel.fragment.AppFragment;
import com.haputech.littleangel.util.ConfigApp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

/**
 * Type of book Fragment
 * 
 * @author NgVietAn
 * 
 */
public class BookFragment extends AppFragment implements OnItemClickListener {

	private ListView lvBook;
	private ArrayList<String> book;
	private BookAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		((AppFragmentActivity) getActivity()).setActionBarTitle(getResources()
				.getString(R.string.book));
		View view = inflater.inflate(R.layout.fragment_list_view, null);
		initComponents(view);
		initData();
		setAdapter();
		addListener();
		return view;
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		lvBook = (ListView) v.findViewById(R.id.lvContent);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();
		book = new ArrayList<String>();
		book.add(before_preg);
		book.add(pregnancy);
		book.add(after_preg);

		adapter = new BookAdapter(getActivity().getApplicationContext(),
				android.R.layout.simple_list_item_1, book);
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub
		lvBook.setAdapter(adapter);
	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub
		lvBook.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

		// luu thong tin lua chon
		SharedPreferences pref = getActivity().getSharedPreferences(
				ConfigApp.SHARED_PREF, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString(ConfigApp.TYPE_BOOK_TIME, book.get(position));
		editor.putInt(ConfigApp.TYPE_BOOK, 0);
		editor.commit();

		// chuyen fragment
		Fragment fragment = new BookListFragment();
		AppFragmentActivity act = (AppFragmentActivity) getActivity();
		act.switchContent(fragment);
	}

}
