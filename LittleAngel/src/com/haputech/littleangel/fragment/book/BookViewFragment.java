package com.haputech.littleangel.fragment.book;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.fragment.AppFragment;
import com.haputech.littleangel.util.ConfigApp;

public class BookViewFragment extends AppFragment {

	private TextView tvSubject, tvContent;
	private String subject, content;
	private SharedPreferences pref;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_book_view, null);
		initComponents(view);
		initData();
		setHasOptionsMenu(true);
		return view;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == android.R.id.home) {
			Fragment fragment = new BookListFragment();
			AppFragmentActivity act = (AppFragmentActivity) getActivity();
			act.switchContent(fragment);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		tvSubject = (TextView) v.findViewById(R.id.tvSubject);
		tvContent = (TextView) v.findViewById(R.id.tvContent);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();

		pref = getActivity().getSharedPreferences(ConfigApp.SHARED_PREF,
				Context.MODE_PRIVATE);
		int file = pref.getInt(ConfigApp.BOOK_NAME, 0);
		subject = "";
		content = "";
		String data;
		data = "";
		InputStream in = getResources().openRawResource(file);
		InputStreamReader inreader = new InputStreamReader(in);
		BufferedReader bufreader = new BufferedReader(inreader);
		StringBuilder builder = new StringBuilder();
		if (in != null) {
			try {
				subject = bufreader.readLine();
				while ((data = bufreader.readLine()) != null) {
					builder.append(data);
					builder.append("\n");
				}
				in.close();
				content = builder.toString();
			} catch (IOException ex) {
				Toast.makeText(getActivity().getApplicationContext(),
						ex.getMessage().toString(), Toast.LENGTH_SHORT).show();
			}
		}

		tvSubject.setText(subject);
		tvContent.setText(content);
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub

	}

}
