package com.haputech.littleangel.fragment.book;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.adapter.BookAdapter;
import com.haputech.littleangel.fragment.AppFragment;
import com.haputech.littleangel.util.ConfigApp;

/**
 * List books fragment
 * 
 * @author NgVietAn
 * 
 */
public class BookListFragment extends AppFragment {

	private Spinner spBook;
	private ListView lvBook;

	private BookAdapter adapter;
	private ArrayList<String> book_name;
	private String[] book_category;
	private int[] book_category_value, book_value;
	private String type;
	private int typeBook;

	private SharedPreferences pref;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_book_list, null);
		initComponents(view);
		initData();
		setAdapter();
		addListener();
		setHasOptionsMenu(true);

		((AppFragmentActivity) getActivity()).setActionBarTitle(type);
		return view;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == android.R.id.home) {
			Fragment fragment = new BookFragment();
			AppFragmentActivity act = (AppFragmentActivity) getActivity();
			act.switchContent(fragment);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		lvBook = (ListView) v.findViewById(R.id.lvBook);
		spBook = (Spinner) v.findViewById(R.id.spBookCategory);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();

		pref = getActivity().getSharedPreferences(ConfigApp.SHARED_PREF,
				Context.MODE_PRIVATE);
		type = pref.getString(ConfigApp.TYPE_BOOK_TIME, before_preg);
		if (type.equals(pregnancy)) {
			book_category = getResources().getStringArray(R.array.mangthai);
			getArray(R.array.mangthai_int);
		} else if (type.equals(before_preg)) {
			book_category = getResources()
					.getStringArray(R.array.truocmangthai);
			getArray(R.array.truocmangthai_int);
		} else if (type.equals(after_preg)) {
			book_category = getResources().getStringArray(R.array.sausinh);
			getArray(R.array.sausinh_int);
		}

		typeBook = pref.getInt(ConfigApp.TYPE_BOOK, 0);

		book_name = new ArrayList<String>();
		adapter = new BookAdapter(getActivity(),
				android.R.layout.simple_list_item_1, book_name);
		if (typeBook != 0) {
			createBookList(book_category_value[typeBook]);
		} else {
			createBookList(book_category_value[0]);
		}
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub
		lvBook.setAdapter(adapter);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity()
				.getApplicationContext(), android.R.layout.simple_spinner_item,
				book_category);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spBook.setAdapter(adapter);

		if (typeBook != 0)
			spBook.setSelection(typeBook);
	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub
		spBook.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> av, View v, int pos,
					long id) {
				createBookList(book_category_value[pos]);

				SharedPreferences.Editor editor = pref.edit();
				editor.putInt(ConfigApp.TYPE_BOOK, pos);
				editor.commit();

				adapter.notifyDataSetChanged();
			}

			@Override
			public void onNothingSelected(AdapterView<?> av) {
				adapter.notifyDataSetChanged();
			}

		});

		lvBook.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
				SharedPreferences.Editor editor = pref.edit();
				editor.putInt(ConfigApp.BOOK_NAME, book_value[pos]);
				editor.commit();

				Fragment fragment = new BookViewFragment();
				AppFragmentActivity act = (AppFragmentActivity) getActivity();
				act.switchContent(fragment);
			}

		});
	}

	/**
	 * Get array String
	 * 
	 * @param array
	 */
	private void getArray(int array) {
		TypedArray ar = getResources().obtainTypedArray(array);
		int len = ar.length();
		book_category_value = new int[len];
		for (int i = 0; i < len; i++) {
			book_category_value[i] = ar.getResourceId(i, 0);
			ar.recycle();
		}
	}

	/**
	 * Get array int
	 * 
	 * @param array
	 */
	private void getValue(int array) {
		TypedArray ar = getResources().obtainTypedArray(array);
		int len = ar.length();
		book_value = new int[len];
		for (int i = 0; i < len; i++) {
			book_value[i] = ar.getResourceId(i, 0);
			ar.recycle();
		}
	}

	/**
	 * Create list books
	 * 
	 * @param resArray
	 */
	private void createBookList(int resArray) {
		getValue(resArray);
		book_name.clear();
		String data;
		for (int i = 0; i < book_value.length; i++) {
			data = "";
			InputStream in = getResources().openRawResource(book_value[i]);
			InputStreamReader inreader = new InputStreamReader(in);
			BufferedReader bufreader = new BufferedReader(inreader);
			if (in != null) {
				try {
					data = bufreader.readLine();
					book_name.add(data);
					in.close();
				} catch (IOException ex) {
					Toast.makeText(getActivity().getApplicationContext(),
							ex.getMessage().toString(), Toast.LENGTH_SHORT)
							.show();
				}
			}
		}
	}

}
