package com.haputech.littleangel.fragment;

import java.util.Calendar;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;

/**
 * Time picker dialog fragment. Use it inside fragment.
 * 
 * @author NgVietAn
 * 
 */
public class TimePickerDialogFragment extends DialogFragment {
	private Calendar mCalendar = Calendar.getInstance();

	private Fragment mFragment;

	public TimePickerDialogFragment(Fragment mFragment) {
		super();
		this.mFragment = mFragment;
	}

	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return new TimePickerDialog(getActivity(),
				(OnTimeSetListener) mFragment,
				mCalendar.get(Calendar.HOUR_OF_DAY),
				mCalendar.get(Calendar.MINUTE), true);
	}
}
