package com.haputech.littleangel.fragment;

import java.util.Calendar;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

import com.haputech.littleangel.R;
import com.haputech.littleangel.communicate.OnTimeCommitListener;

/**
 * Date and time picker dialog fragment. Use inside fragment
 * 
 * @author NgVietAn
 * 
 */
public class DateTimePickerFragment extends DialogFragment implements
		OnClickListener, OnDateChangedListener, OnTimeChangedListener {

	private Button btCancel, btSave;
	private DatePicker datePicker;
	private TimePicker timePicker;
	private TextView tvContent;

	private OnTimeCommitListener mFragment;

	public Calendar mCalendar = Calendar.getInstance();
	public String content = "";

	public DateTimePickerFragment(OnTimeCommitListener mFragment) {
		super();
		this.mFragment = mFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.date_time_picker, null);
		initComponents(view);
		initData();
		addListener();
		return view;
	}

	/**
	 * Khoi tao component
	 * 
	 * @param v
	 */
	protected void initComponents(View v) {
		tvContent = (TextView) v.findViewById(R.id.tvContent);
		btCancel = (Button) v.findViewById(R.id.btCancel);
		btSave = (Button) v.findViewById(R.id.btSave);
		datePicker = (DatePicker) v.findViewById(R.id.datePicker);
		timePicker = (TimePicker) v.findViewById(R.id.timePicker);
	}

	/**
	 * Khoi tao du lieu
	 */
	protected void initData() {
		tvContent.setText(content);

		if (null == mCalendar)
			mCalendar = Calendar.getInstance();

		datePicker.init(mCalendar.get(Calendar.YEAR),
				mCalendar.get(Calendar.MONTH),
				mCalendar.get(Calendar.DAY_OF_MONTH), this);
		datePicker.setMinDate(System.currentTimeMillis() - 1000);
		timePicker.setCurrentHour(mCalendar.get(Calendar.HOUR_OF_DAY));
		timePicker.setCurrentMinute(mCalendar.get(Calendar.MINUTE));
	}

	/**
	 * Them su kien
	 */
	protected void addListener() {
		btSave.setOnClickListener(this);
		btCancel.setOnClickListener(this);
		timePicker.setOnTimeChangedListener(this);

	}

	@Override
	public void onDateChanged(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		// TODO Auto-generated method stub
		mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
		mCalendar.set(Calendar.MONTH, monthOfYear);
		mCalendar.set(Calendar.YEAR, year);
	}

	@Override
	public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
		// TODO Auto-generated method stub
		mCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
		mCalendar.set(Calendar.MINUTE, minute);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btSave:
			mFragment.OnTimeCommit(mCalendar.getTime());
			this.dismiss();
			break;
		case R.id.btCancel:
			this.dismiss();
			break;
		}
	}

}
