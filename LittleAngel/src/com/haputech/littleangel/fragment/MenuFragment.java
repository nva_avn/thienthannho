package com.haputech.littleangel.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.db.DatabaseBaby;
import com.haputech.littleangel.fragment.book.BookFragment;
import com.haputech.littleangel.fragment.game.GameFragment;
import com.haputech.littleangel.fragment.health.HealthFragment;
import com.haputech.littleangel.fragment.hotcall.HotCallFragment;
import com.haputech.littleangel.fragment.remind.RemindFragment;
import com.haputech.littleangel.util.ConfigApp;

/**
 * Menu fragment
 * 
 * @author NgVietAn
 * 
 */
public class MenuFragment extends AppFragment implements OnClickListener {

	private ImageView btnHome, btnHotCall, btnBook, btnRemind, btnHealth,
			btnGame;

	private int babyId;
	private DatabaseBaby db;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_menu, null);
		initComponents(view);
		initData();
		addListener();
		return view;
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		btnHome = (ImageView) v.findViewById(R.id.btnHome);
		btnHotCall = (ImageView) v.findViewById(R.id.btnHotCall);
		btnBook = (ImageView) v.findViewById(R.id.btnBook);
		btnRemind = (ImageView) v.findViewById(R.id.btnRemind);
		btnHealth = (ImageView) v.findViewById(R.id.btnHealth);
		btnGame = (ImageView) v.findViewById(R.id.btnGame);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		db = new DatabaseBaby(getActivity().getApplicationContext());
		SharedPreferences pref = getActivity().getSharedPreferences(
				ConfigApp.SHARED_PREF, Context.MODE_PRIVATE);
		babyId = pref.getInt(ConfigApp.BABY_ID, 1);
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub
		btnHome.setOnClickListener(this);
		btnHotCall.setOnClickListener(this);
		btnBook.setOnClickListener(this);
		btnRemind.setOnClickListener(this);
		btnHealth.setOnClickListener(this);
		btnGame.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Fragment fragment = null;
		int id = v.getId();
		if (id == R.id.btnHome) {
			fragment = new HomeFragment();
		} else if (id == R.id.btnHotCall) {
			fragment = new HotCallFragment();
		} else if (id == R.id.btnBook) {
			fragment = new BookFragment();
		} else if (id == R.id.btnRemind) {
			fragment = new RemindFragment();
		} else if (id == R.id.btnHealth) {
			if (db.getBaby(babyId).getSex() == -1) {
				Toast.makeText(getActivity(),
						getResources().getString(R.string.birth_use),
						Toast.LENGTH_SHORT).show();
			} else {
				fragment = new HealthFragment();
			}
		} else if (id == R.id.btnGame) {
			fragment = new GameFragment();
		}

		if (fragment != null) {
			switchFragment(fragment);
		}
	}

	/**
	 * Switch fragment when click menu
	 * 
	 * @param fragment
	 */
	private void switchFragment(Fragment fragment) {
		AppFragmentActivity act = (AppFragmentActivity) getActivity();
		act.switchContent(fragment);
	}
}
