package com.haputech.littleangel.fragment.remind;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlarmManager;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.PendingIntent;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.db.DatabaseBaby;
import com.haputech.littleangel.db.DatabaseRemind;
import com.haputech.littleangel.fragment.AppFragment;
import com.haputech.littleangel.fragment.DatePickerDialogFragment;
import com.haputech.littleangel.fragment.TimePickerDialogFragment;
import com.haputech.littleangel.object.Remind;
import com.haputech.littleangel.util.ConfigApp;
import com.haputech.littleangel.util.RemindReceiver;

public class ChambeAddFragment extends AppFragment implements
		OnDateSetListener, OnTimeSetListener {

	public static int ADD = 100;
	public static int EDIT = 101;

	private EditText edtCongViec, edtNgay, edtGio, edtGhichu;
	private DatabaseRemind db;
	private DatabaseBaby dbaby;

	public int type = ADD;
	public Remind old = null;
	private Calendar mCalendar = Calendar.getInstance();
	private SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
	private SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");

	private String title, content, date, time;
	private int id, babyId;

	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		// TODO Auto-generated method stub
		mCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
		mCalendar.set(Calendar.MINUTE, minute);
		time = formatTime.format(mCalendar.getTime());
		edtGio.setText(time);
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		// TODO Auto-generated method stub
		mCalendar.set(Calendar.YEAR, year);
		mCalendar.set(Calendar.MONTH, monthOfYear);
		mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
		date = formatDate.format(mCalendar.getTime());
		edtNgay.setText(date);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_add_remind, null);
		initComponents(view);
		initData();
		addListener();
		setHasOptionsMenu(true);
		return view;
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		edtCongViec = (EditText) v.findViewById(R.id.edtCongViec);
		edtNgay = (EditText) v.findViewById(R.id.edtNgay);
		edtGio = (EditText) v.findViewById(R.id.edtGio);
		edtGhichu = (EditText) v.findViewById(R.id.edtGhiChu);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();
		SharedPreferences pref = getActivity().getSharedPreferences(
				ConfigApp.SHARED_PREF, Context.MODE_PRIVATE);
		babyId = pref.getInt(ConfigApp.BABY_ID, 1);
		dbaby = new DatabaseBaby(getActivity());
		db = new DatabaseRemind(getActivity());

		if (type == ADD)
			id = db.getMaxID() + 1;
		else if (type == EDIT) {
			if (old != null) {
				id = old.getID();
				edtCongViec.setText(old.getTitle());
				edtGhichu.setText(old.getContent());

				mCalendar.setTimeInMillis(old.getTime());
				date = formatDate.format(mCalendar.getTime());
				edtNgay.setText(date);
				time = formatTime.format(mCalendar.getTime());
				edtGio.setText(time);
			}
		}
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub
		edtNgay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
				DialogFragment newFragment = new DatePickerDialogFragment(
						ChambeAddFragment.this);
				newFragment.show(ft, "dialog");
			}
		});
		edtGio.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
				DialogFragment newFragment = new TimePickerDialogFragment(
						ChambeAddFragment.this);
				newFragment.show(ft, "dialog");
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == R.id.action_save) {
			title = edtCongViec.getText().toString();
			content = edtGhichu.getText().toString();
			if (title.equals("") || content.equals("") || date.equals("")
					|| time.equals("")) {
				Toast.makeText(getActivity(),
						getResources().getString(R.string.fail_form),
						Toast.LENGTH_SHORT).show();
				return false;
			}
			Remind remind = new Remind(id, babyId, Remind.CHAMSOC, title,
					content, mCalendar.getTimeInMillis(), 0, 0, "");

			AlarmManager am = (AlarmManager) getActivity().getSystemService(
					Context.ALARM_SERVICE);
			PendingIntent pendingIntent;
			Intent intent = new Intent(getActivity(), RemindReceiver.class);
			intent.putExtra("TITLE", dbaby.getBaby(babyId).getName());
			intent.putExtra("CONTENT", remind.getContent());
			intent.putExtra("ID", remind.getID());
			pendingIntent = PendingIntent.getBroadcast(getActivity(),
					remind.getID(), intent, 0);
			if (type == ADD) {
				if (db.addRemind(remind, pendingIntent) != -1) {
					Toast.makeText(getActivity(), add_success,
							Toast.LENGTH_SHORT).show();
					edtCongViec.setText("");
					edtGhichu.setText("");
					edtNgay.setText("");
					edtGio.setText("");
					am.set(AlarmManager.RTC_WAKEUP, remind.getTime(),
							DatabaseRemind.intentArray.get(remind.getID()));
				} else {
					Toast.makeText(getActivity(), add_fail, Toast.LENGTH_SHORT)
							.show();
				}
			} else if (type == EDIT) {
				if (db.updateRemind(old, remind, pendingIntent) != 0) {
					Toast.makeText(getActivity(), update_success,
							Toast.LENGTH_SHORT).show();
					am.cancel(DatabaseRemind.intentArray.get(old.getID()));
					am.set(AlarmManager.RTC_WAKEUP, remind.getTime(),
							DatabaseRemind.intentArray.get(remind.getID()));
				} else {
					Toast.makeText(getActivity(), update_fail,
							Toast.LENGTH_SHORT).show();
				}
			}
		} else if (item.getItemId() == android.R.id.home) {
			Fragment fragment = new ChambeFragment();
			AppFragmentActivity act = (AppFragmentActivity) getActivity();
			act.switchContent(fragment);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.menu_save, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

}
