package com.haputech.littleangel.fragment.remind;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.adapter.KhamThaiAdapter;
import com.haputech.littleangel.communicate.OnTimeCommitListener;
import com.haputech.littleangel.db.DatabaseBaby;
import com.haputech.littleangel.db.DatabaseRemind;
import com.haputech.littleangel.fragment.AppFragment;
import com.haputech.littleangel.fragment.DateTimePickerFragment;
import com.haputech.littleangel.object.Remind;
import com.haputech.littleangel.util.ConfigApp;
import com.haputech.littleangel.util.RemindReceiver;

public class KhamthaiFragment extends AppFragment implements
		OnItemClickListener, OnTimeCommitListener {

	private TextView tvDaKham, tvSeKham, tvBoQua;
	private ListView lvKhamthai;
	private DatabaseRemind db;
	private ArrayList<Remind> list, list2;
	private KhamThaiAdapter adapter;

	private int babyId;

	private int tmpId;

	private int dakham, sekham, boqua;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		((AppFragmentActivity) getActivity()).setActionBarTitle(getResources()
				.getString(R.string.khamthai));
		View view = inflater.inflate(R.layout.fragment_kham_thai, null);
		initComponents(view);
		initData();
		setAdapter();
		addListener();
		setHasOptionsMenu(true);
		return view;
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		lvKhamthai = (ListView) v.findViewById(R.id.lvKhamThai);
		tvDaKham = (TextView) v.findViewById(R.id.tvDaKham);
		tvSeKham = (TextView) v.findViewById(R.id.tvSeKham);
		tvBoQua = (TextView) v.findViewById(R.id.tvBoQua);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();

		db = new DatabaseRemind(getActivity().getApplicationContext());

		SharedPreferences pref = getActivity().getSharedPreferences(
				ConfigApp.SHARED_PREF, Context.MODE_PRIVATE);
		babyId = pref.getInt(ConfigApp.BABY_ID, 1);

		list = db.getBabyRemind(babyId, Remind.KHAMTHAI);
		list2 = db.getBabyRemind(babyId, Remind.KHAMTHAI);
		adapter = new KhamThaiAdapter(getActivity().getApplicationContext(),
				android.R.layout.simple_list_item_1, list);

		dakham = sekham = boqua = 0;
		for (Remind remind : list) {
			if (remind.getStatus().equals(
					getResources().getString(R.string.done)))
				dakham++;
			else if (remind.getStatus().equals(
					getResources().getString(R.string.will)))
				sekham++;
			else if (remind.getStatus().equals(
					getResources().getString(R.string.skip)))
				boqua++;
		}
		tvSeKham.setText(sekham + "");
		tvDaKham.setText(dakham + "");
		tvBoQua.setText(boqua + "");
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub
		lvKhamthai.setAdapter(adapter);
	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub
		lvKhamthai.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

		Remind remind = list.get(position);

		tmpId = position;

		DateTimePickerFragment picker = new DateTimePickerFragment(this);
		picker.mCalendar.setTimeInMillis(remind.getTime());
		picker.content = remind.getContent();

		FragmentTransaction ft = getFragmentManager().beginTransaction();
		picker.show(ft, "dialog");

		Log.i("Click list Kham Thai", position + "");
	}

	@Override
	public void OnTimeCommit(Date time) {
		// TODO Auto-generated method stub
		Remind remind = list.get(tmpId);
		remind.setTime(time.getTime());
		adapter.notifyDataSetChanged();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == R.id.action_save) {
			new UpdateRemind().execute();
		} else if (item.getItemId() == android.R.id.home) {
			Fragment fragment = new RemindFragment();
			AppFragmentActivity act = (AppFragmentActivity) getActivity();
			act.switchContent(fragment);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.menu_save, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	private void updateResult() {
		dakham = sekham = boqua = 0;
		for (Remind remind : list) {
			if (remind.getStatus().equals(
					getResources().getString(R.string.done)))
				dakham++;
			else if (remind.getStatus().equals(
					getResources().getString(R.string.will)))
				sekham++;
			else if (remind.getStatus().equals(
					getResources().getString(R.string.skip)))
				boqua++;
		}
		tvSeKham.setText(sekham + "");
		tvDaKham.setText(dakham + "");
		tvBoQua.setText(boqua + "");
		Toast.makeText(getActivity().getApplicationContext(), update_success,
				Toast.LENGTH_LONG).show();
	}

	class UpdateRemind extends AsyncTask<Void, Void, Void> {

		private ProgressDialog progressDialog;

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			AlarmManager am;
			PendingIntent pendingIntent;

			Calendar time = Calendar.getInstance();
			DatabaseBaby dbaby = new DatabaseBaby(getActivity()
					.getApplicationContext());
			for (int i = 0; i < list.size(); i++) {
				Remind remind = list.get(i);
				Remind old = list2.get(i);
				// lấy status mới
				String status = remind.getStatus();
				// sẽ khám
				if (status.equals(getResources().getString(R.string.will))) {

					// lấy thời gian
					time.setTimeInMillis(remind.getTime());
					long time_remind = time.getTimeInMillis();

					am = (AlarmManager) getActivity().getSystemService(
							Context.ALARM_SERVICE);
					// set Alarm
					Intent intent = new Intent(getActivity(),
							RemindReceiver.class);
					intent.putExtra("TITLE", dbaby.getBaby(babyId).getName());
					intent.putExtra("CONTENT", remind.getContent());
					intent.putExtra("ID", remind.getID());
					pendingIntent = PendingIntent.getBroadcast(getActivity(),
							remind.getID(), intent, 0);

					// hủy alarm cũ nếu có
					am.cancel(DatabaseRemind.intentArray.get(remind.getID()));
					// đặt alarm
					am.set(AlarmManager.RTC_WAKEUP, time_remind,
							DatabaseRemind.intentArray.get(remind.getID()));
					db.updateRemind(old, remind, pendingIntent);

				}
				// bỏ qua hoặc đã khám
				else if (status.equals(getResources().getString(R.string.skip))
						|| status.equals(getResources()
								.getString(R.string.done))) {
					db.updateRemind(old, remind, null);
					am = (AlarmManager) getActivity().getSystemService(
							Context.ALARM_SERVICE);
					am.cancel(DatabaseRemind.intentArray.get(remind.getID()));
				}
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage(getResources().getString(R.string.wait));
			progressDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (progressDialog.isShowing())
				progressDialog.dismiss();
			updateResult();
		}

	}
}
