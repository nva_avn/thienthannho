package com.haputech.littleangel.fragment.remind;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.adapter.TiemChungAdapter;
import com.haputech.littleangel.communicate.OnTimeCommitListener;
import com.haputech.littleangel.db.DatabaseBaby;
import com.haputech.littleangel.db.DatabaseRemind;
import com.haputech.littleangel.fragment.AppFragment;
import com.haputech.littleangel.fragment.DateTimePickerFragment;
import com.haputech.littleangel.object.Remind;
import com.haputech.littleangel.util.ConfigApp;
import com.haputech.littleangel.util.RemindReceiver;

public class TiemchungFragment extends AppFragment implements
		OnTimeCommitListener, OnItemClickListener {

	private ListView lvTiemBatBuoc, lvTiemThem;
	private TabHost tabHost;

	private ArrayList<Remind> listFree, listFee;
	private ArrayList<Remind> listFree2, listFee2;
	private TiemChungAdapter adapterFree, adapterFee;

	private DatabaseBaby dbaby;
	private DatabaseRemind dremind;

	private int babyId, tmpId, typeId;
	private String[] titleFree, titleFee;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		((AppFragmentActivity) getActivity()).setActionBarTitle(getResources()
				.getString(R.string.tiemchung));
		View view = inflater.inflate(R.layout.fragment_tiem_chung, null);
		initComponents(view);
		initData();
		setAdapter();
		addListener();
		setHasOptionsMenu(true);
		return view;
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		tabHost = (TabHost) v.findViewById(R.id.tabHost);
		tabHost.setup();
		addNewTab(R.id.tab1, getResources().getString(R.string.free));
		addNewTab(R.id.tab2, getResources().getString(R.string.fee));
		TextView textTab = (TextView) tabHost.getTabWidget()
				.getChildAt(tabHost.getCurrentTab())
				.findViewById(R.id.tabsText);
		textTab.setTextColor(Color.parseColor("#cd1e6e"));
		lvTiemBatBuoc = (ListView) v.findViewById(R.id.lvTiemBatBuoc);
		lvTiemThem = (ListView) v.findViewById(R.id.lvTiemThem);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();

		SharedPreferences pref = getActivity().getSharedPreferences(
				ConfigApp.SHARED_PREF, Context.MODE_PRIVATE);
		babyId = pref.getInt(ConfigApp.BABY_ID, 1);

		dremind = new DatabaseRemind(getActivity().getApplicationContext());
		dbaby = new DatabaseBaby(getActivity().getApplicationContext());

		if (dbaby.getBaby(babyId).getSex() == -1) {
			titleFree = getResources().getStringArray(
					R.array.mom_tiemchung_batbuoc);
			titleFee = getResources()
					.getStringArray(R.array.mom_tiemchung_them);
		} else {
			titleFree = getResources()
					.getStringArray(R.array.tiemchung_batbuoc);
			titleFee = getResources().getStringArray(R.array.tiemchung_them);
		}

		listFree = new ArrayList<Remind>();
		listFee = new ArrayList<Remind>();
		listFree2 = new ArrayList<Remind>();
		listFee2 = new ArrayList<Remind>();

		for (int i = 0; i < titleFree.length; i++) {
			// lấy danh sách nhắc nhở tiêm chủng bắt buộc và khởi tạo ban đầu
			ArrayList<Remind> tmpBB = dremind.getBabyRemind(babyId,
					Remind.TIEMCHUNG, titleFree[i]);
			if (tmpBB.size() != 0) {
				listFree.add(tmpBB.get(0));
				listFree2.add(tmpBB.get(0));
			} else {
				listFree.add(new Remind(0, babyId, Remind.TIEMCHUNG,
						titleFree[i], titleFree[i], 0, 0, 0, getResources()
								.getString(R.string.will)));
				listFree2.add(new Remind(0, babyId, Remind.TIEMCHUNG,
						titleFree[i], titleFree[i], 0, 0, 0, getResources()
								.getString(R.string.will)));
			}
		}
		for (int i = 0; i < titleFee.length; i++) {
			// lấy danh sách nhắc nhở tiêm chủng tùy chọn và khởi tạo ban đầu
			ArrayList<Remind> tmpT = dremind.getBabyRemind(babyId,
					Remind.TIEMCHUNG, titleFee[i]);
			if (tmpT.size() != 0) {
				listFee.add(tmpT.get(0));
				listFee2.add(tmpT.get(0));
			} else {
				listFee.add(new Remind(0, babyId, Remind.TIEMCHUNG,
						titleFee[i], titleFee[i], 0, 0, 0, getResources()
								.getString(R.string.will)));
				listFee2.add(new Remind(0, babyId, Remind.TIEMCHUNG,
						titleFee[i], titleFee[i], 0, 0, 0, getResources()
								.getString(R.string.will)));
			}
		}

		adapterFree = new TiemChungAdapter(getActivity(),
				android.R.layout.simple_list_item_1, listFree);
		adapterFee = new TiemChungAdapter(getActivity(),
				android.R.layout.simple_list_item_1, listFee);
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub
		lvTiemBatBuoc.setAdapter(adapterFree);
		lvTiemThem.setAdapter(adapterFee);
	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub
		lvTiemBatBuoc.setOnItemClickListener(this);
		lvTiemThem.setOnItemClickListener(this);

		tabHost.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
					TextView textTab = (TextView) tabHost.getTabWidget()
							.getChildAt(i).findViewById(R.id.tabsText);
					textTab.setTextColor(Color.WHITE);
				}
				TextView textTab = (TextView) tabHost.getTabWidget()
						.getChildAt(tabHost.getCurrentTab())
						.findViewById(R.id.tabsText);
				textTab.setTextColor(Color.parseColor("#cd1e6e"));

			}
		});
	}

	@Override
	public void OnTimeCommit(Date time) {
		// TODO Auto-generated method stub
		if (typeId == 0) {
			return;
		} else if (typeId == 1) {
			Remind remind = listFree.get(tmpId);
			remind.setTime(time.getTime());
			adapterFree.notifyDataSetChanged();
		} else if (typeId == 2) {
			Remind remind = listFee.get(tmpId);
			remind.setTime(time.getTime());
			adapterFee.notifyDataSetChanged();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Remind remind = null;
		typeId = 0;
		switch (parent.getId()) {
		case R.id.lvTiemBatBuoc:
			remind = listFree.get(position);
			typeId = 1;
			break;
		case R.id.lvTiemThem:
			remind = listFee.get(position);
			typeId = 2;
			break;
		}
		if (remind != null) {
			tmpId = position;

			DateTimePickerFragment picker = new DateTimePickerFragment(this);
			picker.mCalendar.setTimeInMillis(remind.getTime());
			picker.content = remind.getContent();

			FragmentTransaction ft = getFragmentManager().beginTransaction();
			picker.show(ft, "dialog");
		}
	}

	/** thêm tab mới **/
	private void addNewTab(final int contentID, final String tag) {
		View tabview = createTabView(tabHost.getContext(), tag);
		TabSpec setContent = tabHost.newTabSpec(tag).setIndicator(tabview)
				.setContent(contentID);

		tabHost.addTab(setContent);
	}

	/** modify tab view **/
	private View createTabView(final Context context, final String text) {
		View view = LayoutInflater.from(context).inflate(
				R.layout.remind_tab_view, null);
		TextView tv = (TextView) view.findViewById(R.id.tabsText);
		tv.setText(text);
		return view;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == R.id.action_save) {
			new UpdateRemind().execute();
		} else if (item.getItemId() == android.R.id.home) {
			Fragment fragment = new RemindFragment();
			AppFragmentActivity act = (AppFragmentActivity) getActivity();
			act.switchContent(fragment);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.menu_save, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	private void updateResult() {
		Toast.makeText(getActivity().getApplicationContext(), update_success,
				Toast.LENGTH_LONG).show();
	}

	class UpdateRemind extends AsyncTask<Void, Void, Void> {

		private ProgressDialog progressDialog;

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			AlarmManager am;
			PendingIntent pendingIntent;

			Calendar time = Calendar.getInstance();
			DatabaseBaby dbaby = new DatabaseBaby(getActivity()
					.getApplicationContext());
			int type = 1; // loai 0: them moi, 1: cap nhat

			// cap nhat lich tiem mien phi
			for (int i = 0; i < listFree.size(); i++) {
				type = 1;
				Remind remind = listFree.get(i);
				Remind old = listFree2.get(i);

				// lấy status mới
				String status = remind.getStatus();

				// sẽ khám
				if (status.equals(getResources().getString(R.string.will))) {

					// chua set thoi gian thi bo qua
					if (remind.getTime() == 0)
						continue;

					// lấy thời gian
					time.setTimeInMillis(remind.getTime());
					long time_remind = time.getTimeInMillis();

					am = (AlarmManager) getActivity().getSystemService(
							Context.ALARM_SERVICE);

					// kiem tra them moi hay cap nhat
					if (remind.getID() == 0) {
						type = 0;
						remind.setId(dremind.getMaxID() + 1);
					}
					// set Alarm
					Intent intent = new Intent(getActivity(),
							RemindReceiver.class);
					intent.putExtra("TITLE", dbaby.getBaby(babyId).getName());
					intent.putExtra("CONTENT", remind.getContent());
					intent.putExtra("ID", remind.getID());
					pendingIntent = PendingIntent.getBroadcast(getActivity(),
							remind.getID(), intent, 0);

					if (type == 1) {
						// hủy alarm cũ nếu có
						am.cancel(DatabaseRemind.intentArray.get(remind.getID()));
						dremind.updateRemind(old, remind, pendingIntent);
					} else if (type == 0) {
						dremind.addRemind(remind, pendingIntent);
					}
					// đặt alarm
					am.set(AlarmManager.RTC_WAKEUP, time_remind,
							DatabaseRemind.intentArray.get(remind.getID()));

				}
				// đã khám
				else if (status.equals(getResources().getString(R.string.done))) {
					if (remind.getID() == 0) {
						type = 0;
						remind.setId(dremind.getMaxID() + 1);
					}
					if (type == 1) {
						dremind.updateRemind(old, remind, null);
						am = (AlarmManager) getActivity().getSystemService(
								Context.ALARM_SERVICE);
						am.cancel(DatabaseRemind.intentArray.get(remind.getID()));
					} else if (type == 0) {
						dremind.addRemind(remind, null);
					}
				}
			}

			// cap nhat lich tiem co phi

			for (int i = 0; i < listFee.size(); i++) {
				type = 1;
				Remind remind = listFee.get(i);
				Remind old = listFee2.get(i);

				// lấy status mới
				String status = remind.getStatus();

				// sẽ khám
				if (status.equals(getResources().getString(R.string.will))) {

					// chua set thoi gian thi bo qua
					if (remind.getTime() == 0)
						continue;

					// lấy thời gian
					time.setTimeInMillis(remind.getTime());
					long time_remind = time.getTimeInMillis();

					am = (AlarmManager) getActivity().getSystemService(
							Context.ALARM_SERVICE);

					// kiem tra them moi hay cap nhat
					if (remind.getID() == 0) {
						type = 0;
						remind.setId(dremind.getMaxID() + 1);
					}
					// set Alarm
					Intent intent = new Intent(getActivity(),
							RemindReceiver.class);
					intent.putExtra("TITLE", dbaby.getBaby(babyId).getName());
					intent.putExtra("CONTENT", remind.getContent());
					intent.putExtra("ID", remind.getID());
					pendingIntent = PendingIntent.getBroadcast(getActivity(),
							remind.getID(), intent, 0);

					if (type == 1) {
						// hủy alarm cũ nếu có
						am.cancel(DatabaseRemind.intentArray.get(remind.getID()));
						dremind.updateRemind(old, remind, pendingIntent);
					} else if (type == 0) {
						dremind.addRemind(remind, pendingIntent);
					}
					// đặt alarm
					am.set(AlarmManager.RTC_WAKEUP, time_remind,
							DatabaseRemind.intentArray.get(remind.getID()));

				}
				// đã khám
				else if (status.equals(getResources().getString(R.string.done))) {
					if (remind.getID() == 0) {
						type = 0;
						remind.setId(dremind.getMaxID() + 1);
					}
					if (type == 1) {
						dremind.updateRemind(old, remind, null);
						am = (AlarmManager) getActivity().getSystemService(
								Context.ALARM_SERVICE);
						am.cancel(DatabaseRemind.intentArray.get(remind.getID()));
					} else if (type == 0) {
						dremind.addRemind(remind, null);
					}
				}
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage(getResources().getString(R.string.wait));
			progressDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (progressDialog.isShowing())
				progressDialog.dismiss();
			updateResult();
		}

	}
}
