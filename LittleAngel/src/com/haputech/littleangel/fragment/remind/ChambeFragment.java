package com.haputech.littleangel.fragment.remind;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.adapter.RemindAdapter;
import com.haputech.littleangel.db.DatabaseRemind;
import com.haputech.littleangel.fragment.AppFragment;
import com.haputech.littleangel.fragment.DatePickerDialogFragment;
import com.haputech.littleangel.object.Remind;
import com.haputech.littleangel.util.ConfigApp;

public class ChambeFragment extends AppFragment implements OnDateSetListener {

	private ListView lvCare;
	private EditText edtChooseDay;
	private RemindAdapter adapter;
	private DatabaseRemind db;
	private ArrayList<Remind> reminds;

	SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
	private Calendar mCalendar = Calendar.getInstance();
	private String date;

	private int babyId;

	private int positionRemind;

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		// TODO Auto-generated method stub
		mCalendar.set(Calendar.YEAR, year);
		mCalendar.set(Calendar.MONTH, monthOfYear);
		mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
		mCalendar.set(Calendar.HOUR_OF_DAY, 0);
		mCalendar.set(Calendar.MINUTE, 0);
		date = formatDate.format(mCalendar.getTime());
		edtChooseDay.setText(date);

		Log.i("Chambe", date);
		reminds = db.getRemindDate(babyId, Remind.CHAMSOC,
				mCalendar.getTimeInMillis());
		Log.i("Chambe", reminds.size() + "");
		adapter.updateData(reminds);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		((AppFragmentActivity) getActivity()).setActionBarTitle(getResources()
				.getString(R.string.chambe));
		View view = inflater.inflate(R.layout.fragment_care_baby, null);
		initComponents(view);
		initData();
		setAdapter();
		addListener();
		setHasOptionsMenu(true);
		return view;
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		lvCare = (ListView) v.findViewById(R.id.lvCare);
		edtChooseDay = (EditText) v.findViewById(R.id.edtChooseDay);

		registerForContextMenu(lvCare);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();
		db = new DatabaseRemind(getActivity().getApplicationContext());

		SharedPreferences pref = getActivity().getSharedPreferences(
				ConfigApp.SHARED_PREF, Context.MODE_PRIVATE);
		babyId = pref.getInt(ConfigApp.BABY_ID, 1);

		mCalendar.set(Calendar.HOUR_OF_DAY, 0);
		mCalendar.set(Calendar.MINUTE, 0);
		edtChooseDay.setText(formatDate.format(mCalendar.getTime()));
		reminds = db.getRemindDate(babyId, mCalendar.getTimeInMillis());
		adapter = new RemindAdapter(getActivity().getApplicationContext(),
				android.R.layout.simple_list_item_1, reminds);
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub
		lvCare.setAdapter(adapter);
	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub
		lvCare.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				positionRemind = position;
				getActivity().openContextMenu(lvCare);
			}

		});

		edtChooseDay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
				DialogFragment newFragment = new DatePickerDialogFragment(
						ChambeFragment.this);
				newFragment.show(ft, "dialog");
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == R.id.action_add) {
			Fragment fragment = new ChambeAddFragment();
			AppFragmentActivity act = (AppFragmentActivity) getActivity();
			act.switchContent(fragment);
		} else if (item.getItemId() == android.R.id.home) {
			Fragment fragment = new RemindFragment();
			AppFragmentActivity act = (AppFragmentActivity) getActivity();
			act.switchContent(fragment);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.menu_add, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		Resources res = getResources();

		menu.setHeaderTitle(res.getString(R.string.action_menu));
		menu.add(0, 1, 0, res.getString(R.string.edit));
		menu.add(0, 2, 0, res.getString(R.string.delete));
	}

	@Override
	public boolean onContextItemSelected(android.view.MenuItem item) {
		if (item.getItemId() == 1) {
			ChambeAddFragment fragment = new ChambeAddFragment();
			fragment.type = ChambeAddFragment.EDIT;
			fragment.old = reminds.get(positionRemind);
			AppFragmentActivity act = (AppFragmentActivity) getActivity();
			act.switchContent(fragment);
		} else if (item.getItemId() == 2) {
			if (positionRemind != -1) {
				AlertDialog del = new AlertDialog.Builder(getActivity())
						.setTitle(getResources().getString(R.string.delete))
						.setMessage(
								getResources().getString(R.string.sure_delete))
						.setPositiveButton(
								getResources().getString(R.string.yes),
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										Remind remind = reminds
												.get(positionRemind);
										db.deleteRemind(remind);
										AlarmManager am = (AlarmManager) getActivity()
												.getSystemService(
														Context.ALARM_SERVICE);
										am.cancel(DatabaseRemind.intentArray
												.get(remind.getID()));
										reminds.remove(positionRemind);
										adapter.updateData(reminds);
										positionRemind = -1;
									}
								})
						.setNegativeButton(
								getResources().getString(R.string.no),
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub

									}
								}).create();
				del.show();

			}
		}
		return super.onContextItemSelected(item);
	}
}
