package com.haputech.littleangel.fragment.remind;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.adapter.RemindTypeAdapter;
import com.haputech.littleangel.db.DatabaseBaby;
import com.haputech.littleangel.fragment.AppFragment;
import com.haputech.littleangel.object.Baby;
import com.haputech.littleangel.util.ConfigApp;

public class RemindFragment extends AppFragment implements OnItemClickListener {

	private ListView lvRemind;
	private RemindTypeAdapter adapter;
	private ArrayList<String> type;

	private int babyId;
	private DatabaseBaby db;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		((AppFragmentActivity) getActivity()).setActionBarTitle(getResources()
				.getString(R.string.remind));
		View view = inflater.inflate(R.layout.fragment_list_view, null);
		initComponents(view);
		initData();
		setAdapter();
		addListener();
		return view;
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		lvRemind = (ListView) v.findViewById(R.id.lvContent);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();

		type = new ArrayList<String>();
		type.add(khamthai);
		type.add(tiemchung);
		type.add(chambe);

		adapter = new RemindTypeAdapter(getActivity().getApplicationContext(),
				android.R.layout.simple_list_item_1, type);

		db = new DatabaseBaby(getActivity());
		SharedPreferences pref = getActivity().getSharedPreferences(
				ConfigApp.SHARED_PREF, Context.MODE_PRIVATE);
		babyId = pref.getInt(ConfigApp.BABY_ID, 1);

	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub
		lvRemind.setAdapter(adapter);
	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub
		lvRemind.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Baby baby = db.getBaby(babyId);
		Fragment fragment = null;
		switch (position) {
		case 0:
			if (baby.getSex() == -1) {
				fragment = new KhamthaiFragment();
			} else {
				Toast.makeText(getActivity(),
						getResources().getString(R.string.preg_use),
						Toast.LENGTH_SHORT).show();
			}
			break;
		case 1:
			fragment = new TiemchungFragment();
			break;
		case 2:
			if (baby.getSex() == -1) {
				Toast.makeText(getActivity(),
						getResources().getString(R.string.birth_use),
						Toast.LENGTH_SHORT).show();
			} else {
				fragment = new ChambeFragment();
			}
			break;
		}

		if (fragment != null) {
			AppFragmentActivity act = (AppFragmentActivity) getActivity();
			act.switchContent(fragment);
		}

	}

}
