package com.haputech.littleangel.fragment.health;

import java.util.ArrayList;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.SeriesSelection;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.adapter.HealthAdapter;
import com.haputech.littleangel.db.DatabaseBaby;
import com.haputech.littleangel.db.DatabaseGrowth;
import com.haputech.littleangel.fragment.AppFragment;
import com.haputech.littleangel.object.Baby;
import com.haputech.littleangel.object.Growth;
import com.haputech.littleangel.util.ConfigApp;

public class HealthFragment extends AppFragment {

	private ListView lvData;
	private TabHost tabHost;

	private HealthAdapter adapter;
	private DatabaseGrowth db;
	private DatabaseBaby dbaby;
	private ArrayList<Growth> growths;

	private Resources res;
	private int babyId;

	String[] age, ageBaby;
	double[] w_standard, w_malnutrition, w_obesity, h_standard, h_short;
	double[] w_mBaby;
	double[] h_mBaby;

	View view;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_health, null);
		initComponents(view);
		initData();
		setAdapter();
		addListener();
		this.view = view;
		init();
		showHChart();
		showWChart();
		setHasOptionsMenu(true);
		return view;
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		lvData = (ListView) v.findViewById(R.id.lvHealth);

		tabHost = (TabHost) v.findViewById(R.id.tabHostHealth);
		tabHost.setup();
		addNewTab(R.id.tabHealth1, getResources().getString(R.string.health));
		addNewTab(R.id.tabHealth2,
				getResources().getString(R.string.weight_chart));
		addNewTab(R.id.tabHealth3,
				getResources().getString(R.string.height_chart));
		TextView textTab = (TextView) tabHost.getTabWidget()
				.getChildAt(tabHost.getCurrentTab())
				.findViewById(R.id.tabsText);
		textTab.setTextColor(Color.parseColor("#cd1e6e"));
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		db = new DatabaseGrowth(getActivity());
		dbaby = new DatabaseBaby(getActivity());

		SharedPreferences pref = getActivity().getSharedPreferences(
				ConfigApp.SHARED_PREF, Context.MODE_PRIVATE);
		babyId = pref.getInt(ConfigApp.BABY_ID, 1);

		growths = db.getListGrowth(babyId);
		adapter = new HealthAdapter(getActivity(),
				android.R.layout.simple_list_item_1, growths);

		res = getActivity().getResources();

	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub
		lvData.setAdapter(adapter);
	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub
		tabHost.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
					TextView textTab = (TextView) tabHost.getTabWidget()
							.getChildAt(i).findViewById(R.id.tabsText);
					textTab.setTextColor(Color.WHITE);
				}
				TextView textTab = (TextView) tabHost.getTabWidget()
						.getChildAt(tabHost.getCurrentTab())
						.findViewById(R.id.tabsText);
				textTab.setTextColor(Color.parseColor("#cd1e6e"));

			}
		});
	}

	/** thêm tab mới **/
	private void addNewTab(final int contentID, final String tag) {
		View tabview = createTabView(tabHost.getContext(), tag);
		TabSpec setContent = tabHost.newTabSpec(tag).setIndicator(tabview)
				.setContent(contentID);

		tabHost.addTab(setContent);
	}

	/** modify tab view **/
	private View createTabView(final Context context, final String text) {
		View view = LayoutInflater.from(context).inflate(
				R.layout.remind_tab_view, null);
		TextView tv = (TextView) view.findViewById(R.id.tabsText);
		tv.setText(text);
		return view;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == R.id.action_add) {
			Fragment fragment = new AddHealthFragment();
			AppFragmentActivity act = (AppFragmentActivity) getActivity();
			act.switchContent(fragment);
		} else if (item.getItemId() == android.R.id.home) {

		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.menu_add, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	protected void init() {
		age = getResources().getStringArray(R.array.old_baby);
		w_standard = new double[ConfigApp.AGE.length];
		w_malnutrition = new double[ConfigApp.AGE.length];
		w_obesity = new double[ConfigApp.AGE.length];
		h_standard = new double[ConfigApp.AGE.length];
		h_short = new double[ConfigApp.AGE.length];

		// lấy các thông số đánh giá phù hợp với giới tính
		Baby baby = dbaby.getBaby(babyId);
		if (baby.getSex() == Baby.FEMALE) {
			for (int i = 0; i < ConfigApp.AGE.length; i++) {
				w_standard[i] = ConfigApp.FEMALE_W_STANDARD[i];
				w_malnutrition[i] = ConfigApp.FEMALE_MALNUTRITION[i];
				w_obesity[i] = ConfigApp.FEMALE_OBESITY[i];
				h_standard[i] = ConfigApp.FEMALE_H_STANDARD[i];
				h_short[i] = ConfigApp.FEMALE_SHORT[i];
			}
		} else if (baby.getSex() == Baby.MALE) {
			for (int i = 0; i < ConfigApp.AGE.length; i++) {
				w_standard[i] = ConfigApp.MALE_W_STANDARD[i];
				w_malnutrition[i] = ConfigApp.MALE_MALNUTRITION[i];
				w_obesity[i] = ConfigApp.MALE_OBESITY[i];
				h_standard[i] = ConfigApp.MALE_H_STANDARD[i];
				h_short[i] = ConfigApp.MALE_SHORT[i];
			}
		}

		// lấy danh sách phát triển
		ageBaby = new String[growths.size()];
		w_mBaby = new double[growths.size()];
		h_mBaby = new double[growths.size()];
		for (int i = 0; i < growths.size(); i++) {
			ageBaby[i] = growths.get(i).getAge();
			w_mBaby[i] = growths.get(i).getWeight();
			h_mBaby[i] = growths.get(i).getHeight();
		}
	}

	/** Ve biểu đồ cân nặng **/
	public void showWChart() {
		// khai báo 4 đường của biểu đồ
		XYSeries cnchuanSeries = new XYSeries(
				res.getString(R.string.weight_standard));
		XYSeries thieucanSeries = new XYSeries(
				res.getString(R.string.weight_manultrition));
		XYSeries thuacanSeries = new XYSeries(
				res.getString(R.string.weight_obesity));
		XYSeries cnSeries = new XYSeries(res.getString(R.string.weight_baby));

		// thêm dữ liệu cho mỗi đường
		for (int i = 0; i < ConfigApp.AGE.length; i++) {
			cnchuanSeries.add(ConfigApp.AGE[i], w_standard[i]);
			thieucanSeries.add(ConfigApp.AGE[i], w_malnutrition[i]);
			thuacanSeries.add(ConfigApp.AGE[i], w_obesity[i]);
			for (int j = 0; j < ageBaby.length; j++) {
				if (ageBaby[j].equals(age[i + 1])) {
					cnSeries.add(ConfigApp.AGE[i], w_mBaby[j]);
					break;
				}
			}
		}

		// Creating a dataset to hold each series
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		// Adding Series to dataset
		dataset.addSeries(cnchuanSeries);
		dataset.addSeries(thieucanSeries);
		dataset.addSeries(thuacanSeries);
		dataset.addSeries(cnSeries);

		// Creating XYSeriesRenderer to customize each Series
		XYSeriesRenderer cnchuanRenderer = new XYSeriesRenderer();
		cnchuanRenderer.setColor(Color.GREEN);
		cnchuanRenderer.setPointStyle(PointStyle.CIRCLE);
		cnchuanRenderer.setFillPoints(true);
		cnchuanRenderer.setLineWidth(2);
		cnchuanRenderer.setDisplayChartValues(true);

		XYSeriesRenderer cnRenderer = new XYSeriesRenderer();
		cnRenderer.setColor(Color.YELLOW);
		cnRenderer.setPointStyle(PointStyle.CIRCLE);
		cnRenderer.setFillPoints(true);
		cnRenderer.setLineWidth(2);
		cnRenderer.setDisplayChartValues(true);

		XYSeriesRenderer thieucanRenderer = new XYSeriesRenderer();
		thieucanRenderer.setColor(Color.BLUE);
		thieucanRenderer.setPointStyle(PointStyle.CIRCLE);
		thieucanRenderer.setFillPoints(true);
		thieucanRenderer.setLineWidth(2);
		thieucanRenderer.setDisplayChartValues(true);

		XYSeriesRenderer thuacanRenderer = new XYSeriesRenderer();
		thuacanRenderer.setColor(Color.RED);
		thuacanRenderer.setPointStyle(PointStyle.CIRCLE);
		thuacanRenderer.setFillPoints(true);
		thuacanRenderer.setLineWidth(2);
		thuacanRenderer.setDisplayChartValues(true);

		// Creating a XYMultipleSeriesRenderer to customize the whole chart
		XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
		multiRenderer.setYTitle("KG");
		multiRenderer.setApplyBackgroundColor(true);
		multiRenderer.setMargins(new int[] { 0, 0, 0, 0 });
		multiRenderer.setMarginsColor(Color.argb(0x00, 0x01, 0x01, 0x01));
		multiRenderer.setBackgroundColor(Color.TRANSPARENT);
		multiRenderer.setLabelsTextSize(getResources().getDimension(
				R.dimen.text_size1));
		multiRenderer.setLabelsColor(getResources()
				.getColor(R.color.dark_green));
		multiRenderer.setXLabelsColor(Color.BLACK);
		multiRenderer.setYLabelsColor(0, Color.BLACK);
		multiRenderer.setPointSize(4);
		multiRenderer.setAxesColor(Color.BLACK);
		multiRenderer.setShowGrid(true);
		multiRenderer.setGridColor(Color.LTGRAY);
		multiRenderer.setYLabelsAlign(Align.LEFT);
		multiRenderer.setYAxisAlign(Align.LEFT, 0);

		// Adding each XYSeriesRender to multipleRenderer
		// Note: The order of adding dataseries to dataset and renderers to
		// multipleRenderer should be same
		multiRenderer.addSeriesRenderer(cnchuanRenderer);
		multiRenderer.addSeriesRenderer(thieucanRenderer);
		multiRenderer.addSeriesRenderer(thuacanRenderer);
		multiRenderer.addSeriesRenderer(cnRenderer);

		// Getting a reference to LinearLayout of the MainActivity Layout
		LinearLayout chartContainer = (LinearLayout) view
				.findViewById(R.id.chart_weight);

		// Creating a Line Chart
		final GraphicalView mChart;
		mChart = ChartFactory.getLineChartView(getActivity()
				.getApplicationContext(), dataset, multiRenderer);
		multiRenderer.setClickEnabled(true);// Cho phép click
		multiRenderer.setSelectableBuffer(10);// Thiết lập vùng đệm
		mChart.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				SeriesSelection seriesSelection = mChart
						.getCurrentSeriesAndPoint();
				if (seriesSelection != null) {
					int seriesIndex = seriesSelection.getSeriesIndex();
					if (seriesIndex == 3) {
						// int tuoi = (int) seriesSelection.getXValue();
						// Intent xemtangtruong = new Intent(
						// CareActivity.this, CareView.class);
						// Bundle bd = new Bundle();
						// bd.putInt("AGE", tuoi);
						// xemtangtruong.putExtras(bd);
						// startActivity(xemtangtruong);
					}

				}
			}
		});
		// Adding the Line Chart to the LinearLayout
		chartContainer.addView(mChart);
	}

	/** hiện biểu đồ chiều cao **/
	public void showHChart() {

		// khai báo 4 đường của biểu đồ
		XYSeries chuanSeries = new XYSeries(
				res.getString(R.string.height_standard));
		XYSeries sddSeries = new XYSeries(res.getString(R.string.height_short));
		XYSeries babySeries = new XYSeries(res.getString(R.string.height_baby));

		// thêm dữ liệu cho mỗi đường
		for (int i = 0; i < ConfigApp.AGE.length; i++) {
			chuanSeries.add(ConfigApp.AGE[i], h_standard[i]);
			sddSeries.add(ConfigApp.AGE[i], h_short[i]);
			for (int j = 0; j < ageBaby.length; j++) {
				if (ageBaby[j].equals(age[i + 1])) {
					babySeries.add(ConfigApp.AGE[i], h_mBaby[j]);
					break;
				}
			}
		}

		// Creating a dataset to hold each series
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		// Adding Series to dataset
		dataset.addSeries(chuanSeries);
		dataset.addSeries(sddSeries);
		dataset.addSeries(babySeries);

		// Creating XYSeriesRenderer to customize each Series
		XYSeriesRenderer chuanRenderer = new XYSeriesRenderer();
		chuanRenderer.setColor(Color.GREEN);
		chuanRenderer.setPointStyle(PointStyle.CIRCLE);
		chuanRenderer.setFillPoints(true);
		chuanRenderer.setLineWidth(2);
		chuanRenderer.setDisplayChartValues(true);

		XYSeriesRenderer babyRenderer = new XYSeriesRenderer();
		babyRenderer.setColor(Color.YELLOW);
		babyRenderer.setPointStyle(PointStyle.CIRCLE);
		babyRenderer.setFillPoints(true);
		babyRenderer.setLineWidth(2);
		babyRenderer.setDisplayChartValues(true);

		XYSeriesRenderer sddRenderer = new XYSeriesRenderer();
		sddRenderer.setColor(Color.BLUE);
		sddRenderer.setPointStyle(PointStyle.CIRCLE);
		sddRenderer.setFillPoints(true);
		sddRenderer.setLineWidth(2);
		sddRenderer.setDisplayChartValues(true);

		// Creating a XYMultipleSeriesRenderer to customize the whole chart
		XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
		multiRenderer.setYTitle("CM");
		multiRenderer.setApplyBackgroundColor(true);
		multiRenderer.setMargins(new int[] { 0, 0, 0, 0 });
		multiRenderer.setMarginsColor(Color.argb(0x00, 0x01, 0x01, 0x01));
		multiRenderer.setBackgroundColor(Color.TRANSPARENT);
		multiRenderer.setLabelsTextSize(getResources().getDimension(
				R.dimen.text_size1));
		multiRenderer.setLabelsColor(getResources()
				.getColor(R.color.dark_green));
		multiRenderer.setXLabelsColor(Color.BLACK);
		multiRenderer.setYLabelsColor(0, Color.BLACK);
		multiRenderer.setPointSize(4);
		multiRenderer.setAxesColor(Color.BLACK);
		multiRenderer.setShowGrid(true);
		multiRenderer.setGridColor(Color.LTGRAY);
		multiRenderer.setYLabelsAlign(Align.LEFT);
		multiRenderer.setYAxisAlign(Align.LEFT, 0);

		// Adding each XYSeriesRender to multipleRenderer
		// Note: The order of adding dataseries to dataset and renderers to
		// multipleRenderer should be same
		multiRenderer.addSeriesRenderer(chuanRenderer);
		multiRenderer.addSeriesRenderer(sddRenderer);
		multiRenderer.addSeriesRenderer(babyRenderer);

		// Getting a reference to LinearLayout of the MainActivity Layout
		LinearLayout chartContainer = (LinearLayout) view
				.findViewById(R.id.chart_height);

		// Creating a Line Chart
		final GraphicalView mChart;
		mChart = ChartFactory.getLineChartView(getActivity()
				.getApplicationContext(), dataset, multiRenderer);
		multiRenderer.setClickEnabled(true);// Cho phép click
		multiRenderer.setSelectableBuffer(10);// Thiết lập vùng đệm
		mChart.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				SeriesSelection seriesSelection = mChart
						.getCurrentSeriesAndPoint();
				if (seriesSelection != null) {
					int seriesIndex = seriesSelection.getSeriesIndex();
					if (seriesIndex == 2) {
						// int tuoi = (int) seriesSelection.getXValue();
						// Intent xemtangtruong = new Intent(
						// CareActivity.this, CareView.class);
						// Bundle bd = new Bundle();
						// bd.putInt("AGE", tuoi);
						// xemtangtruong.putExtras(bd);
						// startActivity(xemtangtruong);
					}

				}
			}
		});
		// Adding the Line Chart to the LinearLayout
		chartContainer.addView(mChart);
	}

}
