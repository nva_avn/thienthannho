package com.haputech.littleangel.fragment.health;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.db.DatabaseBaby;
import com.haputech.littleangel.db.DatabaseGrowth;
import com.haputech.littleangel.fragment.AppFragment;
import com.haputech.littleangel.fragment.book.BookViewFragment;
import com.haputech.littleangel.object.Baby;
import com.haputech.littleangel.object.Growth;
import com.haputech.littleangel.util.ConfigApp;

public class AddHealthFragment extends AppFragment implements OnClickListener {

	private Spinner spAge;
	private EditText edWeight, edHeight;
	private Button btSaveData, btCancelData;
	private TextView tvRate;

	String[] age;

	int id, babyId;
	double weight, height;
	double[] w_standard, w_malnutrition, w_obesity, h_standard, h_short;
	private int[] advice;
	private int pos;
	String old;
	String rate;
	DatabaseGrowth dg;
	DatabaseBaby db;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_add_health, null);
		initComponents(view);
		initData();
		setAdapter();
		addListener();
		setHasOptionsMenu(true);
		return view;
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		spAge = (Spinner) v.findViewById(R.id.spAge);
		edWeight = (EditText) v.findViewById(R.id.edWeight);
		edHeight = (EditText) v.findViewById(R.id.edHeight);
		btSaveData = (Button) v.findViewById(R.id.btSaveData);
		btCancelData = (Button) v.findViewById(R.id.btCancelData);
		tvRate = (TextView) v.findViewById(R.id.tvRate);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();
		age = getResources().getStringArray(R.array.old_baby);
		dg = new DatabaseGrowth(getActivity());
		db = new DatabaseBaby(getActivity());
		w_standard = new double[ConfigApp.AGE.length];
		w_malnutrition = new double[ConfigApp.AGE.length];
		w_obesity = new double[ConfigApp.AGE.length];
		h_standard = new double[ConfigApp.AGE.length];
		h_short = new double[ConfigApp.AGE.length];

		SharedPreferences pref = getActivity().getSharedPreferences(
				ConfigApp.SHARED_PREF, Context.MODE_PRIVATE);
		babyId = pref.getInt(ConfigApp.BABY_ID, 1);

		Baby baby = db.getBaby(babyId);
		if (baby.getSex() == Baby.FEMALE) {
			for (int i = 0; i < ConfigApp.AGE.length; i++) {
				w_standard[i] = ConfigApp.FEMALE_W_STANDARD[i];
				w_malnutrition[i] = ConfigApp.FEMALE_MALNUTRITION[i];
				w_obesity[i] = ConfigApp.FEMALE_OBESITY[i];
				h_standard[i] = ConfigApp.FEMALE_H_STANDARD[i];
				h_short[i] = ConfigApp.FEMALE_SHORT[i];
			}
		} else if (baby.getSex() == Baby.MALE) {
			for (int i = 0; i < ConfigApp.AGE.length; i++) {
				w_standard[i] = ConfigApp.MALE_W_STANDARD[i];
				w_malnutrition[i] = ConfigApp.MALE_MALNUTRITION[i];
				w_obesity[i] = ConfigApp.MALE_OBESITY[i];
				h_standard[i] = ConfigApp.MALE_H_STANDARD[i];
				h_short[i] = ConfigApp.MALE_SHORT[i];
			}
		}
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_spinner_item, age);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spAge.setAdapter(adapter);
	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub
		btSaveData.setOnClickListener(this);
		btCancelData.setOnClickListener(this);

		spAge.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> av, View v, int position,
					long id) {
				if (position >= 1)
					old = age[position];
				pos = position - 1;
			}

			@Override
			public void onNothingSelected(AdapterView<?> av) {
				old = "";
			}

		});
		tvRate.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Resources res = getResources();
		switch (v.getId()) {
		case R.id.btCancelData:
			edHeight.setText("");
			edWeight.setText("");
			tvRate.setText("");
			break;
		case R.id.btSaveData:
			if (saveData()) {
				tvRate.setVisibility(View.VISIBLE);
				if (!rate.equals(res.getString(R.string.care_standard)))
					tvRate.setText(rate + "\n"
							+ res.getString(R.string.press_advice));
			}
			break;
		case R.id.tvRate:
			if (!rate.equals(res.getString(R.string.care_standard))) {
				getValue(rate);
				SharedPreferences pref = getActivity().getSharedPreferences(
						ConfigApp.SHARED_PREF, Context.MODE_PRIVATE);
				Editor editor = pref.edit();
				editor.putInt(ConfigApp.BOOK_NAME, advice[pos]);
				editor.commit();
				Fragment book = new BookViewFragment();
				AppFragmentActivity act = (AppFragmentActivity) getActivity();
				act.switchContent(book);
			}
			break;
		}
	}

	private boolean saveData() {
		String w = edWeight.getText().toString();
		String h = edHeight.getText().toString();

		if (old.equals("")) {
			Toast.makeText(getActivity().getApplicationContext(),
					getResources().getString(R.string.check_growth_age),
					Toast.LENGTH_SHORT).show();
			return false;
		}
		if (w.equals("")) {
			Toast.makeText(getActivity().getApplicationContext(),
					getResources().getString(R.string.check_growth_weight),
					Toast.LENGTH_SHORT).show();
			return false;
		}
		if (h.equals("")) {
			Toast.makeText(getActivity().getApplicationContext(),
					getResources().getString(R.string.check_growth_height),
					Toast.LENGTH_SHORT).show();
			return false;
		}
		weight = Double.parseDouble(w);
		height = Double.parseDouble(h);
		rate();
		if (dg.getGrowth(babyId, old) != null) {
			Toast.makeText(getActivity().getApplicationContext(),
					"Đã có dữ liệu của tuổi này !", Toast.LENGTH_LONG).show();
			return false;
		} else {
			id = dg.getMaxID() + 1;
			Growth growth = new Growth(id, babyId, old, height, weight, rate);
			dg.addData(growth);
			Toast.makeText(getActivity().getApplicationContext(),
					getResources().getString(R.string.add_success),
					Toast.LENGTH_SHORT).show();
			return true;
		}

	}

	/** đánh giá phát triển **/
	private void rate() {
		int i;
		Resources res = getResources();
		for (i = 0; i < age.length; i++)
			if (old.equals(age[i]))
				break;
		i = i - 1;
		if (weight > w_obesity[i])
			rate = res.getString(R.string.care_obesity);
		else if (weight > w_standard[i]) {
			if (height >= h_standard[i])
				rate = res.getString(R.string.care_standard);
			else if (height >= h_short[i])
				rate = res.getString(R.string.care_obesity_warning);
			else
				rate = res.getString(R.string.care_obesity);
		} else if (weight == w_standard[i]) {
			if (height == h_standard[i])
				rate = res.getString(R.string.care_standard);
			else
				rate = res.getString(R.string.care_short);
		} else if (weight > w_malnutrition[i]) {
			if (height > h_standard[i])
				rate = res.getString(R.string.care_weight);
			else
				rate = res.getString(R.string.care_malnu_warning);
		} else
			rate = res.getString(R.string.care_malnu);
	}

	private void getValue(String rate) {
		TypedArray ar;
		if (rate.equals(getResources().getString(R.string.care_malnu))
				|| rate.equals(getResources().getString(
						R.string.care_malnu_warning))) {
			ar = getResources().obtainTypedArray(R.array.thieucan_advice);
		} else if (rate.equals(getResources().getString(R.string.care_short))) {
			ar = getResources().obtainTypedArray(R.array.chieucao_advice);
		} else {
			ar = getResources().obtainTypedArray(R.array.thuacan_advice);
		}
		int len = ar.length();
		advice = new int[len];
		for (int i = 0; i < len; i++) {
			advice[i] = ar.getResourceId(i, 0);
			ar.recycle();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == R.id.action_save) {

		} else if (item.getItemId() == android.R.id.home) {
			Fragment fragment = new HealthFragment();
			AppFragmentActivity act = (AppFragmentActivity) getActivity();
			act.switchContent(fragment);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.menu_save, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}
}
