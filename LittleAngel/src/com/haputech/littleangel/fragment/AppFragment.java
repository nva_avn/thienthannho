package com.haputech.littleangel.fragment;

import com.haputech.littleangel.R;

import android.support.v4.app.Fragment;
import android.view.View;

/**
 * Application Fragment
 * 
 * @author NgVietAn
 * 
 */
public abstract class AppFragment extends Fragment {

	public static String add_success, add_fail, del_success, del_fail,
			update_success, update_fail;

	public static String before_preg, pregnancy, after_preg;

	public static String khamthai, tiemchung, chambe;

	/**
	 * Khoi tao cac components
	 */
	protected abstract void initComponents(View v);

	/**
	 * Khoi tao du lieu
	 */
	protected void initData() {
		add_success = getResources().getString(R.string.add_success);
		add_fail = getResources().getString(R.string.add_fail);
		del_success = getResources().getString(R.string.del_success);
		del_fail = getResources().getString(R.string.del_fail);
		update_success = getResources().getString(R.string.update_success);
		update_fail = getResources().getString(R.string.update_fail);

		before_preg = getResources().getString(R.string.before_preg);
		pregnancy = getResources().getString(R.string.pregnancy);
		after_preg = getResources().getString(R.string.after_preg);

		khamthai = getResources().getString(R.string.khamthai);
		tiemchung = getResources().getString(R.string.tiemchung);
		chambe = getResources().getString(R.string.chambe);
	}

	/**
	 * Set Adapter (neu co)
	 */
	protected abstract void setAdapter();

	/**
	 * Add listener cho cac components
	 */
	protected abstract void addListener();

	public final void setHomeButton(boolean hasHomeButton) {
		getActivity().getActionBar().setHomeButtonEnabled(hasHomeButton);
		getActivity().getActionBar().setDisplayHomeAsUpEnabled(hasHomeButton);
	}
}
