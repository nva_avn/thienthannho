package com.haputech.littleangel.fragment.hotcall;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.adapter.HotCallAdapter;
import com.haputech.littleangel.db.DatabaseTel;
import com.haputech.littleangel.fragment.AppFragment;
import com.haputech.littleangel.object.HotTel;

public class HotCallFragment extends AppFragment implements OnItemClickListener {

	private ListView lvPhone;
	private DatabaseTel db;
	private ArrayList<HotTel> list;
	private HotCallAdapter adapter;

	int pos = -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		((AppFragmentActivity) getActivity()).setActionBarTitle(getResources()
				.getString(R.string.danhba));
		View view = inflater.inflate(R.layout.fragment_list_view, null);
		initComponents(view);
		initData();
		setAdapter();
		addListener();
		setHasOptionsMenu(true);
		return view;
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		lvPhone = (ListView) v.findViewById(R.id.lvContent);
		registerForContextMenu(lvPhone);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();
		db = new DatabaseTel(getActivity().getApplicationContext());
		list = db.getListTel();
		adapter = new HotCallAdapter(getActivity().getApplicationContext(),
				android.R.layout.simple_list_item_1, list);
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub
		lvPhone.setAdapter(adapter);
	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub
		lvPhone.setOnItemClickListener(this);

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		pos = position;
		getActivity().openContextMenu(lvPhone);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == R.id.action_add) {
			Fragment fragment = new AddHotCallFragment();
			AppFragmentActivity act = (AppFragmentActivity) getActivity();
			act.switchContent(fragment);
		} else if (item.getItemId() == android.R.id.home) {

		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.menu_add, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		Resources res = getResources();

		menu.setHeaderTitle(res.getString(R.string.action_menu));
		menu.add(0, 1, 0, res.getString(R.string.call));
		menu.add(0, 2, 0, res.getString(R.string.edit));
		menu.add(0, 3, 0, res.getString(R.string.delete));
	}

	@Override
	public boolean onContextItemSelected(android.view.MenuItem item) {
		if (item.getItemId() == 1) {
			String uriCall = "tel:" + list.get(pos).getTel();
			Intent call = new Intent(Intent.ACTION_DIAL, Uri.parse(uriCall));
			startActivity(call);
		} else if (item.getItemId() == 2) {
			AddHotCallFragment fragment = new AddHotCallFragment();
			fragment.type = AddHotCallFragment.EDIT;
			fragment.id = list.get(pos).getID();
			AppFragmentActivity act = (AppFragmentActivity) getActivity();
			act.switchContent(fragment);
		} else if (item.getItemId() == 3) {
			if (pos != -1) {
				AlertDialog del = new AlertDialog.Builder(getActivity())
						.setTitle(getResources().getString(R.string.delete))
						.setMessage(
								getResources().getString(R.string.sure_delete))
						.setPositiveButton(
								getResources().getString(R.string.yes),
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										HotTel tel = list.get(pos);
										db.deleteHotTel(tel);
										list.remove(pos);
										adapter.notifyDataSetChanged();
									}
								})
						.setNegativeButton(
								getResources().getString(R.string.no),
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub

									}
								}).create();
				del.show();

			}
		}
		return super.onContextItemSelected(item);
	}
}
