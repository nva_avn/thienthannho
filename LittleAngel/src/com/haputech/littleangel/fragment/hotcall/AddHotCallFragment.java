package com.haputech.littleangel.fragment.hotcall;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.db.DatabaseTel;
import com.haputech.littleangel.fragment.AppFragment;
import com.haputech.littleangel.object.HotTel;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

public class AddHotCallFragment extends AppFragment {

	public static int ADD = 100;
	public static int EDIT = 101;
	public int type = ADD;
	public int id;

	private EditText edtName, edtPhone;
	private DatabaseTel db;
	private HotTel old;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		((AppFragmentActivity) getActivity()).setActionBarTitle(getResources()
				.getString(R.string.add_danhba));
		View view = inflater.inflate(R.layout.fragment_add_hotcall, null);
		initComponents(view);
		initData();
		setHasOptionsMenu(true);
		return view;
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		edtName = (EditText) v.findViewById(R.id.edtHotCallName);
		edtPhone = (EditText) v.findViewById(R.id.edtHotCallPhone);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();
		db = new DatabaseTel(getActivity().getApplicationContext());

		if (type == EDIT) {
			old = db.getTel(id);
			edtName.setText(old.getName());
			edtPhone.setText(old.getTel());
			edtName.setEnabled(false);
		} else if (type == ADD) {
			id = db.getMaxID() + 1;
		}
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == R.id.action_save) {
			if (type == ADD) {
				if (savePhone()) {
					Toast.makeText(getActivity().getApplicationContext(),
							add_success, Toast.LENGTH_LONG).show();
					edtName.setText("");
					edtPhone.setText("");
				} else {
					Toast.makeText(getActivity().getApplicationContext(),
							add_fail, Toast.LENGTH_LONG).show();
				}
			} else if (type == EDIT) {
				if (savePhone()) {
					Toast.makeText(getActivity().getApplicationContext(),
							update_success, Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(getActivity().getApplicationContext(),
							update_fail, Toast.LENGTH_LONG).show();
				}
			}
		} else if (item.getItemId() == android.R.id.home) {
			Fragment fragment = new HotCallFragment();
			AppFragmentActivity act = (AppFragmentActivity) getActivity();
			act.switchContent(fragment);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.menu_save, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	private boolean savePhone() {
		String name = edtName.getText().toString();
		String phone = edtPhone.getText().toString();

		if (!name.equals("") && !phone.equals("")) {
			if (type == ADD) {
				if (db.checkName(name)) {
					HotTel tel = new HotTel(id, name, phone);
					long add = db.addHotTel(tel);
					if (add != -1)
						return true;
				}
			} else if (type == EDIT) {
				HotTel tel = new HotTel(id, name, phone);
				int edit = db.updateHotTel(old, tel);
				if (edit != 0)
					return true;
			}
		}
		return false;
	}
}
