package com.haputech.littleangel.fragment.game;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.fragment.AppFragment;

public class GameFragment extends AppFragment implements OnClickListener {

	private Button btnPlay, btnScore, btnTut;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_game_menu, null);
		initComponents(view);
		initData();
		addListener();
		return view;
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		btnPlay = (Button) v.findViewById(R.id.btnPlay);
		btnScore = (Button) v.findViewById(R.id.btnHighScore);
		btnTut = (Button) v.findViewById(R.id.btnTutorial);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub
		btnPlay.setOnClickListener(this);
		btnScore.setOnClickListener(this);
		btnTut.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		Fragment fragment = null;
		if (id == R.id.btnPlay) {
			fragment = new GameChooseFragment();
		} else if (id == R.id.btnHighScore) {
			fragment = new GameHighScore();
		} else if (id == R.id.btnTutorial) {
			fragment = new GameTutorial();
		}

		if (fragment != null) {
			AppFragmentActivity act = (AppFragmentActivity) getActivity();
			act.switchContent(fragment);
		}
	}

}
