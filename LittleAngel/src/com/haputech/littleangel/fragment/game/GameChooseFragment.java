package com.haputech.littleangel.fragment.game;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.fragment.AppFragment;
import com.haputech.littleangel.object.Game;

public class GameChooseFragment extends AppFragment implements OnClickListener {

	private Button btnDinhDuong, btnSucKhoe, btnTamLy, btnMeoVat, btnKienThuc;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_game_choose_play, null);
		initComponents(view);
		addListener();
		return view;
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		btnDinhDuong = (Button) v.findViewById(R.id.btnDinhDuong);
		btnSucKhoe = (Button) v.findViewById(R.id.btnSucKhoe);
		btnTamLy = (Button) v.findViewById(R.id.btnTamLy);
		btnMeoVat = (Button) v.findViewById(R.id.btnMeoVat);
		btnKienThuc = (Button) v.findViewById(R.id.btnKienThuc);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub
		btnDinhDuong.setOnClickListener(this);
		btnSucKhoe.setOnClickListener(this);
		btnTamLy.setOnClickListener(this);
		btnKienThuc.setOnClickListener(this);
		btnMeoVat.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		GamePlayFragment fragment = new GamePlayFragment();
		fragment.type = "";
		int id = v.getId();
		if (id == R.id.btnDinhDuong) {
			fragment.type = Game.DINHDUONG;
		} else if (id == R.id.btnTamLy) {
			fragment.type = Game.TAMLY;
		} else if (id == R.id.btnSucKhoe) {
			fragment.type = Game.SUCKHOE;
		} else if (id == R.id.btnMeoVat) {
			fragment.type = Game.SUCKHOE;
		} else if (id == R.id.btnKienThuc) {
			fragment.type = Game.SUCKHOE;
		}

		if (!fragment.type.equals("")) {
			AppFragmentActivity act = (AppFragmentActivity) getActivity();
			act.switchContent(fragment);
		}
	}
}
