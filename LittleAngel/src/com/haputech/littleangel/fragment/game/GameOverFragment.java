package com.haputech.littleangel.fragment.game;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.fragment.AppFragment;

public class GameOverFragment extends AppFragment implements OnClickListener {

	public int score;
	private TextView tvScore;
	private Button btnRestart, btnCancel;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (GamePlayFragment.nhapnhay != null) {
			GamePlayFragment.nhapnhay.cancel(true);
			GamePlayFragment.nhapnhay = null;
		}
		if (GamePlayFragment.countDown != null) {
			GamePlayFragment.countDown.cancel();
			GamePlayFragment.countDown = null;
		}

		View view = inflater.inflate(R.layout.fragment_game_over, null);
		initComponents(view);
		initData();
		addListener();
		return view;
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		tvScore = (TextView) v.findViewById(R.id.tvGameOver);
		btnRestart = (Button) v.findViewById(R.id.btnRestartGame);
		btnCancel = (Button) v.findViewById(R.id.btnCancelGame);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();
		tvScore.setText(getResources().getString(R.string.highscore_game) + " "
				+ score + "\n"
				+ getResources().getString(R.string.restart_game));
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub
		btnRestart.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Fragment fragment = null;
		int id = v.getId();
		if (id == R.id.btnRestartGame) {
			fragment = new GameChooseFragment();
		} else if (id == R.id.btnCancelGame) {
			fragment = new GameFragment();
		}

		if (fragment != null) {
			AppFragmentActivity act = (AppFragmentActivity) getActivity();
			act.switchContent(fragment);
		}
	}
}
