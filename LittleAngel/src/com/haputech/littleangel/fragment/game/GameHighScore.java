package com.haputech.littleangel.fragment.game;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.haputech.littleangel.R;
import com.haputech.littleangel.db.DatabaseGame;
import com.haputech.littleangel.fragment.AppFragment;
import com.haputech.littleangel.object.HighScore;

public class GameHighScore extends AppFragment {

	private ListView lvScore;
	private ArrayList<HighScore> scores;
	private HighScoreAdapter adapter;
	private DatabaseGame db;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_game_highscore, null);
		initComponents(view);
		initData();
		setAdapter();
		return view;
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		lvScore = (ListView) v.findViewById(R.id.lvHighScore);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();

		db = new DatabaseGame(getActivity());
		scores = db.getListHighScore();

		adapter = new HighScoreAdapter(getActivity(),
				android.R.layout.simple_list_item_1, scores);
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub
		lvScore.setAdapter(adapter);
	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub

	}

	class HighScoreAdapter extends ArrayAdapter<HighScore> {

		Context context;
		ArrayList<HighScore> scores;

		public HighScoreAdapter(Context context, int textViewResourceId,
				ArrayList<HighScore> scores) {
			super(context, textViewResourceId, scores);
			// TODO Auto-generated constructor stub
			this.context = context;
			this.scores = scores;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View listView;
			listView = null;
			convertView = null;
			if (convertView == null) {
				listView = new View(context);
				listView = inflater.inflate(R.layout.row_high_score, null);
				TextView tvName = (TextView) listView
						.findViewById(R.id.tvNameScore);
				TextView tvScore = (TextView) listView
						.findViewById(R.id.tvScore);
				tvName.setText(scores.get(position).getName() + "");
				tvScore.setText(scores.get(position).getScore() + "");
			}
			return listView;
		}
	}
}
