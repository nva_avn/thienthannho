package com.haputech.littleangel.fragment.game;

import java.util.ArrayList;
import java.util.Random;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.haputech.littleangel.AppFragmentActivity;
import com.haputech.littleangel.R;
import com.haputech.littleangel.db.DatabaseBaby;
import com.haputech.littleangel.db.DatabaseGame;
import com.haputech.littleangel.fragment.AppFragment;
import com.haputech.littleangel.object.Game;
import com.haputech.littleangel.object.HighScore;
import com.haputech.littleangel.util.ConfigApp;

public class GamePlayFragment extends AppFragment implements OnClickListener {

	public String type;
	private TextView tvQuestion, tvQuestionContent, tvTime;
	private Button btnA, btnB, btnC, btnD;

	private DatabaseGame db;

	private ArrayList<Game> questions;
	private Random random;
	private Game question;
	private String name;
	private int score;
	private int timeInMilis = 60000;
	public static CountDownTimer countDown = null;

	private String answer = "";

	private AlertDialog dialog = null;
	public static AsyncTask<Void, Void, Void> nhapnhay;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setHomeButton(false);
		View view = inflater.inflate(R.layout.fragment_game_play, null);
		initComponents(view);
		initData();
		addListener();
		return view;
	}

	@Override
	protected void initComponents(View v) {
		// TODO Auto-generated method stub
		tvQuestion = (TextView) v.findViewById(R.id.tvQuestion);
		tvQuestionContent = (TextView) v.findViewById(R.id.tvQuestionContent);
		tvTime = (TextView) v.findViewById(R.id.tvTime);
		btnA = (Button) v.findViewById(R.id.btnA);
		btnB = (Button) v.findViewById(R.id.btnB);
		btnC = (Button) v.findViewById(R.id.btnC);
		btnD = (Button) v.findViewById(R.id.btnD);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		super.initData();

		DatabaseBaby dbaby = new DatabaseBaby(getActivity());
		SharedPreferences pref = getActivity().getSharedPreferences(
				ConfigApp.SHARED_PREF, Context.MODE_PRIVATE);
		int babyId = pref.getInt(ConfigApp.BABY_ID, 1);
		name = dbaby.getBaby(babyId).getName();

		db = new DatabaseGame(getActivity());
		questions = db.getListQuestion(type);
		random = new Random();
		score = 0;

		if (questions != null && questions.size() != 0) {
			setQuestion();
		} else {
			tvQuestionContent.setText("");
		}
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void addListener() {
		// TODO Auto-generated method stub
		btnA.setOnClickListener(this);
		btnB.setOnClickListener(this);
		btnC.setOnClickListener(this);
		btnD.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		if (id == R.id.btnA) {
			answer = "a";
			btnA.setBackgroundResource(R.drawable.background_game_solution_choose);
			btnB.setBackgroundResource(R.drawable.background_game_solution);
			btnC.setBackgroundResource(R.drawable.background_game_solution);
			btnD.setBackgroundResource(R.drawable.background_game_solution);
		} else if (id == R.id.btnB) {
			answer = "b";
			btnB.setBackgroundResource(R.drawable.background_game_solution_choose);
			btnA.setBackgroundResource(R.drawable.background_game_solution);
			btnC.setBackgroundResource(R.drawable.background_game_solution);
			btnD.setBackgroundResource(R.drawable.background_game_solution);
		} else if (id == R.id.btnC) {
			answer = "c";
			btnC.setBackgroundResource(R.drawable.background_game_solution_choose);
			btnA.setBackgroundResource(R.drawable.background_game_solution);
			btnB.setBackgroundResource(R.drawable.background_game_solution);
			btnD.setBackgroundResource(R.drawable.background_game_solution);
		} else if (id == R.id.btnD) {
			answer = "d";
			btnD.setBackgroundResource(R.drawable.background_game_solution_choose);
			btnA.setBackgroundResource(R.drawable.background_game_solution);
			btnB.setBackgroundResource(R.drawable.background_game_solution);
			btnC.setBackgroundResource(R.drawable.background_game_solution);
		}

		if (!answer.equals("")) {
			dialog = new AlertDialog.Builder(getActivity())
					.setTitle(getResources().getString(R.string.choose_answer))
					.setMessage(
							getResources().getString(R.string.sure_answer)
									+ answer.toUpperCase())
					.setPositiveButton(getResources().getString(R.string.yes),
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									checkAnswer();
									btnA.setEnabled(false);
									btnB.setEnabled(false);
									btnC.setEnabled(false);
									btnD.setEnabled(false);
								}
							})
					.setNegativeButton(getResources().getString(R.string.no),
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									btnC.setBackgroundResource(R.drawable.background_game_solution);
									btnA.setBackgroundResource(R.drawable.background_game_solution);
									btnB.setBackgroundResource(R.drawable.background_game_solution);
									btnD.setBackgroundResource(R.drawable.background_game_solution);
								}
							}).create();
			dialog.setCancelable(false);
			dialog.show();
		}
	}

	/**
	 * Random cau hoi
	 */
	private void setQuestion() {
		btnC.setBackgroundResource(R.drawable.background_game_solution);
		btnA.setBackgroundResource(R.drawable.background_game_solution);
		btnB.setBackgroundResource(R.drawable.background_game_solution);
		btnD.setBackgroundResource(R.drawable.background_game_solution);

		int rand = random.nextInt(questions.size());

		question = questions.get(rand);
		tvQuestion.setText(getResources().getString(R.string.question) + " "
				+ (score + 1) + " :");
		tvQuestionContent.setText(question.getCauHoi());
		btnA.setText("A. " + question.getCauA());
		btnB.setText("B. " + question.getCauB());
		btnC.setText("C. " + question.getCauC());
		btnD.setText("D. " + question.getCauD());

		// dem gio
		countDown();
	}

	/**
	 * Dem nguoc
	 */
	private void countDown() {
		if (countDown != null)
			countDown.cancel();
		countDown = new CountDownTimer(timeInMilis, 1000) {
			@Override
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub
				tvTime.setText(millisUntilFinished / 1000 + "");
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				Toast.makeText(getActivity(),
						getResources().getString(R.string.time_up),
						Toast.LENGTH_SHORT).show();
				if (score != 0)
					saveHighScore();
				changeScreen();
			}
		};
		countDown.start();
	}

	/**
	 * Kiem tra dap an
	 */
	private void checkAnswer() {
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
		nhapnhay = new AsyncTask<Void, Void, Void>() {

			int time = 0;

			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub
				while (time < 3000) {
					time += 500;
					Log.i("Hoa debug", "-->" + time);
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					publishProgress();
				}
				return null;

			}

			@Override
			protected void onProgressUpdate(Void... values) {
				// TODO Auto-generated method stub
				super.onProgressUpdate(values);
				if (((time / 500) % 2) == 1) {
					if (question.getDapAn().equals("a")) {
						btnA.setBackgroundResource(R.drawable.background_game_solution_correct);
					} else if (question.getDapAn().equals("b")) {
						btnB.setBackgroundResource(R.drawable.background_game_solution_correct);
					} else if (question.getDapAn().equals("c")) {
						btnC.setBackgroundResource(R.drawable.background_game_solution_correct);
					} else if (question.getDapAn().equals("d")) {
						btnD.setBackgroundResource(R.drawable.background_game_solution_correct);
					}
				} else {
					if (question.getDapAn().equals("a")) {
						btnA.setBackgroundResource(R.drawable.background_game_solution);
					} else if (question.getDapAn().equals("b")) {
						btnB.setBackgroundResource(R.drawable.background_game_solution);
					} else if (question.getDapAn().equals("c")) {
						btnC.setBackgroundResource(R.drawable.background_game_solution);
					} else if (question.getDapAn().equals("d")) {
						btnD.setBackgroundResource(R.drawable.background_game_solution);
					}
				}
			}

			@Override
			protected void onPostExecute(Void result) {
				// chon dung
				if (question.checkDapAn(answer)) {
					score++;
					setQuestion();
					Toast.makeText(getActivity(),
							getResources().getString(R.string.correct_answer),
							Toast.LENGTH_SHORT).show();
					btnA.setEnabled(true);
					btnB.setEnabled(true);
					btnC.setEnabled(true);
					btnD.setEnabled(true);
				}

				// chon sai
				else {
					Toast.makeText(getActivity(),
							getResources().getString(R.string.wrong_answer),
							Toast.LENGTH_SHORT).show();
					if (countDown != null)
						countDown.cancel();
					if (score != 0)
						saveHighScore();
					changeScreen();
				}
			}

		};
		nhapnhay.execute();

	}

	/**
	 * Doi man hinh
	 */
	private void changeScreen() {
		setHomeButton(true);
		GameOverFragment gameOver = new GameOverFragment();
		gameOver.score = score;
		AppFragmentActivity act = (AppFragmentActivity) getActivity();
		act.switchContent(gameOver);
	}

	/**
	 * Luu lai neu diem cao
	 */
	private void saveHighScore() {
		ArrayList<HighScore> highScore = db.getListHighScore();
		if (highScore != null) {
			switch (highScore.size()) {
			case 0:
				db.addNewHighhScore(new HighScore(1, name, score));
				break;
			case 1:
				db.addNewHighhScore(new HighScore(2, name, score));
				break;
			case 2:
				db.addNewHighhScore(new HighScore(3, name, score));
				break;
			case 3:
				// kiem tra thu hang
				int thuhang = 0;
				for (thuhang = 0; thuhang < highScore.size(); thuhang++) {
					if (highScore.get(thuhang).getScore() < score)
						break;
				}

				// khong nam trong top 3 thi loai
				if (thuhang == 3) {
					break;
				}

				// nam trong top 3
				else {
					HighScore newScore = new HighScore(highScore.get(thuhang)
							.getId(), name, score);

					// luu lai diem
					db.updateBaby(highScore.get(thuhang), newScore);

					// cap nhat lai danh sach
					for (int i = thuhang; i < highScore.size(); i++) {
						db.updateBaby(highScore.get(thuhang + 1),
								highScore.get(thuhang));
					}
				}
				break;
			}
		}
	}
}
