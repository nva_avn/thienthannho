package com.haputech.littleangel;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;

/**
 * Activity cho app
 * 
 * @author NgVietAn
 * 
 */
public abstract class AppActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#cd1e6e")));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == android.R.id.home)
			onBackPressed();
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Khởi tạo các thành phần trong layout
	 */
	protected abstract void initComponents();

	/**
	 * Khởi tạo dữ liệu ban đầu
	 */
	protected abstract void initData();

	/**
	 * Set Adapter
	 */
	protected abstract void setAdapter();

	/**
	 * Thêm các listener cho các thành phần trong layout
	 */
	protected abstract void addListeners();

}
