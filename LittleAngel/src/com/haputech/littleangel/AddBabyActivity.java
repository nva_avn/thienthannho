package com.haputech.littleangel;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

import com.haputech.littleangel.db.DatabaseBaby;
import com.haputech.littleangel.db.DatabaseRemind;
import com.haputech.littleangel.object.Baby;
import com.haputech.littleangel.object.Remind;
import com.haputech.littleangel.util.ImageHelper;
import com.haputech.littleangel.util.RemindReceiver;

import eu.janmuller.android.simplecropimage.CropImage;

/**
 * Them hoac chinh sua thong tin be
 * 
 * @author NgVietAn
 * 
 */
public class AddBabyActivity extends AppActivity implements OnClickListener,
		OnCheckedChangeListener, OnFocusChangeListener {

	/**
	 * Gia tri type : Them be moi
	 */
	public static final int ADD = 100;

	/**
	 * Gia tri type : Chinh sua thong tin be
	 */
	public static final int EDIT = 101;

	/**
	 * Gia tri mode : be da ra doi
	 */
	private static final int BIRTHDAY = 1;

	/**
	 * Gia tri mode : be chua ra doi
	 */
	private static final int PREGNANCY = 2;

	/**
	 * Chup anh tu may anh
	 */
	private static final int TAKE_PICTURE = 4;

	/**
	 * Crop anh
	 */
	private static final int REQUEST_CODE_CROP_IMAGE = 5;

	/**
	 * Chon anh tu the nho
	 */
	private static final int SELECT_PHOTO = 6;

	private ImageView ivBaby;
	private EditText edtName, edtFullName, edtBirthday, edtPregnancy;
	private RadioGroup radGroupBirth, radSex;
	private RadioButton rbBirth, rbPreg, rbMale, rbFemale;
	private LinearLayout layout_birthday, layout_preg;

	/**
	 * Loai ADD : them, EDIT : chinh sua
	 */
	private int type;

	/**
	 * Che do BIRTHDAY : da ra doi PREGNANCY : chua ra doi
	 */
	private int mode;

	private Uri imageUri;
	private String name, fullname, image, day;
	private int id, sex;
	private long time;
	private DatabaseBaby db;
	private Resources res;
	private Baby old;
	private Calendar calendar = Calendar.getInstance();
	private SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
	DatePickerDialog.OnDateSetListener d = new OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			calendar.set(Calendar.MONTH, monthOfYear);
			calendar.set(Calendar.YEAR, year);
			day = formatDate.format(calendar.getTime());
			if (mode == BIRTHDAY) {
				edtBirthday.setText(day);
			} else if (mode == PREGNANCY) {
				edtPregnancy.setText(day);
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_baby);
		initComponents();
		addListeners();
		initData();

	}

	@Override
	protected void initComponents() {
		// TODO Auto-generated method stub
		ivBaby = (ImageView) findViewById(R.id.ivBaby);
		edtName = (EditText) findViewById(R.id.edtName);
		edtFullName = (EditText) findViewById(R.id.edtFullName);
		edtBirthday = (EditText) findViewById(R.id.edtBirthday);
		edtPregnancy = (EditText) findViewById(R.id.edtPregnancy);
		radGroupBirth = (RadioGroup) findViewById(R.id.radGroupBirth);
		radSex = (RadioGroup) findViewById(R.id.radSex);

		rbBirth = (RadioButton) findViewById(R.id.radBirth);
		rbPreg = (RadioButton) findViewById(R.id.radPreg);
		rbMale = (RadioButton) findViewById(R.id.radMale);
		rbFemale = (RadioButton) findViewById(R.id.radFemale);

		layout_birthday = (LinearLayout) findViewById(R.id.layout_birthday);
		layout_preg = (LinearLayout) findViewById(R.id.layout_preg);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		type = getIntent().getExtras().getInt("TYPE");
		res = getResources();
		db = new DatabaseBaby(this);

		// neu la them moi
		if (type == ADD) {
			id = db.getMaxID() + 1;
			sex = -1;
			mode = 0;
			image = "";
			imageUri = null;
		}
		// neu la chinh sua thi khoi thong tin ban dau
		else if (type == EDIT) {
			getActionBar().setTitle(getResources().getString(R.string.edit));
			id = getIntent().getExtras().getInt("ID");
			old = db.getBaby(id);
			sex = old.getSex();
			edtName.setText(old.getName());
			if (sex == -1) {
				rbPreg.setChecked(true);
				day = formatDate.format(old.getPregnancy());
				edtPregnancy.setText(formatDate.format(old.getPregnancy()));
			} else {
				rbBirth.setChecked(true);
				day = formatDate.format(old.getBirthday());
				edtBirthday.setText(formatDate.format(old.getBirthday()));
				edtFullName.setText(old.getName());
				if (sex == Baby.MALE) {
					rbMale.setChecked(true);
				} else if (sex == Baby.FEMALE) {
					rbFemale.setChecked(true);
				}
			}
			image = old.getImage();
			if (!old.getImage().equals("")) {
				Bitmap bitmap = BitmapFactory.decodeFile(old.getImage());
				ivBaby.setImageBitmap(ImageHelper.getResizedBitmapByWidth(
						bitmap, 100));
			}
		}
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void addListeners() {
		// TODO Auto-generated method stub
		edtBirthday.setOnFocusChangeListener(this);
		edtPregnancy.setOnFocusChangeListener(this);
		ivBaby.setOnClickListener(this);

		radGroupBirth.setOnCheckedChangeListener(this);
		radSex.setOnCheckedChangeListener(this);

		registerForContextMenu(ivBaby);
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// TODO Auto-generated method stub
		switch (group.getId()) {
		case R.id.radGroupBirth:
			switch (checkedId) {
			case R.id.radBirth:
				layout_birthday.setVisibility(View.VISIBLE);
				layout_preg.setVisibility(View.GONE);
				mode = BIRTHDAY;
				break;
			case R.id.radPreg:
				layout_birthday.setVisibility(View.GONE);
				layout_preg.setVisibility(View.VISIBLE);
				mode = PREGNANCY;
				sex = -1;
				break;
			}
			break;
		case R.id.radSex:
			switch (checkedId) {
			case R.id.radMale:
				sex = Baby.MALE;
				break;
			case R.id.radFemale:
				sex = Baby.FEMALE;
				break;
			default:
				sex = -1;
				break;
			}
			break;
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.ivBaby) {
			openContextMenu(ivBaby);
		}
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		if (v.getId() == R.id.edtBirthday || v.getId() == R.id.edtPregnancy) {
			if (hasFocus) {
				new DatePickerDialog(this, d, calendar.get(Calendar.YEAR),
						calendar.get(Calendar.MONTH),
						calendar.get(Calendar.DAY_OF_MONTH)).show();
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == android.R.id.home) {
			Intent back = getIntent();
			setResult(RESULT_CANCELED, back);
			finish();
		} else if (item.getItemId() == R.id.action_save) {
			if (saveBaby()) {
				if (mode == BIRTHDAY) {
					Intent back = getIntent();
					setResult(RESULT_OK, back);
					finish();
				} else if (mode == PREGNANCY) {
					new SetRemind().execute();
				}
			}
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_save, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == TAKE_PICTURE) {
			if (resultCode == Activity.RESULT_OK) {
				cropImage();
			}
		}

		else if (requestCode == REQUEST_CODE_CROP_IMAGE) {
			String path = data.getStringExtra(CropImage.IMAGE_PATH);

			// if nothing received
			if (path == null) {

				return;
			}

			// cropped bitmap
			Bitmap bitmap = BitmapFactory.decodeFile(imageUri.getPath());
			ivBaby.setImageBitmap(ImageHelper.getResizedBitmapByWidth(bitmap,
					100));

		}

		else if (requestCode == SELECT_PHOTO) {
			if (resultCode == RESULT_OK) {
				Cursor cursor = getContentResolver().query(data.getData(),
						null, null, null, null);

				cursor.moveToFirst();
				int idx = cursor
						.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
				imageUri = Uri.parse(cursor.getString(idx));

				cropImage();
			}
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		Resources res = getResources();

		menu.setHeaderTitle(res.getString(R.string.pic_from));
		menu.add(0, 1, 0, res.getString(R.string.camera));
		menu.add(0, 2, 0, res.getString(R.string.sdcard));
	}

	@Override
	public boolean onContextItemSelected(android.view.MenuItem item) {
		if (item.getItemId() == 1) {
			Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
			File photo = new File(Environment.getExternalStorageDirectory(),
					"Pic" + id + ".jpg");
			intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
			imageUri = Uri.fromFile(photo);
			startActivityForResult(intent, TAKE_PICTURE);
		} else if (item.getItemId() == 2) {
			Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
			photoPickerIntent.setType("image/*");
			startActivityForResult(photoPickerIntent, SELECT_PHOTO);
		}
		return super.onContextItemSelected(item);
	}

	private void cropImage() {
		// create explicit intent
		Intent intent = new Intent(this, CropImage.class);

		// tell CropImage activity to look for image to crop
		String filePath = imageUri.getPath();
		intent.putExtra(CropImage.IMAGE_PATH, filePath);

		// allow CropImage activity to rescale image
		intent.putExtra(CropImage.SCALE, true);

		// if the aspect ratio is fixed to ratio 3/2
		intent.putExtra(CropImage.ASPECT_X, 1);
		intent.putExtra(CropImage.ASPECT_Y, 1);

		// start activity CropImage with certain request code and listen
		// for result
		startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
	}

	/**
	 * Luu lai thong tin be
	 * 
	 * @return true neu thanh cong, false neu that bai
	 */
	private boolean saveBaby() {
		Baby baby = null;
		name = edtName.getText().toString();
		if (imageUri != null)
			image = imageUri.getPath() + "";
		if (name.equals("")) {
			Toast.makeText(getApplicationContext(),
					res.getString(R.string.fail_name), Toast.LENGTH_SHORT)
					.show();
			return false;
		}
		if (mode == BIRTHDAY) {
			fullname = edtFullName.getText().toString();
			if (sex == -1) {
				Toast.makeText(getApplicationContext(),
						res.getString(R.string.fail_sex), Toast.LENGTH_SHORT)
						.show();
				return false;
			}
			if (day.equals("")) {
				Toast.makeText(getApplicationContext(),
						res.getString(R.string.fail_birthday),
						Toast.LENGTH_SHORT).show();
				return false;
			}
			time = calendar.getTimeInMillis();
			baby = new Baby(id, name, fullname, time, 0, sex, image);
		} else if (mode == PREGNANCY) {
			if (day.equals("")) {
				Toast.makeText(getApplicationContext(),
						res.getString(R.string.fail_preg), Toast.LENGTH_SHORT)
						.show();
				return false;
			}
			time = calendar.getTimeInMillis();
			baby = new Baby(id, name, fullname, 0, time, sex, image);
		}
		if (type == ADD) {
			if (db.addBaby(baby) == -1) {
				Toast.makeText(getApplicationContext(),
						res.getString(R.string.add_fail), Toast.LENGTH_SHORT)
						.show();
				return false;
			} else {
				Toast.makeText(getApplicationContext(),
						res.getString(R.string.add_success), Toast.LENGTH_SHORT)
						.show();
				return true;
			}
		} else if (type == EDIT) {
			if (db.updateBaby(old, baby) == 0) {
				Toast.makeText(getApplicationContext(),
						res.getString(R.string.update_fail), Toast.LENGTH_SHORT)
						.show();
				return false;
			} else {
				Toast.makeText(getApplicationContext(),
						res.getString(R.string.update_success),
						Toast.LENGTH_SHORT).show();
				return true;
			}
		}

		return false;
	}

	/**
	 * Quay lai main screen
	 */
	private void setRemindResult() {
		Intent back = getIntent();
		setResult(RESULT_OK, back);
		finish();
	}

	/**
	 * Them nhac nho kham thai neu dang mang thai
	 * 
	 * @author NgVietAn
	 * 
	 */
	class SetRemind extends AsyncTask<Void, Void, Void> {

		private ProgressDialog progressDialog;
		private String[] name;
		private int[] week;

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			name = res.getStringArray(R.array.khamthai);
			week = res.getIntArray(R.array.khamthai_week);

			Calendar calendar = Calendar.getInstance();
			int remindId;
			long today = calendar.getTimeInMillis();
			long time;
			String title, content, status;

			// intent receive
			Intent intent = new Intent(AddBabyActivity.this,
					RemindReceiver.class);
			PendingIntent pendingIntent;

			calendar.setTime(new Date(AddBabyActivity.this.time));
			calendar.set(Calendar.HOUR_OF_DAY, 7);
			calendar.set(Calendar.MINUTE, 0);

			DatabaseRemind dr = new DatabaseRemind(AddBabyActivity.this);

			// thời gian 1 tuần tính bằng milisecond
			long week_miliseconds = 604800000;

			// tính thời gian lần khám thai
			long tmp = calendar.getTimeInMillis();
			long time_week;
			for (int i = 0; i < 10; i++) {
				time_week = 0;
				time_week = tmp + week[i] * week_miliseconds;
				calendar.setTimeInMillis(time_week);

				time = calendar.getTimeInMillis();
				remindId = dr.getMaxID() + 1;
				title = name[i];
				content = getResources().getString(R.string.khamthai) + " "
						+ title;
				if (time_week <= today)
					status = getResources().getString(R.string.skip);
				else
					status = getResources().getString(R.string.will);

				// set Alarm
				intent.putExtra("TITLE", Remind.KHAMTHAI + " " + title);
				intent.putExtra("CONTENT", content);
				pendingIntent = PendingIntent.getBroadcast(
						AddBabyActivity.this, remindId, intent, 0);

				Remind remind = new Remind(remindId, id, Remind.KHAMTHAI,
						title, content, time, Remind.VOICE_ON, 0, status);
				dr.addRemind(remind, pendingIntent);

				AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
				if (type == EDIT) {
					am.cancel(DatabaseRemind.intentArray.get(remind.getID()));
				}
				am.set(AlarmManager.RTC_WAKEUP, time, pendingIntent);
			}

			return null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog = new ProgressDialog(AddBabyActivity.this);
			progressDialog.setMessage(res.getString(R.string.wait));
			progressDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (progressDialog.isShowing())
				progressDialog.dismiss();
			setRemindResult();
		}

	}
}
