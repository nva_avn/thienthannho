package com.haputech.littleangel.communicate;

import java.util.Date;

/**
 * Listener su kien thay doi thoi gian lay tu date/time picker
 * 
 * @author NgVietAn
 * 
 */
public interface OnTimeCommitListener {

	public void OnTimeCommit(Date time);
}
