package com.haputech.littleangel.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.haputech.littleangel.R;
import com.haputech.littleangel.object.HotTel;

/**
 * Adapter danh sach danh ba
 * 
 * @author NgVietAn
 * 
 */
public class HotCallAdapter extends ArrayAdapter<HotTel> {

	Context context;
	ArrayList<HotTel> hotTel;

	public HotCallAdapter(Context context, int resource,
			ArrayList<HotTel> hotTel) {
		super(context, resource, hotTel);
		this.context = context;
		this.hotTel = hotTel;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View listView;
		listView = null;
		convertView = null;
		if (convertView == null) {
			listView = new View(context);
			listView = inflater.inflate(R.layout.row_multitext_img, null);
			TextView tvPhone = (TextView) listView.findViewById(R.id.tvContent);
			TextView tvName = (TextView) listView.findViewById(R.id.tvTitle2);
			ImageView ivPhone = (ImageView) listView.findViewById(R.id.ivType2);

			HotTel tel = hotTel.get(position);
			tvName.setText(tel.getName());
			tvPhone.setText(tel.getTel());
			ivPhone.setImageResource(R.drawable.ic_hotcall);
		}
		return listView;
	}

	/**
	 * Cap nhat danh ba
	 * 
	 * @param tel
	 *            danh ba moi
	 */
	public void updateData(ArrayList<HotTel> tel) {
		this.hotTel.clear();
		this.hotTel.addAll(tel);
		this.notifyDataSetChanged();
	}
}
