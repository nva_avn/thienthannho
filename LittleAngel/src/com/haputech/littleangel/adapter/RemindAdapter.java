package com.haputech.littleangel.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.haputech.littleangel.R;
import com.haputech.littleangel.object.Remind;

/**
 * Adapter danh sach nhac nho
 * 
 * @author NgVietAn
 * 
 */
public class RemindAdapter extends ArrayAdapter<Remind> {

	Context context;
	ArrayList<Remind> remind;

	public RemindAdapter(Context context, int resource, ArrayList<Remind> remind) {
		super(context, resource, remind);
		this.context = context;
		this.remind = remind;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View listView;
		listView = null;
		convertView = null;
		if (convertView == null) {
			listView = new View(context);
			listView = inflater.inflate(R.layout.row_multitext_img, null);
			TextView tvTime = (TextView) listView.findViewById(R.id.tvContent);
			TextView tvRemind = (TextView) listView.findViewById(R.id.tvTitle2);
			ImageView ivRemind = (ImageView) listView
					.findViewById(R.id.ivType2);

			Remind rm = remind.get(position);

			SimpleDateFormat formatTime = new SimpleDateFormat(
					"dd/MM/yyyy  HH:mm");

			tvTime.setText(formatTime.format(rm.getTime()));
			tvRemind.setText(rm.getTitle());
			ivRemind.setImageResource(R.drawable.ic_remind);
		}
		return listView;
	}

	public void updateData(ArrayList<Remind> remind) {
		this.remind.clear();
		this.remind.addAll(remind);
		this.notifyDataSetChanged();
	}
}
