package com.haputech.littleangel.adapter;

import java.util.ArrayList;
import com.haputech.littleangel.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Adapter danh sach cac loai nhac nho
 * 
 * @author NgVietAn
 * 
 */
public class RemindTypeAdapter extends ArrayAdapter<String> {

	private Context context;
	private ArrayList<String> remind;

	public RemindTypeAdapter(Context context, int textViewResourceId,
			ArrayList<String> remind) {
		super(context, textViewResourceId, remind);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.remind = remind;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View listView;
		listView = null;
		convertView = null;
		if (convertView == null) {
			listView = new View(context);
			listView = inflater.inflate(R.layout.row_text_img, null);
			TextView tvBookName = (TextView) listView
					.findViewById(R.id.tvTitle1);
			ImageView ivBook = (ImageView) listView.findViewById(R.id.ivType);
			tvBookName.setText(remind.get(position));
			ivBook.setImageResource(R.drawable.ic_remind);
		}
		return listView;
	}

}
