package com.haputech.littleangel.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.haputech.littleangel.R;

/**
 * Adapter danh sach cam nang
 * 
 * @author NgVietAn
 * 
 */
public class BookAdapter extends ArrayAdapter<String> {

	Context context;
	ArrayList<String> book_name;

	public BookAdapter(Context context, int textViewResourceId,
			ArrayList<String> book_name) {
		super(context, textViewResourceId, book_name);
		this.context = context;
		this.book_name = book_name;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View listView;
		listView = null;
		convertView = null;
		if (convertView == null) {
			listView = new View(context);
			listView = inflater.inflate(R.layout.row_text_img, null);
			TextView tvBookName = (TextView) listView
					.findViewById(R.id.tvTitle1);
			ImageView ivBook = (ImageView) listView.findViewById(R.id.ivType);
			tvBookName.setText(book_name.get(position));
			ivBook.setImageResource(R.drawable.ic_book);
		}
		return listView;
	}

}
