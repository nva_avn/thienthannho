package com.haputech.littleangel.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.haputech.littleangel.R;
import com.haputech.littleangel.object.Remind;
import com.org.QuickActionActivity.ActionItem;
import com.org.QuickActionActivity.QuickAction;
import com.org.QuickActionActivity.QuickAction.OnActionItemClickListener;

/**
 * Adapter danh sach kham thai
 * 
 * @author NgVietAn
 * 
 */
public class KhamThaiAdapter extends ArrayAdapter<Remind> {

	Context context;
	ArrayList<Remind> list;

	public KhamThaiAdapter(Context context, int textViewResourceId,
			ArrayList<Remind> list) {
		super(context, textViewResourceId, list);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.list = list;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View listView;
		listView = null;
		convertView = null;
		if (convertView == null) {
			listView = new View(context);
			listView = inflater.inflate(R.layout.row_multitext_button, null);
			TextView tvLankham = (TextView) listView
					.findViewById(R.id.tvTitle3);
			TextView tvThoigianKham = (TextView) listView
					.findViewById(R.id.tvContent2);
			final ImageView btStatus = (ImageView) listView
					.findViewById(R.id.btState);

			final Remind remind = list.get(position);
			SimpleDateFormat formatTime = new SimpleDateFormat(
					"dd/MM/yyyy  HH:mm");

			tvLankham.setText(remind.getTitle());
			tvThoigianKham.setText(formatTime.format(remind.getTime()));

			if (remind.getStatus().equals(
					context.getResources().getString(R.string.will))) {
				btStatus.setImageResource(R.drawable.ic_will);
			} else if (remind.getStatus().equals(
					context.getResources().getString(R.string.skip))) {
				btStatus.setImageResource(R.drawable.ic_skip);
			} else if (remind.getStatus().equals(
					context.getResources().getString(R.string.done))) {
				btStatus.setImageResource(R.drawable.ic_done);
			}

			// create quick action menu
			final ActionItem will = new ActionItem();
			will.setTitle(context.getResources().getString(R.string.will));
			will.setIcon(context.getResources().getDrawable(R.drawable.ic_will));

			final ActionItem skip = new ActionItem();
			skip.setTitle(context.getResources().getString(R.string.skip));
			skip.setIcon(context.getResources().getDrawable(R.drawable.ic_skip));

			final ActionItem ed = new ActionItem();
			ed.setTitle(context.getResources().getString(R.string.done));
			ed.setIcon(context.getResources().getDrawable(R.drawable.ic_done));

			final QuickAction quickAction = new QuickAction(context);
			quickAction.addActionItem(will);
			quickAction.addActionItem(skip);
			quickAction.addActionItem(ed);

			quickAction
					.setOnActionItemClickListener(new OnActionItemClickListener() {

						@Override
						public void onItemClick(int pos) {
							Drawable drawable = null;
							String status = "";
							if (pos == 0) {
								drawable = will.getIcon();
								status = will.getTitle();
							} else if (pos == 1) {
								drawable = skip.getIcon();
								status = skip.getTitle();
							} else if (pos == 2) {
								drawable = ed.getIcon();
								status = ed.getTitle();
							}
							btStatus.setImageDrawable(drawable);
							remind.setStatus(status);
						}
					});

			btStatus.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					quickAction.show(v);
					quickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_CENTER);
				}
			});

		}
		return listView;
	}
}
