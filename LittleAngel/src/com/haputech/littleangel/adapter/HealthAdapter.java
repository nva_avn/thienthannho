package com.haputech.littleangel.adapter;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.haputech.littleangel.R;
import com.haputech.littleangel.db.DatabaseGrowth;
import com.haputech.littleangel.object.Growth;

/**
 * Adapter danh sach danh gia suc khoe
 * 
 * @author NgVietAn
 * 
 */
public class HealthAdapter extends ArrayAdapter<Growth> {

	Context context;
	ArrayList<Growth> growths;

	public HealthAdapter(Context context, int textViewResourceId,
			ArrayList<Growth> growths) {
		super(context, textViewResourceId, growths);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.growths = growths;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View listView;
		listView = null;
		convertView = null;
		if (convertView == null) {
			listView = new View(context);
			listView = inflater.inflate(R.layout.row_health_data, null);
			TextView tvAge = (TextView) listView.findViewById(R.id.tvAge);
			TextView tvData = (TextView) listView.findViewById(R.id.tvData);
			TextView tvRate = (TextView) listView.findViewById(R.id.tvRate);
			ImageButton btnDelete = (ImageButton) listView
					.findViewById(R.id.btnDelete);

			final Resources res = context.getResources();
			final Growth growth = growths.get(position);
			tvAge.setText(growth.getAge());
			tvData.setText(res.getString(R.string.weight_data)
					+ growth.getWeight() + " kg, "
					+ res.getString(R.string.height_data) + growth.getHeight()
					+ " cm");
			tvRate.setText(growth.getRate());

			final DatabaseGrowth db = new DatabaseGrowth(context);
			btnDelete.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					AlertDialog del = new AlertDialog.Builder(context)
							.setTitle(res.getString(R.string.delete))
							.setMessage(res.getString(R.string.sure_delete))
							.setPositiveButton(res.getString(R.string.yes),
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											db.deleteData(growth);
											growths.remove(position);
											notifyDataSetChanged();
										}
									})
							.setNegativeButton(res.getString(R.string.no),
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											// TODO Auto-generated method stub

										}
									}).create();
					del.show();
				}
			});
		}
		return listView;
	}
}
