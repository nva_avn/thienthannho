package com.haputech.littleangel.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.haputech.littleangel.R;
import com.haputech.littleangel.object.Remind;
import com.org.QuickActionActivity.ActionItem;
import com.org.QuickActionActivity.QuickAction;
import com.org.QuickActionActivity.QuickAction.OnActionItemClickListener;

/**
 * Adapter danh sach tiem chung
 * 
 * @author NgVietAn
 * 
 */
public class TiemChungAdapter extends ArrayAdapter<Remind> {

	Context context;
	ArrayList<Remind> list;

	public TiemChungAdapter(Context context, int textViewResourceId,
			ArrayList<Remind> list) {
		super(context, textViewResourceId, list);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.list = list;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View listView;
		listView = null;
		convertView = null;
		if (convertView == null) {
			listView = new View(context);
			listView = inflater.inflate(R.layout.row_multitext_button, null);
			TextView tvLankham = (TextView) listView
					.findViewById(R.id.tvTitle3);
			TextView tvThoigianKham = (TextView) listView
					.findViewById(R.id.tvContent2);
			final ImageView btStatus = (ImageView) listView
					.findViewById(R.id.btState);

			final Remind remind = list.get(position);
			SimpleDateFormat formatTime = new SimpleDateFormat(
					"dd/MM/yyyy  HH:mm");

			tvLankham.setText(remind.getTitle());
			if (remind.getTime() != 0)
				tvThoigianKham.setText(formatTime.format(remind.getTime()));
			else
				tvThoigianKham.setText("");
			if (remind.getStatus().equals(
					context.getResources().getString(R.string.will))) {
				btStatus.setImageResource(R.drawable.ic_will);
			} else if (remind.getStatus().equals(
					context.getResources().getString(R.string.done))) {
				btStatus.setImageResource(R.drawable.ic_done);
			}

			// create quick action menu
			final ActionItem will = new ActionItem();
			will.setTitle(context.getResources().getString(R.string.will));
			will.setIcon(context.getResources().getDrawable(R.drawable.ic_will));

			final ActionItem ed = new ActionItem();
			ed.setTitle(context.getResources().getString(R.string.done));
			ed.setIcon(context.getResources().getDrawable(R.drawable.ic_done));

			final QuickAction quickAction = new QuickAction(context);
			quickAction.addActionItem(will);
			quickAction.addActionItem(ed);

			quickAction
					.setOnActionItemClickListener(new OnActionItemClickListener() {

						@Override
						public void onItemClick(int pos) {
							Drawable drawable = null;
							String status = "";
							if (pos == 0) {
								drawable = will.getIcon();
								status = will.getTitle();

							} else if (pos == 1) {
								drawable = ed.getIcon();
								status = ed.getTitle();
							}
							Log.i("Tiem chung ", status);
							btStatus.setImageDrawable(drawable);
							remind.setStatus(status);
						}
					});

			btStatus.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					quickAction.show(v);
					quickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_CENTER);
				}
			});

		}
		return listView;
	}
}
