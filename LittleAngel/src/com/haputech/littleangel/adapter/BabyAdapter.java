package com.haputech.littleangel.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.haputech.littleangel.R;
import com.haputech.littleangel.object.Baby;
import com.haputech.littleangel.util.ImageHelper;

/**
 * Adapter danh sach be
 * 
 * @author NgVietAn
 * 
 */
public class BabyAdapter extends ArrayAdapter<Baby> {

	private Context context;
	private ArrayList<Baby> baby;

	public BabyAdapter(Context context, int textViewResourceId,
			ArrayList<Baby> baby) {
		super(context, textViewResourceId, baby);
		this.context = context;
		this.baby = baby;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View listView;
		listView = null;
		convertView = null;
		if (convertView == null) {
			listView = new View(context);
			listView = inflater.inflate(R.layout.row_main_baby, null);
			ImageView ivBaby = (ImageView) listView
					.findViewById(R.id.ivBabyMain);
			TextView tvNameBaby = (TextView) listView
					.findViewById(R.id.tvNameBaby);
			TextView tvOldBaby = (TextView) listView
					.findViewById(R.id.tvOldBaby);

			if (position % 2 == 0) {
				listView.setBackgroundResource(R.color.baby_blue);
			} else {
				listView.setBackgroundResource(R.color.baby_red);
			}

			SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
			Baby bb = baby.get(position);
			tvNameBaby.setText(bb.getName());
			if (bb.getBirthday() != 0) {
				tvOldBaby.setText(formatDate.format(bb.getBirthday()));
			} else {
				tvOldBaby.setText("");
			}
			if (!bb.getImage().equals("")) {
				Bitmap bitmap = BitmapFactory.decodeFile(bb.getImage());
				ivBaby.setImageBitmap(ImageHelper.getResizedBitmapByWidth(
						bitmap, 100));
			}
		}
		return listView;
	}

	/**
	 * Cap nhat danh sach be
	 * 
	 * @param baby
	 *            danh sach be moi
	 */
	public void updateData(ArrayList<Baby> baby) {
		this.baby.clear();
		this.baby.addAll(baby);
		this.notifyDataSetChanged();
	}
}
