package com.haputech.littleangel;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

/**
 * Dialog nhac nho
 * 
 * @author NgVietAn
 * 
 */
public class RemindAlarm extends Activity {

	/**
	 * Phat nhac
	 */
	MediaPlayer mp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mp = MediaPlayer.create(this, R.drawable.alarm_sound);

		showDialog();
		showNotification();
	}

	/**
	 * Hien thi dialog
	 */
	public void showDialog() {
		AlertDialog dialog = create();
		dialog.show();
	}

	/**
	 * Tao dialog
	 * 
	 * @return dialog
	 */
	public AlertDialog create() {
		Intent i = getIntent();
		mp.start();
		String message = i.getExtras().getString("CONTENT");
		String title = i.getExtras().getString("TITLE");
		AlertDialog alert = new AlertDialog.Builder(this).setTitle(title)
				.setMessage(message)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						mp.stop();
						onBackPressed();
					}
				}).create();
		return alert;
	}

	/**
	 * Hien thi notification
	 */
	@SuppressWarnings("deprecation")
	public void showNotification() {
		NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		Intent intent = getIntent();
		int id = intent.getExtras().getInt("ID");
		String title = intent.getExtras().getString("TITLE");
		String content = intent.getExtras().getString("CONTENT");
		PendingIntent contentIntent = PendingIntent.getBroadcast(this, 0,
				intent, 0);
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this).setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(title).setContentText(content);
		mBuilder.setAutoCancel(true);
		mBuilder.setContentIntent(contentIntent);
		mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
		nm.notify(id, mBuilder.getNotification());
	}

	@Override
	public void onBackPressed() {
		this.finish();
		if (this.isFinishing())
			onDestroy();
		super.onBackPressed();
	}
}
