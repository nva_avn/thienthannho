package com.haputech.littleangel.object;

/**
 * Baby
 * 
 * @author NgVietAn
 * 
 */
public class Baby {

	private int id, sex;
	private String name, image, fullname;
	private long birthday, pregnancy;

	public static final int MALE = 1;
	public static final int FEMALE = 0;

	public Baby(int id, String name, String fullname, long birthday,
			long pregnancy, int sex, String image) {
		this.id = id;
		this.name = name;
		this.fullname = fullname;
		this.birthday = birthday;
		this.pregnancy = pregnancy;
		this.sex = sex;
		this.image = image;
	}

	/**
	 * Set id for baby
	 * 
	 * @param id
	 */
	public void setID(int id) {
		this.id = id;
	}

	/**
	 * Set name for baby
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Set sex for baby
	 * 
	 * @param sex
	 *            1 : MALE, 0 : FEMALE
	 */
	public void setSex(int sex) {
		this.sex = sex;
	}

	/**
	 * Get id of baby
	 * 
	 * @return
	 */
	public int getID() {
		return id;
	}

	/**
	 * Get name of baby
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get fullname of baby
	 * 
	 * @return
	 */
	public String getFullname() {
		return fullname;
	}

	/**
	 * Get sex of baby
	 * 
	 * @return 0 if female, 1 if male, -1 if pregnancy
	 */
	public int getSex() {
		return sex;
	}

	/**
	 * Get path image of baby
	 * 
	 * @return
	 */
	public String getImage() {
		return image;
	}

	/**
	 * Set path image for baby
	 * 
	 * @param image
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * Get birhtday of baby
	 * 
	 * @return
	 */
	public long getBirthday() {
		return birthday;
	}

	/**
	 * Set birthday for baby
	 * 
	 * @param birthday
	 */
	public void setBirthday(long birthday) {
		this.birthday = birthday;
	}

	/**
	 * Get day of pregnancy of baby
	 * 
	 * @return
	 */
	public long getPregnancy() {
		return pregnancy;
	}

	/**
	 * Set day of pregnancy of baby
	 * 
	 * @param pregnancy
	 */
	public void setPregnancy(long pregnancy) {
		this.pregnancy = pregnancy;
	}

}
