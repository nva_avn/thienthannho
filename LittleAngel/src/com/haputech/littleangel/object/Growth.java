package com.haputech.littleangel.object;

public class Growth {

	private int id, babyID;
	private double height, weight;
	private String age, rate;

	public Growth(int id, int babyID, String age, double height, double weight,
			String rate) {
		this.id = id;
		this.babyID = babyID;
		this.age = age;
		this.height = height;
		this.weight = weight;
		this.rate = rate;
	}

	public void setID(int id) {
		this.id = id;
	}

	public void setBabyID(int babyID) {
		this.babyID = babyID;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public int getID() {
		return id;
	}

	public int getBabyID() {
		return babyID;
	}

	public String getAge() {
		return age;
	}

	public double getHeight() {
		return height;
	}

	public double getWeight() {
		return weight;
	}

	public String getRate() {
		return rate;
	}

}
