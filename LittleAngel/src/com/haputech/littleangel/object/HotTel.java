package com.haputech.littleangel.object;

public class HotTel {

	private int id;
	private String name, tel;

	public HotTel(int id, String name, String tel) {
		this.id = id;
		this.name = name;
		this.tel = tel;
	}

	public int getID() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getTel() {
		return tel;
	}

}
