package com.haputech.littleangel.object;

public class Remind {

	private int id, babyID, voice, type;
	private long replay, time;
	private String content, title, status;

	public static final int VOICE_OFF = 1;
	public static final int VOICE_ON = 0;

	public static final int KHAMTHAI = 100;
	public static final int TIEMCHUNG = 101;
	public static final int CHAMSOC = 102;

	public Remind(int id, int babyID, int type, String title, String content,
			long time, int voice, long replay, String status) {
		this.id = id;
		this.babyID = babyID;
		this.type = type;
		this.title = title;
		this.content = content;
		this.time = time;
		this.voice = voice;
		this.replay = replay;
		this.status = status;
	}

	public int getID() {
		return id;
	}

	public int getBabyID() {
		return babyID;
	}

	public int getType() {
		return type;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	public long getTime() {
		return time;
	}

	public int getVoice() {
		return voice;
	}

	public long getReplay() {
		return replay;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public void setId(int id) {
		this.id = id;
	}

}
