package com.haputech.littleangel.object;

public class Game {

	private int id;
	private String cauHoi, cauA, cauB, cauC, cauD, dapAn, type;

	public static final String DINHDUONG = "dinhduong";
	public static final String TAMLY = "tamly";
	public static final String SUCKHOE = "suckhoe";
	public static final String MEOVAT = "meovat";
	public static final String KIENTHUC = "kienthuc";

	public Game(int id, String cauHoi, String cauA, String cauB, String cauC,
			String cauD, String dapAn, String type) {
		super();
		this.id = id;
		this.cauHoi = cauHoi;
		this.cauA = cauA;
		this.cauB = cauB;
		this.cauC = cauC;
		this.cauD = cauD;
		this.dapAn = dapAn;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCauHoi() {
		return cauHoi;
	}

	public void setCauHoi(String cauHoi) {
		this.cauHoi = cauHoi;
	}

	public String getCauA() {
		return cauA;
	}

	public void setCauA(String cauA) {
		this.cauA = cauA;
	}

	public String getCauB() {
		return cauB;
	}

	public void setCauB(String cauB) {
		this.cauB = cauB;
	}

	public String getCauC() {
		return cauC;
	}

	public void setCauC(String cauC) {
		this.cauC = cauC;
	}

	public String getCauD() {
		return cauD;
	}

	public void setCauD(String cauD) {
		this.cauD = cauD;
	}

	public String getDapAn() {
		return dapAn;
	}

	public void setDapAn(String dapAn) {
		this.dapAn = dapAn;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean checkDapAn(String dapAn) {
		return this.dapAn.equals(dapAn);
	}
}
