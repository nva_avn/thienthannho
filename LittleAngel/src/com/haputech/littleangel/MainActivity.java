package com.haputech.littleangel;

import java.io.File;
import java.util.ArrayList;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.haputech.littleangel.adapter.BabyAdapter;
import com.haputech.littleangel.db.DatabaseBaby;
import com.haputech.littleangel.db.DatabaseGrowth;
import com.haputech.littleangel.db.DatabaseRemind;
import com.haputech.littleangel.object.Baby;
import com.haputech.littleangel.object.Growth;
import com.haputech.littleangel.object.Remind;
import com.haputech.littleangel.util.ConfigApp;

/**
 * Man hinh chinh
 * 
 * @author NgVietAn
 * 
 */
public class MainActivity extends AppActivity implements OnItemClickListener {

	private ListView lvBaby;
	private Button btnAddBaby;

	private ArrayList<Baby> listBaby;
	private BabyAdapter adapter;

	private DatabaseBaby db;
	private int pos = -1;
	private static final int ADD_BABY = 100;
	private static final int EDIT_BABY = 101;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_screen);
		initComponents();
		initData();
		setAdapter();
		addListeners();
		getActionBar().setHomeButtonEnabled(false);
		getActionBar().setDisplayHomeAsUpEnabled(false);

	}

	@Override
	protected void initComponents() {
		// TODO Auto-generated method stub
		lvBaby = (ListView) findViewById(R.id.lvBaby);
		btnAddBaby = (Button) findViewById(R.id.btnAddBaby);
		registerForContextMenu(lvBaby);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		db = new DatabaseBaby(this);
		listBaby = new ArrayList<Baby>();
		listBaby = db.getListBaby();
		Log.i("Count baby", listBaby.size() + "");

		if (listBaby.size() == 0) {
			lvBaby.setVisibility(View.GONE);
			btnAddBaby.setVisibility(View.VISIBLE);
			Intent add = new Intent(MainActivity.this, AddBabyActivity.class);
			add.putExtra("TYPE", AddBabyActivity.ADD);
			startActivityForResult(add, ADD_BABY);
		}
	}

	@Override
	protected void setAdapter() {
		// TODO Auto-generated method stub
		adapter = new BabyAdapter(this, android.R.layout.simple_list_item_1,
				listBaby);
		lvBaby.setAdapter(adapter);
	}

	@Override
	protected void addListeners() {
		// TODO Auto-generated method stub
		lvBaby.setOnItemClickListener(this);

		btnAddBaby.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent add = new Intent(MainActivity.this,
						AddBabyActivity.class);
				add.putExtra("TYPE", AddBabyActivity.ADD);
				startActivityForResult(add, ADD_BABY);
			}
		});
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		pos = position;
		openContextMenu(lvBaby);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (requestCode == ADD_BABY && resultCode == RESULT_OK) {
			onResume();
		}
		if (requestCode == EDIT_BABY && resultCode == RESULT_OK) {
			onResume();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		listBaby = db.getListBaby();
		Log.i("Count baby", listBaby.size() + "");
		if (listBaby.size() == 0) {
			lvBaby.setVisibility(View.GONE);
			btnAddBaby.setVisibility(View.VISIBLE);
		} else {
			lvBaby.setVisibility(View.VISIBLE);
			btnAddBaby.setVisibility(View.GONE);
		}
		adapter.updateData(listBaby);
		super.onResume();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == R.id.action_add) {
			Intent add = new Intent(MainActivity.this, AddBabyActivity.class);
			add.putExtra("TYPE", AddBabyActivity.ADD);
			startActivityForResult(add, ADD_BABY);
		} else if (item.getItemId() == android.R.id.home) {

		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_add, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		Resources res = getResources();

		menu.setHeaderTitle(res.getString(R.string.action_menu));
		menu.add(0, 1, 0, res.getString(R.string.view));
		menu.add(0, 2, 0, res.getString(R.string.edit));
		menu.add(0, 3, 0, res.getString(R.string.delete));
	}

	@Override
	public boolean onContextItemSelected(android.view.MenuItem item) {
		if (item.getItemId() == 1) {
			SharedPreferences pref = getSharedPreferences(
					ConfigApp.SHARED_PREF, Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = pref.edit();
			editor.putInt(ConfigApp.BABY_ID, listBaby.get(pos).getID());
			editor.commit();

			Intent menu = new Intent(MainActivity.this,
					AppFragmentActivity.class);
			startActivity(menu);
		} else if (item.getItemId() == 2) {
			Intent add = new Intent(MainActivity.this, AddBabyActivity.class);
			add.putExtra("TYPE", AddBabyActivity.EDIT);
			add.putExtra("ID", listBaby.get(pos).getID());
			startActivityForResult(add, EDIT_BABY);
		} else if (item.getItemId() == 3) {
			if (pos != -1) {
				AlertDialog del = new AlertDialog.Builder(this)
						.setTitle(getResources().getString(R.string.delete))
						.setMessage(
								getResources().getString(R.string.sure_delete))
						.setPositiveButton(
								getResources().getString(R.string.yes),
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										new DeleteBaby().execute(listBaby
												.get(pos));
									}
								})
						.setNegativeButton(
								getResources().getString(R.string.no),
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub

									}
								}).create();
				del.show();

			}
		}
		return super.onContextItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.exit))
				.setIcon(R.drawable.ic_action_remove)
				.setMessage(getResources().getString(R.string.exit_app))
				.setPositiveButton(getResources().getString(R.string.yes),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								MainActivity.this.finish();
							}
						})
				.setNegativeButton(getResources().getString(R.string.no),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {

							}
						}).create().show();
	}

	private void deleteSuccess() {
		listBaby.remove(pos);
		adapter.updateData(listBaby);
		pos = -1;
		Toast.makeText(getApplicationContext(),
				getResources().getString(R.string.del_success),
				Toast.LENGTH_SHORT).show();
		onResume();
	}

	class DeleteBaby extends AsyncTask<Baby, Void, Void> {

		private ProgressDialog progressDialog;
		private Exception e = null;

		@Override
		protected Void doInBackground(Baby... params) {
			// TODO Auto-generated method stub
			Baby baby = params[0];
			int babyId = baby.getID();
			try {
				DatabaseGrowth dg = new DatabaseGrowth(MainActivity.this);
				DatabaseRemind dr = new DatabaseRemind(MainActivity.this);

				// Xoa du lieu phat trien
				ArrayList<Growth> growth = dg.getListGrowth(babyId);
				if (growth != null) {
					for (int i = 0; i < growth.size(); i++) {
						dg.deleteData(growth.get(i));
					}
				}

				// Xoa nhac nho
				ArrayList<Remind> remind = dr.getBabyRemind(babyId);
				AlarmManager am = (AlarmManager) MainActivity.this
						.getSystemService(Context.ALARM_SERVICE);
				if (remind != null) {
					for (int i = 0; i < remind.size(); i++) {
						dr.deleteRemind(remind.get(i));
						am.cancel(DatabaseRemind.intentArray.get(remind.get(i)
								.getID()));
					}
				}

				// Xoa be
				db.deleteBaby(baby);

				// Xoa anh be neu co
				File file = new File(baby.getImage());
				if (file.exists()) {
					file.delete();
				}
			} catch (Exception ex) {
				e = ex;
				return null;
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog = new ProgressDialog(MainActivity.this);
			progressDialog.setMessage(getResources().getString(R.string.wait));
			progressDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (progressDialog.isShowing())
				progressDialog.dismiss();
			if (e == null) {
				deleteSuccess();
			} else {
				Toast.makeText(MainActivity.this,
						getResources().getString(R.string.del_fail),
						Toast.LENGTH_SHORT).show();
			}
		}

	}
}
