package com.haputech.littleangel.util;

import com.haputech.littleangel.RemindAlarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Service receive remind
 * 
 * @author NgVietAn
 * 
 */
public class RemindReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// process alarm
		Intent i = new Intent(context, RemindAlarm.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.putExtras(intent);
		context.startActivity(i);
	}

}
