package com.haputech.littleangel.util;

/**
 * Config App
 * 
 * @author NgVietAn
 * 
 */
public class ConfigApp {
	public static final String SHARED_PREF = "LittleAngelPref";
	public static final String BABY_ID = "babyId";
	public static final String TYPE_BOOK_TIME = "TypeBookTime";
	public static final String TYPE_BOOK = "TypeBook";
	public static final String BOOK_NAME = "BookName";

	// các độ tuổi có dữ liệu để đánh giá
	public static final int[] AGE = { 0, 1, 3, 6, 12, 18, 24, 36, 48, 60 };

	// các thông số đánh giá bé gái
	public static final double[] FEMALE_W_STANDARD = { 3.2, 4.2, 5.8, 7.3, 8.9,
			10.2, 11.5, 13.9, 16.1, 18.2 };
	public static final double[] FEMALE_MALNUTRITION = { 2.4, 3.2, 4.5, 5.7, 7,
			8.1, 9, 10.8, 12.3, 13.7 };
	public static final double[] FEMALE_OBESITY = { 4.2, 5.5, 7.5, 9.3, 11.5,
			13.2, 14.8, 18.1, 21.5, 24.9 };
	public static final double[] FEMALE_H_STANDARD = { 49.1, 53.7, 57.1, 65.7,
			74, 80.7, 86.4, 95.1, 102.7, 109.4 };
	public static final double[] FEMALE_SHORT = { 45.4, 49.8, 55.6, 61.2, 68.9,
			74.9, 80, 87.4, 94.1, 99.9 };

	// các thông số đánh giá bé trai
	public static final double[] MALE_W_STANDARD = { 3.3, 4.5, 6.4, 7.9, 9.6,
			10.9, 12.2, 14.3, 16.3, 18.3 };
	public static final double[] MALE_MALNUTRITION = { 2.4, 3.4, 5, 6.4, 7.7,
			8.8, 9.7, 11.3, 12.7, 14.1 };
	public static final double[] MALE_OBESITY = { 4.4, 5.8, 8, 9.8, 12, 13.7,
			15.3, 18.3, 21.2, 24.2 };
	public static final double[] MALE_H_STANDARD = { 49.9, 54.7, 58.4, 67.6,
			75.7, 82.3, 87.8, 96.1, 103.3, 110 };
	public static final double[] MALE_SHORT = { 46.1, 50.8, 57.3, 63.3, 71.0,
			76.9, 81.7, 88.7, 94.9, 100.7 };
}
