
package com.haputech.littleangel.util;

import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.Log;

public class ImageHelper {
	
	/**
	 * Round corner at the top of bitmap.
	 * 
	 * @param round_size Round size.
	 * @param requiredWidth The required width of bitmap dest.
	 * @param bitmap Bitmap source.
	 * @return Bitmap dest.  
	 */
	public static Bitmap getRoundedCornerTopBitmap(float round_size, int requiredWidth, Bitmap bitmap) {		
		bitmap = getResizedBitmapByWidth(bitmap,requiredWidth);
		
		Bitmap bmp = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmp);
        Shader shader = new BitmapShader(bitmap, Shader.TileMode.MIRROR,Shader.TileMode.MIRROR);

        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setAntiAlias(true);
        paint.setShader(shader);
        RectF rec = new RectF(0, 0, bitmap.getWidth(),bitmap.getHeight() - round_size);
        c.drawRect(new RectF(0, round_size, bitmap.getWidth(),bitmap.getHeight()), paint);
        c.drawRoundRect(rec, round_size, round_size, paint);
        return bmp;
	}

	/**
	 * Resize a bitmap by required width.
	 * 
	 * @param bitmap Bitmap source.
	 * @param requiredWidth The required width of bitmap dest.
	 * @return Bitmap dest.
	 */
	public static Bitmap getResizedBitmapByWidth(Bitmap bitmap, int requiredWidth) {
		int requiredHeight = (int)((float)requiredWidth*bitmap.getHeight())/ bitmap.getWidth();
		return getResizedBitmap(bitmap,requiredHeight,requiredWidth);
	}
	
	/**
	 * Resize a bitmap by required height.
	 * 
	 * @param bitmap Bitmap source.
	 * @param requiredHeight The required height of bitmap dest.
	 * @return Bitmap dest.
	 */
	public static Bitmap getResizedBitmapByHeight(Bitmap bitmap, int requiredHeight) {
		int requiredWidth = (int)((float)requiredHeight*bitmap.getWidth())/ bitmap.getHeight();
		return getResizedBitmap(bitmap,requiredHeight,requiredWidth);
	}
	
	/**
	 * Resize bitmap by new width, new height.
	 * 
	 * @param bm Bitmap source.
	 * @param newHeight New height.
	 * @param newWidth New width.
	 * @return Bitmap dest.
	 */
	public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
	    int width = bm.getWidth();
	    int height = bm.getHeight();
	    float scaleWidth = ((float) newWidth) / width;
	    float scaleHeight = ((float) newHeight) / height;
	    // CREATE A MATRIX FOR THE MANIPULATION
	    Matrix matrix = new Matrix();
	    // RESIZE THE BIT MAP
	    matrix.postScale(scaleWidth, scaleHeight);

	    // "RECREATE" THE NEW BITMAP
	    Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
	    return resizedBitmap;
	}
	
	/**
	 * Resize bitmap by sample
	 * @param is inputstream
	 * @param size required size
	 * @return resized bitmap
	 */
	public static Bitmap scaleBitmap(InputStream is, int size) {		
		try {
			BitmapFactory.Options opt = new BitmapFactory.Options();
			opt.inJustDecodeBounds = false;
			opt.inSampleSize = size;
			return BitmapFactory.decodeStream(is, null,opt);
		}
		catch (Exception e){
			Log.d("SAN",e.getMessage());
			return null;
		}
	}

	public static int calculateBitmapSize(InputStream is, int rw, int rh) { 		
		int scale = 1;
		try {
			BitmapFactory.Options opt = new BitmapFactory.Options();
			opt.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(is, null,opt);
			int w = opt.outWidth;
			int h = opt.outHeight;
			
			if(w<h) {
				scale = Math.round((float)h/rh + 0.5f);
			}
			else {
				scale = Math.round((float)w/rw + 0.5f);
			}			
		}
		catch (Exception e){
			Log.d("SAN",e.getMessage());
		}
		
		return scale;
	}
	
	public enum SIZE {SMALL,MEDIUM, LARGE};
	/**
	 * Get version of pic size
	 * @param link origin link
	 * @param size required size
	 * @return link
	 */
	public static String getPicVersion(String link, SIZE size) {
		if(size == SIZE.LARGE) return link;
		
		int i1 = link.lastIndexOf("/"); 
		String link1 = link.substring(0,i1);
		String link2 = link.substring(i1+1,link.length());
		
		if(size == SIZE.SMALL)
			return link1+"/s/"+link2;
		else if(size == SIZE.MEDIUM)
			return link1+"/m/"+link2;
		
		return link;
	}
}
