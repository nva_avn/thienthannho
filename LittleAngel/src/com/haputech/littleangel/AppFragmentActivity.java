package com.haputech.littleangel;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.haputech.littleangel.fragment.HomeFragment;
import com.haputech.littleangel.fragment.MenuFragment;
import com.haputech.littleangel.fragment.book.BookFragment;
import com.haputech.littleangel.fragment.game.GameChooseFragment;
import com.haputech.littleangel.fragment.game.GameFragment;
import com.haputech.littleangel.fragment.game.GameOverFragment;
import com.haputech.littleangel.fragment.game.GamePlayFragment;
import com.haputech.littleangel.fragment.health.HealthFragment;
import com.haputech.littleangel.fragment.hotcall.HotCallFragment;
import com.haputech.littleangel.fragment.remind.RemindFragment;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;

/**
 * Activity Fragment
 * 
 * @author NgVietAn
 * 
 */
public class AppFragmentActivity extends SlidingFragmentActivity {
	/**
	 * action bar
	 */
	private ActionBar actionBar;

	/**
	 * content fragment
	 */
	private Fragment mContent;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initActionBar();

		if (savedInstanceState != null)
			mContent = getSupportFragmentManager().getFragment(
					savedInstanceState, "mContent");
		if (mContent == null)
			mContent = new HomeFragment();

		// set the Above View
		setContentView(R.layout.content_frame);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.content_frame, mContent).commit();

		// set the Behind View
		setBehindContentView(R.layout.menu_frame);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.menu_frame, new MenuFragment()).commit();

		initSlideMenu();

		this.onAttachedToWindow();
	}

	@Override
	protected void onUserLeaveHint() {
		// TODO Auto-generated method stub
		Log.i("KEY", "home press");
		if (GamePlayFragment.nhapnhay != null
				&& !GamePlayFragment.nhapnhay.isCancelled()) {
			GamePlayFragment.nhapnhay.cancel(true);
			GamePlayFragment.nhapnhay = null;
		}
		if (GamePlayFragment.countDown != null) {
			GamePlayFragment.countDown.cancel();
			GamePlayFragment.nhapnhay = null;
		}
		this.finish();
		super.onUserLeaveHint();
	}

	/**
	 * Khoi tao action bar
	 */
	protected void initActionBar() {

		actionBar = getActionBar();
		// Hien button back tren action bar
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#cd1e6e")));
	}

	/**
	 * Khoi tao slide menu
	 */
	protected void initSlideMenu() {
		SlidingMenu sm = getSlidingMenu();
		// sm.setShadowWidthRes(R.dimen.shadow_width);
		// sm.setShadowDrawable(R.drawable.shadow);
		// sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setBehindWidthRes(R.dimen.behind_width);
		sm.setFadeDegree(0.36f);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == R.id.slide_menu) {
			toggle();
		} else if (item.getItemId() == android.R.id.home) {
			Fragment fragment = new HomeFragment();
			switchContent(fragment);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		getSupportFragmentManager().putFragment(outState, "mContent", mContent);
	}

	/**
	 * Thay doi fragment content
	 * 
	 * @param fragment
	 */
	public void switchContent(Fragment fragment) {
		mContent = fragment;
		if (fragment instanceof HomeFragment
				|| fragment instanceof BookFragment
				|| fragment instanceof GameFragment
				|| fragment instanceof HealthFragment
				|| fragment instanceof RemindFragment
				|| fragment instanceof HotCallFragment
				|| fragment instanceof GameOverFragment
				|| fragment instanceof GameChooseFragment) {
			getSupportFragmentManager().popBackStack(null,
					FragmentManager.POP_BACK_STACK_INCLUSIVE);
			if (!(fragment instanceof HomeFragment)) {
				getSupportFragmentManager().beginTransaction()
						.replace(R.id.content_frame, fragment)
						.addToBackStack(fragment.getTag()).commit();
			} else {
				getSupportFragmentManager().beginTransaction()
						.replace(R.id.content_frame, fragment).commit();
			}
		} else {
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, fragment)
					.addToBackStack(fragment.getTag()).commit();
		}
		getSlidingMenu().showContent(true);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mContent instanceof GamePlayFragment)
			return;
		if (mContent instanceof GameChooseFragment) {
			Fragment fragment = new GameFragment();
			switchContent(fragment);
			return;
		}
		super.onBackPressed();
	}

	/**
	 * Dat tieu de cho action bar
	 * 
	 * @param title
	 *            tieu de cua action bar
	 */
	public void setActionBarTitle(String title) {
		actionBar.setTitle(title);
	}

}
