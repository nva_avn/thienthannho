package com.haputech.littleangel.db;

import java.io.IOException;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.haputech.littleangel.object.Growth;

/**
 * Database danh gia phat trien
 * 
 * @author NgVietAn
 * 
 */
public class DatabaseGrowth {

	Context context;
	DatabaseHelper db;
	private static final String TABLE_NAME = "growth";
	private static final String ID = "id";
	private static final String BABY_ID = "babyID";
	private static final String AGE = "age";
	private static final String HEIGHT = "height";
	private static final String WEIGHT = "weight";
	private static final String RATE = "rate";

	public DatabaseGrowth(Context context) {
		this.context = context;
		db = new DatabaseHelper(context);
		try {
			db.createDataBase();
		} catch (IOException ioe) {
			throw new Error("Unable to create database");
		}
	}

	private void openDb() {
		try {
			db.openDataBase();
		} catch (SQLException sqle) {
			db.close();
			throw sqle;
		}
	}

	/**
	 * Add new growth
	 * 
	 * @param growth
	 * @return
	 */
	public long addData(Growth growth) {
		openDb();
		SQLiteDatabase data = db.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ID, growth.getID());
		values.put(BABY_ID, growth.getBabyID());
		values.put(AGE, growth.getAge());
		values.put(WEIGHT, growth.getWeight());
		values.put(HEIGHT, growth.getHeight());
		values.put(RATE, growth.getRate());
		long check = data.insert(TABLE_NAME, ID, values);
		data.close();
		db.close();
		return check;
	}

	/**
	 * Delete growth
	 * 
	 * @param growth
	 * @return
	 */
	public int deleteData(Growth growth) {
		openDb();
		String[] growthID = { String.valueOf(growth.getID()) };
		SQLiteDatabase data = db.getWritableDatabase();
		int check = data.delete(TABLE_NAME, ID + "=?", growthID);
		data.close();
		db.close();
		return check;
	}

	/**
	 * Update growth
	 * 
	 * @param oldGrowth
	 * @param newGrowth
	 * @return
	 */
	public int updateData(Growth oldGrowth, Growth newGrowth) {
		openDb();
		String[] growthID = { String.valueOf(oldGrowth.getID()) };
		SQLiteDatabase data = db.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ID, newGrowth.getID());
		values.put(BABY_ID, newGrowth.getBabyID());
		values.put(AGE, newGrowth.getAge());
		values.put(WEIGHT, newGrowth.getWeight());
		values.put(HEIGHT, newGrowth.getHeight());
		values.put(RATE, newGrowth.getRate());
		int check = data.update(TABLE_NAME, values, ID + "=?", growthID);
		data.close();
		db.close();
		return check;
	}

	/**
	 * Get growth
	 * 
	 * @param babyID
	 *            id of baby
	 * @param age
	 *            age of baby
	 * @return
	 */
	public Growth getGrowth(int babyID, String age) {
		openDb();
		SQLiteDatabase data = db.getReadableDatabase();
		String exeSQL = "Select * from " + TABLE_NAME + " where " + BABY_ID
				+ " = " + babyID + " and " + AGE + "='" + age + "'";
		Cursor c = data.rawQuery(exeSQL, null);
		if (!c.moveToFirst()) {
			c.close();
			data.close();
			db.close();
			return null;
		}

		int idcol = c.getColumnIndex(ID);
		int babycol = c.getColumnIndex(BABY_ID);
		int agecol = c.getColumnIndex(AGE);
		int wcol = c.getColumnIndex(WEIGHT);
		int hcol = c.getColumnIndex(HEIGHT);
		int ratecol = c.getColumnIndex(RATE);

		Growth growth = new Growth(c.getInt(idcol), c.getInt(babycol),
				c.getString(agecol), c.getDouble(hcol), c.getDouble(wcol),
				c.getString(ratecol));
		c.close();
		data.close();
		db.close();
		return growth;
	}

	/**
	 * Get Max Id in table growth
	 * 
	 * @return
	 */
	public int getMaxID() {
		openDb();
		int id = 0;
		SQLiteDatabase data = db.getReadableDatabase();
		String exeSQL = "Select * from " + TABLE_NAME;
		Cursor c = data.rawQuery(exeSQL, null);
		if (!c.moveToFirst()) {
			c.close();
			data.close();
			db.close();
			return id;
		}

		id = c.getInt(c.getColumnIndex(ID));
		c.close();
		data.close();
		db.close();
		return id;
	}

	/**
	 * Get list growth
	 * 
	 * @param babyID
	 * @return
	 */
	public ArrayList<Growth> getListGrowth(int babyID) {
		openDb();
		SQLiteDatabase data = db.getReadableDatabase();
		String exeSQL = "Select * from " + TABLE_NAME + " where " + BABY_ID
				+ " = " + babyID + " order by " + AGE;
		Cursor c = data.rawQuery(exeSQL, null);
		int idcol = c.getColumnIndex(ID);
		int babycol = c.getColumnIndex(BABY_ID);
		int agecol = c.getColumnIndex(AGE);
		int wcol = c.getColumnIndex(WEIGHT);
		int hcol = c.getColumnIndex(HEIGHT);
		int ratecol = c.getColumnIndex(RATE);

		ArrayList<Growth> growth = new ArrayList<Growth>();

		if (c.moveToFirst()) {
			do {
				growth.add(new Growth(c.getInt(idcol), c.getInt(babycol), c
						.getString(agecol), c.getDouble(hcol), c
						.getDouble(wcol), c.getString(ratecol)));
			} while (c.moveToNext());
		}
		c.close();
		data.close();
		db.close();
		return growth;
	}
}
