package com.haputech.littleangel.db;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.haputech.littleangel.object.Remind;

/**
 * Database nhac nho
 * 
 * @author NgVietAn
 * 
 */
public class DatabaseRemind {

	Context context;
	DatabaseHelper db;

	private static final String TABLE_NAME = "remind";
	private static final String ID = "id";
	private static final String BABY_ID = "babyID";
	private static final String TYPE = "type";
	private static final String TITLE = "title";
	private static final String CONTENT = "content";
	private static final String TIME = "time";
	private static final String VOICE = "voice";
	private static final String REPLAY = "replay";
	private static final String STATUS = "status";

	/**
	 * Array PendingIntent Remind
	 */
	public static HashMap<Integer, PendingIntent> intentArray = new HashMap<Integer, PendingIntent>();

	public DatabaseRemind(Context context) {
		this.context = context;
		db = new DatabaseHelper(context);
		try {
			db.createDataBase();
		} catch (IOException ioe) {
			throw new Error("Unable to create database");
		}
	}

	private void openDb() {
		try {
			db.openDataBase();
		} catch (SQLException sqle) {
			db.close();
			throw sqle;
		}
	}

	/**
	 * Add remind
	 * 
	 * @param remind
	 * @param pendingIntent
	 * @return -1 if fail, id of row if success
	 */
	public long addRemind(Remind remind, PendingIntent pendingIntent) {
		openDb();
		SQLiteDatabase data = db.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ID, remind.getID());
		values.put(BABY_ID, remind.getBabyID());
		values.put(TYPE, remind.getType());
		values.put(TITLE, remind.getTitle());
		values.put(CONTENT, remind.getContent());
		values.put(TIME, remind.getTime());
		values.put(VOICE, remind.getVoice());
		values.put(REPLAY, remind.getReplay());
		values.put(STATUS, remind.getStatus());
		long check = data.insert(TABLE_NAME, ID, values);

		// save PendingIntent, index is the same with id of remind
		if (check != -1)
			intentArray.put(remind.getID(), pendingIntent);
		data.close();
		db.close();
		return check;
	}

	/**
	 * Update Remind
	 * 
	 * NOTE : cancel old alarm in Activity before use updateRemind and
	 * deleteRemind method
	 * 
	 * @param oldRemind
	 * @param newRemind
	 * @param pendingIntent
	 * @return number of row affected
	 * 
	 * 
	 */
	public int updateRemind(Remind oldRemind, Remind newRemind,
			PendingIntent pendingIntent) {
		openDb();
		SQLiteDatabase data = db.getWritableDatabase();
		String[] id = { String.valueOf(oldRemind.getID()) };

		ContentValues values = new ContentValues();
		values.put(ID, newRemind.getID());
		values.put(BABY_ID, newRemind.getBabyID());
		values.put(TYPE, newRemind.getType());
		values.put(TITLE, newRemind.getTitle());
		values.put(CONTENT, newRemind.getContent());
		values.put(TIME, newRemind.getTime());
		values.put(VOICE, newRemind.getVoice());
		values.put(REPLAY, newRemind.getReplay());
		values.put(STATUS, newRemind.getStatus());
		int check = data.update(TABLE_NAME, values, ID + "=?", id);
		if (pendingIntent != null && check != 0) {
			intentArray.remove(oldRemind.getID());
			intentArray.put(newRemind.getID(), pendingIntent);
		}
		data.close();
		db.close();
		return check;
	}

	/**
	 * Delete Remind
	 * 
	 * @param remind
	 * @return number of row affected
	 */
	public int deleteRemind(Remind remind) {
		openDb();
		SQLiteDatabase data = db.getWritableDatabase();
		String[] id = { String.valueOf(remind.getID()) };

		int check = data.delete(TABLE_NAME, ID + "=?", id);
		if (check != 0)
			intentArray.remove(remind.getID());
		data.close();
		db.close();
		return check;
	}

	/**
	 * Get Remind
	 * 
	 * @param id
	 *            id of remind
	 * @return remind
	 */
	public Remind getRemind(int id) {
		openDb();
		SQLiteDatabase data = db.getReadableDatabase();
		String exeSQL = "Select * from " + TABLE_NAME + " where " + ID + "="
				+ id;

		Cursor c = data.rawQuery(exeSQL, null);
		if (!c.moveToFirst()) {
			c.close();
			data.close();
			return null;
		}
		int idcol = c.getColumnIndex(ID);
		int babycol = c.getColumnIndex(BABY_ID);
		int typecol = c.getColumnIndex(TYPE);
		int titcol = c.getColumnIndex(TITLE);
		int contentcol = c.getColumnIndex(CONTENT);
		int timecol = c.getColumnIndex(TIME);
		int voicecol = c.getColumnIndex(VOICE);
		int replaycol = c.getColumnIndex(REPLAY);
		int statuscol = c.getColumnIndex(STATUS);

		Remind remind = new Remind(c.getInt(idcol), c.getInt(babycol),
				c.getInt(typecol), c.getString(titcol),
				c.getString(contentcol), c.getLong(timecol),
				c.getInt(voicecol), c.getLong(replaycol),
				c.getString(statuscol));

		c.close();
		data.close();
		db.close();
		return remind;
	}

	/**
	 * Get list remind
	 * 
	 * @param sqlQuerry
	 *            query to get list
	 * @return
	 */
	public ArrayList<Remind> getListRemind(String sqlQuerry) {
		openDb();
		SQLiteDatabase data = db.getReadableDatabase();

		Cursor c = data.rawQuery(sqlQuerry, null);
		int idcol = c.getColumnIndex(ID);
		int babycol = c.getColumnIndex(BABY_ID);
		int typecol = c.getColumnIndex(TYPE);
		int titcol = c.getColumnIndex(TITLE);
		int contentcol = c.getColumnIndex(CONTENT);
		int timecol = c.getColumnIndex(TIME);
		int voicecol = c.getColumnIndex(VOICE);
		int replaycol = c.getColumnIndex(REPLAY);
		int statuscol = c.getColumnIndex(STATUS);
		ArrayList<Remind> list = new ArrayList<Remind>();
		if (c.moveToFirst()) {
			do {
				list.add(new Remind(c.getInt(idcol), c.getInt(babycol), c
						.getInt(typecol), c.getString(titcol), c
						.getString(contentcol), c.getLong(timecol), c
						.getInt(voicecol), c.getLong(replaycol), c
						.getString(statuscol)));
			} while (c.moveToNext());
		}

		c.close();
		data.close();
		db.close();
		return list;
	}

	/**
	 * Get list remind of baby
	 * 
	 * @param babyID
	 *            id of baby
	 * @return
	 */
	public ArrayList<Remind> getBabyRemind(int babyID) {
		String exeSQL = "Select * from " + TABLE_NAME + " where " + BABY_ID
				+ "=" + babyID;
		return getListRemind(exeSQL);
	}

	/**
	 * Get list remind of baby with type
	 * 
	 * @param babyID
	 *            id of baby
	 * @param type
	 *            type of remind
	 * @return
	 */
	public ArrayList<Remind> getBabyRemind(int babyID, int type) {
		String exeSQL = "Select * from " + TABLE_NAME + " where " + BABY_ID
				+ "=" + babyID + " and " + TYPE + "='" + type + "'";
		return getListRemind(exeSQL);
	}

	/**
	 * Get list remind of baby with type and title
	 * 
	 * @param babyID
	 *            id of baby
	 * @param type
	 *            type of remind
	 * @param title
	 *            title of remind
	 * @return
	 */
	public ArrayList<Remind> getBabyRemind(int babyID, int type, String title) {
		String exeSQL = "Select * from " + TABLE_NAME + " where " + BABY_ID
				+ "=" + babyID + " and " + TYPE + "='" + type + "' and "
				+ TITLE + "='" + title + "'";
		return getListRemind(exeSQL);
	}

	/**
	 * Get list remind of baby in date
	 * 
	 * @param babyID
	 *            id of baby
	 * @param date
	 *            date of remind
	 * @return
	 */
	public ArrayList<Remind> getRemindDate(int babyID, long date) {
		String exeSQL = "Select * from " + TABLE_NAME + " where " + BABY_ID
				+ "=" + babyID + " and " + TIME + " between " + date + " and "
				+ (date + 86400000) + " order by " + TIME;
		Log.i("SQL", exeSQL);
		return getListRemind(exeSQL);
	}

	/**
	 * Get list remind of baby in date with type
	 * 
	 * @param babyID
	 * @param type
	 * @param date
	 * @return
	 */
	public ArrayList<Remind> getRemindDate(int babyID, int type, long date) {
		String exeSQL = "Select * from " + TABLE_NAME + " where " + BABY_ID
				+ "=" + babyID + " and " + TIME + " between " + date + " and "
				+ (date + 86400000) + " and " + TYPE + "='" + type
				+ "' order by " + TIME;
		Log.i("SQL", exeSQL);
		return getListRemind(exeSQL);
	}

	/**
	 * Get max id
	 * 
	 * @return
	 */
	public int getMaxID() {
		openDb();
		int id = 0;
		SQLiteDatabase data = db.getReadableDatabase();
		String exeSQL = "Select " + ID + " from " + TABLE_NAME + " order by "
				+ ID;
		Cursor c = data.rawQuery(exeSQL, null);
		if (!c.moveToFirst()) {
			c.close();
			data.close();
			return id;
		}
		if (c.moveToLast())
			id = c.getInt(c.getColumnIndex(ID));
		c.close();
		data.close();
		db.close();
		return id;
	}
}
