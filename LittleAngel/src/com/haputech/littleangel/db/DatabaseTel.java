package com.haputech.littleangel.db;

import java.io.IOException;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.haputech.littleangel.object.HotTel;

/**
 * Database danh ba
 * 
 * @author NgVietAn
 * 
 */
public class DatabaseTel {

	Context context;
	DatabaseHelper db;
	private static final String TABLE_NAME = "hotcall";
	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String TEL = "tel";

	public DatabaseTel(Context context) {
		this.context = context;
		db = new DatabaseHelper(context);
		try {
			db.createDataBase();
		} catch (IOException ioe) {
			throw new Error("Unable to create database");
		}
	}

	private void openDb() {
		try {
			db.openDataBase();
		} catch (SQLException sqle) {
			db.close();
			throw sqle;
		}
	}

	/**
	 * Add new tel
	 * 
	 * @param tel
	 * @return -1 if fail, id of row if success
	 */
	public long addHotTel(HotTel tel) {
		openDb();
		SQLiteDatabase data = db.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ID, tel.getID());
		values.put(NAME, tel.getName());
		values.put(TEL, tel.getTel());
		long insert = data.insert(TABLE_NAME, ID, values);
		data.close();
		db.close();
		return insert;
	}

	/**
	 * Delete tel
	 * 
	 * @param tel
	 * @return number of rows affected
	 */
	public int deleteHotTel(HotTel tel) {
		openDb();
		String[] telID = { String.valueOf(tel.getID()) };
		SQLiteDatabase data = db.getWritableDatabase();
		int count = data.delete(TABLE_NAME, ID + "=?", telID);
		data.close();
		db.close();
		return count;
	}

	/**
	 * Update tel
	 * 
	 * @param oldTel
	 * @param newTel
	 * @return number of rows affected
	 */
	public int updateHotTel(HotTel oldTel, HotTel newTel) {
		openDb();
		String[] telID = { String.valueOf(oldTel.getID()) };
		SQLiteDatabase data = db.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ID, newTel.getID());
		values.put(NAME, newTel.getName());
		values.put(TEL, newTel.getTel());
		// values.put(TYPE, newTel.getType());
		int count = data.update(TABLE_NAME, values, ID + "=?", telID);
		data.close();
		db.close();
		return count;
	}

	/**
	 * Get tel
	 * 
	 * @param id
	 * @return
	 */
	public HotTel getTel(int id) {
		openDb();
		SQLiteDatabase data = db.getReadableDatabase();
		String exeSQL = "Select * from " + TABLE_NAME + " where " + ID + " = "
				+ id;
		Cursor c = data.rawQuery(exeSQL, null);
		if (!c.moveToFirst()) {
			c.close();
			data.close();
			return null;
		}
		int idcol = c.getColumnIndex(ID);
		int namecol = c.getColumnIndex(NAME);
		int telcol = c.getColumnIndex(TEL);
		// int typecol = c.getColumnIndex(TYPE);

		HotTel tel = new HotTel(c.getInt(idcol), c.getString(namecol),
				c.getString(telcol));
		c.close();
		data.close();
		db.close();
		return tel;
	}

	/**
	 * Get list tel
	 * 
	 * @return
	 */
	public ArrayList<HotTel> getListTel() {
		openDb();
		SQLiteDatabase data = db.getReadableDatabase();
		String exeSQL = "Select * from " + TABLE_NAME;
		Cursor c = data.rawQuery(exeSQL, null);
		int idcol = c.getColumnIndex(ID);
		int namecol = c.getColumnIndex(NAME);
		int telcol = c.getColumnIndex(TEL);
		// int typecol = c.getColumnIndex(TYPE);

		ArrayList<HotTel> tel = new ArrayList<HotTel>();
		if (c.moveToFirst()) {
			do {
				tel.add(new HotTel(c.getInt(idcol), c.getString(namecol), c
						.getString(telcol)));
			} while (c.moveToNext());
		}
		c.close();
		data.close();
		db.close();
		return tel;
	}

	/**
	 * Check name exist
	 * 
	 * @param name
	 * @return
	 */
	public boolean checkName(String name) {
		openDb();
		SQLiteDatabase data = db.getReadableDatabase();
		String exeSQL = "Select * from " + TABLE_NAME + " where " + NAME
				+ " = '" + name + "'";
		Cursor c = data.rawQuery(exeSQL, null);
		int count = c.getCount();
		if (count == 0)
			return true;
		return false;
	}

	/**
	 * Get max id
	 * 
	 * @return
	 */
	public int getMaxID() {
		openDb();
		SQLiteDatabase data = db.getReadableDatabase();
		int id = 0;
		String exeSQL = "Select * from " + TABLE_NAME;
		Cursor c = data.rawQuery(exeSQL, null);
		if (!c.moveToFirst()) {
			c.close();
			data.close();
			return id;
		}
		int idcol = c.getColumnIndex(ID);
		if (c.moveToLast())
			id = c.getInt(idcol);
		c.close();
		data.close();
		db.close();
		return id;
	}

}
