package com.haputech.littleangel.db;

import java.io.IOException;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.haputech.littleangel.object.Baby;

/**
 * Database baby
 * 
 * @author NgVietAn
 * 
 */
public class DatabaseBaby {

	Context context;
	DatabaseHelper db;
	private static final String TABLE_NAME = "baby";
	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String FULLNAME = "fullname";
	private static final String BIRTHDAY = "birthday";
	private static final String PREGNANCY = "pregnancy";
	private static final String SEX = "sex";
	private static final String IMAGE = "image";

	public DatabaseBaby(Context context) {
		this.context = context;
		db = new DatabaseHelper(context);
		try {
			db.createDataBase();
		} catch (IOException ioe) {
			throw new Error("Unable to create database");
		}
	}

	private void openDb() {
		try {
			db.openDataBase();
		} catch (SQLException sqle) {
			db.close();
			throw sqle;
		}
	}

	/**
	 * Add new baby
	 * 
	 * @param baby
	 * @return -1 if fail, id of row if success
	 */
	public long addBaby(Baby baby) {
		openDb();
		SQLiteDatabase data = db.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ID, baby.getID());
		values.put(NAME, baby.getName());
		values.put(FULLNAME, baby.getFullname());
		values.put(BIRTHDAY, baby.getBirthday());
		values.put(PREGNANCY, baby.getPregnancy());
		values.put(SEX, baby.getSex());
		values.put(IMAGE, baby.getImage());
		long check = data.insert(TABLE_NAME, ID, values);
		data.close();
		db.close();
		return check;
	}

	/**
	 * Delete baby
	 * 
	 * @param baby
	 * @return number of rows affected
	 */
	public int deleteBaby(Baby baby) {
		openDb();
		String[] babyID = { String.valueOf(baby.getID()) };
		SQLiteDatabase data = db.getWritableDatabase();
		int check = data.delete(TABLE_NAME, ID + "=?", babyID);
		data.close();
		db.close();
		return check;
	}

	/**
	 * Update baby
	 * 
	 * @param oldBaby
	 * @param newBaby
	 * @return number of rows affected
	 */
	public int updateBaby(Baby oldBaby, Baby newBaby) {
		openDb();
		String[] babyID = { String.valueOf(oldBaby.getID()) };
		SQLiteDatabase data = db.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ID, newBaby.getID());
		values.put(NAME, newBaby.getName());
		values.put(FULLNAME, newBaby.getFullname());
		values.put(BIRTHDAY, newBaby.getBirthday());
		values.put(PREGNANCY, newBaby.getPregnancy());
		values.put(SEX, newBaby.getSex());
		values.put(IMAGE, newBaby.getImage());
		int check = data.update(TABLE_NAME, values, ID + "=?", babyID);
		data.close();
		db.close();
		return check;
	}

	/**
	 * Get baby
	 * 
	 * @param id
	 *            id of baby
	 * @return
	 */
	public Baby getBaby(int id) {
		openDb();
		SQLiteDatabase data = db.getReadableDatabase();
		String exeSQL = "Select * from " + TABLE_NAME + " where " + ID + "="
				+ id;
		Cursor c = data.rawQuery(exeSQL, null);
		if (!c.moveToFirst()) {
			c.close();
			data.close();
			db.close();
			return null;
		}
		int idcol = c.getColumnIndex(ID);
		int namecol = c.getColumnIndex(NAME);
		int fullnamecol = c.getColumnIndex(FULLNAME);
		int bircol = c.getColumnIndex(BIRTHDAY);
		int pregcol = c.getColumnIndex(PREGNANCY);
		int sexcol = c.getColumnIndex(SEX);
		int imagecol = c.getColumnIndex(IMAGE);

		Baby baby = new Baby(c.getInt(idcol), c.getString(namecol),
				c.getString(fullnamecol), c.getLong(bircol),
				c.getLong(pregcol), c.getInt(sexcol), c.getString(imagecol));
		c.close();
		data.close();
		db.close();
		return baby;
	}

	/**
	 * Get list baby
	 * 
	 * @return
	 */
	public ArrayList<Baby> getListBaby() {
		openDb();
		SQLiteDatabase data = db.getReadableDatabase();
		String exeSQL = "Select * from " + TABLE_NAME;
		Cursor c = data.rawQuery(exeSQL, null);
		int idcol = c.getColumnIndex(ID);
		int namecol = c.getColumnIndex(NAME);
		int fullnamecol = c.getColumnIndex(FULLNAME);
		int bircol = c.getColumnIndex(BIRTHDAY);
		int pregcol = c.getColumnIndex(PREGNANCY);
		int sexcol = c.getColumnIndex(SEX);
		int imagecol = c.getColumnIndex(IMAGE);

		ArrayList<Baby> baby = new ArrayList<Baby>();
		if (c.moveToFirst()) {
			do {
				baby.add(new Baby(c.getInt(idcol), c.getString(namecol), c
						.getString(fullnamecol), c.getLong(bircol), c
						.getLong(pregcol), c.getInt(sexcol), c
						.getString(imagecol)));
			} while (c.moveToNext());
		}
		c.close();
		data.close();
		db.close();
		return baby;
	}

	/**
	 * Get max id
	 * 
	 * @return max id in db
	 */
	public int getMaxID() {
		openDb();
		SQLiteDatabase data = db.getReadableDatabase();
		int id = 0;
		String exeSQL = "Select * from " + TABLE_NAME;
		Cursor c = data.rawQuery(exeSQL, null);
		if (!c.moveToFirst()) {
			c.close();
			data.close();
			return id;
		}
		int idcol = c.getColumnIndex(ID);
		if (c.moveToLast())
			id = c.getInt(idcol);
		c.close();
		data.close();
		db.close();
		return id;
	}
}
