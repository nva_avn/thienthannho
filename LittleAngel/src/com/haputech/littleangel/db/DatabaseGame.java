package com.haputech.littleangel.db;

import java.util.ArrayList;

import com.haputech.littleangel.object.Game;
import com.haputech.littleangel.object.HighScore;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseGame {

	Context context;
	DatabaseHelper db;
	private static final String TABLE_NAME_GAME = "game";
	private static final String ID = "id";
	private static final String CAU_HOI = "cau_hoi";
	private static final String CAU_A = "cau_a";
	private static final String CAU_B = "cau_b";
	private static final String CAU_C = "cau_c";
	private static final String CAU_D = "cau_d";
	private static final String DAP_AN = "dap_an";
	private static final String LOAI = "loai";

	private static final String TABLE_NAME_SCORE = "highscore";
	private static final String NAME = "name";
	private static final String SCORE = "score";

	public DatabaseGame(Context context) {
		super();
		this.context = context;
		db = new DatabaseHelper(context);
	}

	private void openDb() {
		try {
			db.openDataBase();
		} catch (SQLException sqle) {
			db.close();
			throw sqle;
		}
	}

	public ArrayList<Game> getListQuestion(String loai) {
		openDb();
		SQLiteDatabase data = db.getReadableDatabase();
		String select = "SELECT * FROM " + TABLE_NAME_GAME + " WHERE " + LOAI
				+ "='" + loai + "'";
		Cursor c = data.rawQuery(select, null);
		int id = c.getColumnIndex(ID);
		int loaiCauHoi = c.getColumnIndex(LOAI);
		int cauhoi = c.getColumnIndex(CAU_HOI);
		int cauA = c.getColumnIndex(CAU_A);
		int cauB = c.getColumnIndex(CAU_B);
		int cauC = c.getColumnIndex(CAU_C);
		int cauD = c.getColumnIndex(CAU_D);
		int dapAn = c.getColumnIndex(DAP_AN);

		ArrayList<Game> question = new ArrayList<Game>();

		if (c.moveToFirst())
			do {
				question.add(new Game(c.getInt(id), c.getString(cauhoi), c
						.getString(cauA), c.getString(cauB), c.getString(cauC),
						c.getString(cauD), c.getString(dapAn), c.getString(
								loaiCauHoi).trim()));
			} while (c.moveToNext());
		c.close();
		data.close();
		db.close();
		return question;
	}

	public long addNewHighhScore(HighScore score) {
		openDb();
		SQLiteDatabase data = db.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ID, score.getId());
		values.put(NAME, score.getName());
		values.put(SCORE, score.getScore());
		long check = data.insert(TABLE_NAME_SCORE, ID, values);
		data.close();
		db.close();
		return check;
	}

	public int updateBaby(HighScore oldScore, HighScore newScore) {
		openDb();
		String[] scoreId = { String.valueOf(oldScore.getId()) };
		SQLiteDatabase data = db.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(NAME, newScore.getName());
		values.put(SCORE, newScore.getScore());
		int check = data.update(TABLE_NAME_SCORE, values, ID + "=?", scoreId);
		data.close();
		db.close();
		return check;
	}

	public ArrayList<HighScore> getListHighScore() {
		openDb();
		SQLiteDatabase data = db.getReadableDatabase();
		String select = "SELECT * FROM " + TABLE_NAME_SCORE + " ORDER BY "
				+ SCORE;

		Cursor c = data.rawQuery(select, null);
		int id = c.getColumnIndex(ID);
		int name = c.getColumnIndex(NAME);
		int score = c.getColumnIndex(SCORE);

		ArrayList<HighScore> scores = new ArrayList<HighScore>();

		if (c.moveToFirst())
			do {
				scores.add(new HighScore(c.getInt(id), c.getString(name), c
						.getInt(score)));

			} while (c.moveToNext());
		c.close();
		data.close();
		db.close();
		return scores;
	}
}
